package com.workflow.nikomi.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.Utility;

import static com.workflow.nikomi.utils.App.CHANNEL_ID;
import static com.workflow.nikomi.utils.Constant.SHIFT_HOURS;
import static com.workflow.nikomi.utils.Constant.SHIFT_MIN_FINAL;

public class OverrideJobSchedulerService extends JobService {
    JobParameters params;
    DoItTask doIt;
    private static final String TAG = "JobSchedulerService";
    public JobScheduler jobScheduler;
    JobInfo jobInfo;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d("DoItTask", "Work to be called from here");
        this.params = params;

        doIt = new DoItTask(getApplicationContext());
        doIt.execute();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d("TestService", "System calling to stop the job here");
        if (doIt != null)
            doIt.cancel(true);
        return false;
    }

    private class DoItTask extends AsyncTask<Void, Void, Void>  {
        Context context;
        public DoItTask(Context context) {
            this.context = context;
            jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("DoItTask", "Clean up the task here and call jobFinished...");
            jobFinished(params, false);

            if (Utility.isAppIsInBackground(context)){

                Intent mIntent = new Intent(context, MainActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.putExtra("override",1);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0  , mIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                generateNotification("Override Job", getString(R.string.do_you_want_override_job) ,pendingIntent);

            }else{
                sendBroadcast(new Intent(TAG));

                getTimeToRestartScheduler();
            }

            super.onPostExecute(aVoid);
        }

        private void restartOverrideScheduler() {
            //jobScheduler.cancelAll();
            ComponentName componentName = new ComponentName(getApplicationContext(), OverrideJobSchedulerService.class);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                jobInfo = new JobInfo.Builder(102, componentName)
                        .setMinimumLatency((14) * 60 * 1000)
                        .setOverrideDeadline((14) * 60 * 1000)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();
            } else {
                jobInfo = new JobInfo.Builder(102, componentName)
                        .setPeriodic((14) * 60 * 1000)
                        .setRequiresCharging(true)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();
            }

            int jobId = jobScheduler.schedule(jobInfo);
            if(jobId>0){
                Log.d(TAG, "onNo: Successfully restart override scheduled job:"+jobId);
            }else{
                Log.d(TAG, "onNo: Override restartscheduled job failed:"+jobId);
                jobScheduler.schedule(jobInfo);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("DoItTask", "Working here...");
            return null;
        }

        private void generateNotification(String title, String messageBody, PendingIntent pendingIntent) {

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(title)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody)) // added on 27/04/2020
                            .setContentText(messageBody)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Nikomi",
                        NotificationManager.IMPORTANCE_DEFAULT);

                notificationManager.createNotificationChannel(channel);
            }

            notificationManager.notify( 0, notificationBuilder.build());

            getTimeToRestartScheduler();
        }

        private void getTimeToRestartScheduler() {
            long tempDiff = Long.parseLong(new AppSession(OverrideJobSchedulerService.this).getShiftHour()) - System.currentTimeMillis();

            if (params.getJobId() == 101 && tempDiff>840000)
                /*Restart the JobScheduler before the 1 min. of the shift end time*/
                restartOverrideScheduler();
        }
    }

}
