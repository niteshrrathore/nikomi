package com.workflow.nikomi.service;

import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonObject;
import com.workflow.nikomi.activity.LoginActivity;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.api.ResponseInterface;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.workflow.nikomi.api.APIRequest.getHeaderMaps;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_COMPLETED;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_COMPLETED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;

public class AutoSignoutJobSchedulerService extends JobService {

    JobParameters params;
    AutoSignoutTask doIt;
    private static final String TAG = "JobSchedulerService";
    Call<JsonObject> registerUser = null;
    public JobScheduler jobScheduler;
    HashMap<String, String> paramSignout;

    @Override
    public boolean onStartJob(JobParameters params) {
        this.params = params;
        Log.d("AutoSignoutTask", "Work to be called from here");
        doIt = new AutoSignoutTask(getApplicationContext());
        doIt.execute();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d("AutoSignoutTask", "System calling to stop the job here");

        if (doIt != null)
            doIt.cancel(true);
        return true;
    }

    private class AutoSignoutTask extends AsyncTask<Void, Void, Void> {
        Context context;
        AppSession appSession;
        ResponseInterface userDataRequest;

        public AutoSignoutTask(Context context) {
            this.context = context;
            this.appSession = new AppSession(context);
            this.userDataRequest = APIRequest.provideInterface();
            jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("AutoSignoutTask", "Clean up the task here and call jobFinished...");

            if (Utility.isConnectingToInternet(getApplicationContext())) {
                registerUser = getDataForSignout();
                registerUser.enqueue(new Callback<JsonObject>() {

                    @Override
                    public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {

                        if (response.isSuccessful()) {
                            jobScheduler.cancelAll();
                            String responseString = response.body().toString();
                            Log.e("logout responseString>>>>", "" + responseString);
                            try {
                                jobFinished(params, false);
                                BaseActivity.stopAutoSignoutService();
                                int mStatus = new JSONObject(responseString).optInt("status");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                appSession.clearPrefs();
                                context.startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e("Main Activity", "onFailure() called with: " + "call = [" + call + "], t = [" + t.getMessage() + "]");
                    }

                });

            }

            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("AutoSignoutTask", "Working here...");
            return null;
        }

        public Call<JsonObject> getDataForSignout() {
            //if (!appSession.isUserOverrideJob())
            if (appSession.getUserPartDetails() != null) {
                /*If user working on any part or fab
                 * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                if (appSession.isPartAbortClicked()) {
                    paramSignout = new HashMap<>();
                    paramSignout.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                    paramSignout.put("signout", "1");
                    paramSignout.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));
                    paramSignout.put("override", "0");

                } else if (appSession.getUserPartDetails().getStatusId() == ACTION_PART_FAB_COMPLETED
                        && appSession.getUserPartDetails().getStatusId() == ACTION_PART_FAB_READY_FOR_INSPECT) {
                    paramSignout = new HashMap<>();
                    paramSignout.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                    paramSignout.put("signout", "1");
                    paramSignout.put("override", "0");
                }
                return userDataRequest.updatePartAction(getHeaderMaps(context, ServicesType.updatePartAction), appSession.getUserPartDetails().getId().toString(), paramSignout);
            } else if (appSession.getUserFabDetails() != null) {
                /*If user working on any part or fab
                 * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                if (appSession.isPartAbortClicked()) {
                    paramSignout = new HashMap<>();
                    paramSignout.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                    paramSignout.put("signout", "1");
                    paramSignout.put("override", "0");
                    paramSignout.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));
                } else {
                    /*Update fab status with NON fullfil*/
                    paramSignout = new HashMap<>();
                    paramSignout.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                    paramSignout.put("signout", "1");
                    paramSignout.put("override", "0");
                }

                return userDataRequest.updateFabAction(getHeaderMaps(context, ServicesType.updateFabAction), appSession.getUserFabDetails().getId().toString(), paramSignout);
            } else if (appSession.getOnsiteJobDetails() != null) {
                /*If user working on any part or fab
                 * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                if (appSession.isPartAbortClicked()) {
                    paramSignout = new HashMap<>();
                    paramSignout.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                    paramSignout.put("signout", "1");
                    paramSignout.put("override", "0");
                    paramSignout.put("onsite_status_id", String.valueOf(ACTION_JOB_NOT_FULFILLED));
                } else if(appSession.getOnsiteJobDetails().getOnsiteStatusId()== ACTION_JOB_COMPLETED ||
                        appSession.getOnsiteJobDetails().getOnsiteStatusId() == ACTION_JOB_IN_PROGRESS){
                    paramSignout = new HashMap<>();
                    paramSignout.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                    paramSignout.put("signout", "1");
                    paramSignout.put("override", "0");
                    paramSignout.put("onsite_status_id", String.valueOf(ACTION_JOB_NOT_FULFILLED));

                    return userDataRequest.updateJobAction(getHeaderMaps(context, ServicesType.updateJobAction), appSession.getOnsiteJobDetails().getId().toString(), paramSignout);
                } else {
                    /*Update fab status with NON fullfil*/
                    paramSignout = new HashMap<>();
                    paramSignout.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                    paramSignout.put("signout", "1");
                    paramSignout.put("override", "0");
                }

                return userDataRequest.updateJobAction(getHeaderMaps(context, ServicesType.updateJobAction), appSession.getOnsiteJobDetails().getId().toString(), paramSignout);
            } else {

                /*User not working on any of the part*/
                paramSignout = new HashMap<>();
               return userDataRequest.logout(getHeaderMaps(context, ServicesType.logout));
            }

        }

    }

}

