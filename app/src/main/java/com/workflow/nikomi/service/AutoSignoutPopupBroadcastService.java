package com.workflow.nikomi.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.LoginActivity;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.Constant;

import static com.workflow.nikomi.utils.App.CHANNEL_ID;
import static com.workflow.nikomi.utils.Constant.SHIFT_HOURS;

public class AutoSignoutPopupBroadcastService extends Service {

    private final static String TAG = "BroadcastService";
    public JobScheduler jobScheduler;
    JobInfo jobInfo, autoSignOutjobInfo;
    ComponentName mOverrideJobComponentName, mAutoSignoutJobComponentName;
    Intent notificationIntent;
    AppSession appSession;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Starting timer...");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy cancelled");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        appSession = new AppSession(this);

        if (appSession.isUserLogin()) {
            SHIFT_HOURS = Long.parseLong(appSession.getShiftHour()) - System.currentTimeMillis();
            //SHIFT_HOURS = (Integer.parseInt(appSession.getShiftHour()) * 60) / 2;
            notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.putExtra("override", 0);
        } else
            notificationIntent = new Intent(this, LoginActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        createNotificationChannel();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Override time Service")
                .setContentText("Auto signout Service")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);
        setSchedulerForAutomatedProcess();

        return START_STICKY/*super.onStartCommand(intent, flags, startId)*/;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private void setSchedulerForAutomatedProcess() {
        mOverrideJobComponentName = new ComponentName(getApplicationContext(), OverrideJobSchedulerService.class);
        mAutoSignoutJobComponentName = new ComponentName(getApplicationContext(), AutoSignoutJobSchedulerService.class);

        long tempDiff = Long.parseLong(appSession.getShiftHour()) - System.currentTimeMillis();

        /*jobInfo for to open the override popup before 15min. of the shift time*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            if (tempDiff >= 900000)
                //900000milisec= 15min.
                jobInfo = new JobInfo.Builder(101, mOverrideJobComponentName)
                        .setMinimumLatency(SHIFT_HOURS - 900000)
                        .setOverrideDeadline(SHIFT_HOURS - 900000)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();

            else
                //60000milisec = 1min.
                jobInfo = new JobInfo.Builder(101, mOverrideJobComponentName)
                        .setMinimumLatency(SHIFT_HOURS - 60000)
                        .setOverrideDeadline(SHIFT_HOURS - 60000)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();

        } else {
            if (tempDiff >= 900000)
                jobInfo = new JobInfo.Builder(101, mOverrideJobComponentName)
                        .setPeriodic(SHIFT_HOURS - 900000)
                        .setRequiresCharging(true)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();
            else
                jobInfo = new JobInfo.Builder(101, mOverrideJobComponentName)
                        .setPeriodic(SHIFT_HOURS - 60000)
                        .setRequiresCharging(true)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .build();
        }
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            jobInfo = new JobInfo.Builder(101, mOverrideJobComponentName)
                    .setMinimumLatency((SHIFT_HOURS - 15) * 60 * 1000)
                    .setOverrideDeadline((SHIFT_HOURS - 15) * 60 * 1000)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .build();
        } else {
            jobInfo = new JobInfo.Builder(101, mOverrideJobComponentName)
                    .setPeriodic((SHIFT_HOURS - 15) * 60 * 1000)
                    .setRequiresCharging(true)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .build();
        }*/

        int jobId = jobScheduler.schedule(jobInfo);
        if (jobId > 0) {
            Log.d(TAG, "onNo: Successfully override scheduled job:" + jobId);
        } else {
            Log.d(TAG, "onNo: Override scheduled job failed:" + jobId);
            jobScheduler.schedule(jobInfo);
        }

        /*autoSignOutjobInfo for auto signout on shift time*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            autoSignOutjobInfo = new JobInfo.Builder(103, mAutoSignoutJobComponentName)
                    .setMinimumLatency(SHIFT_HOURS)
                    .setRequiresCharging(true)
                    .setOverrideDeadline(SHIFT_HOURS)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .build();
        } else {
            autoSignOutjobInfo = new JobInfo.Builder(103, mAutoSignoutJobComponentName)
                    .setPeriodic(SHIFT_HOURS)
                    .setRequiresCharging(true)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .build();
        }

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            autoSignOutjobInfo = new JobInfo.Builder(103, mAutoSignoutJobComponentName)
                    .setMinimumLatency(SHIFT_HOURS * 60 * 1000)
                    .setRequiresCharging(true)
                    .setOverrideDeadline(SHIFT_HOURS * 60 * 1000)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .build();
        } else {
            autoSignOutjobInfo = new JobInfo.Builder(103, mAutoSignoutJobComponentName)
                    .setPeriodic(SHIFT_HOURS * 60 * 1000)
                    .setRequiresCharging(true)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .build();
        }*/

        int autoSignOutJobId = jobScheduler.schedule(autoSignOutjobInfo);
        if (autoSignOutJobId > 0) {
            Log.d(TAG, "onNo: Successfully auto signout scheduled job:" + autoSignOutJobId);
        } else {
            Log.d(TAG, "onNo: Auto signout scheduled job failed:" + autoSignOutJobId);
            jobScheduler.schedule(autoSignOutjobInfo);
        }

    }

}
