package com.workflow.nikomi.activity;

import androidx.appcompat.app.ActionBar;

import android.app.ProgressDialog;
import android.content.Context;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.workflow.nikomi.R;

import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.CustomProgressDialog;
import com.workflow.nikomi.utils.CustomSemiBoldTextView;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

import static com.workflow.nikomi.utils.Constant.FAB_DRAWING;
import static com.workflow.nikomi.utils.Constant.GENERAL_ASSEMBLY;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.PART_DRAWING;

import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_FAB;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_PART;
import static com.workflow.nikomi.utils.Constant.TYPE_ONSITE_DOC;

public class ViewDrawingActivity extends BaseActivity implements ServicesListener, DownloadFile.Listener {

    String url = "https://sugardate.me/sugarshop/generatePdf/1097/spa-og-massage-for-2_7907.pdf";
    CustomProgressDialog progressDialog;
    RelativeLayout root;
    PDFPagerAdapter adapter;
    RemotePDFViewPager remotePDFViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.header);
        View view = getSupportActionBar().getCustomView();

        if(getIntent().getStringExtra("from").equals(TYPE_ONSITE_DOC)){
            ((CustomSemiBoldTextView) view.findViewById(R.id.tvHeaderTitle)).setText(getString(R.string.viewdocument));
        }else {
            ((CustomSemiBoldTextView) view.findViewById(R.id.tvHeaderTitle)).setText(getString(R.string.view_drawing));
        }
                ((ImageView) view.findViewById(R.id.imgSearch)).setVisibility(View.GONE);

        ImageView imageButton = (ImageView) view.findViewById(R.id.imgBack);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setContentView(R.layout.activity_view_drawing);
        progressDialog = new CustomProgressDialog(this);
        root = (RelativeLayout) findViewById(R.id.remote_pdf_root);

        getPartFabDrawing();

    }

    @Override
    public void success(String response, ServicesType type) {

        switch (type) {
            case getOnsiteDoc:
            case partDrawing:
            case fabDrawing:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        progressDialog.show();

                        if (getIntent().getStringExtra("from").equals(TYPE_ONLY_PART)) {
                            url = jsonObject.getJSONObject(RESPONSE_DATA).getString(PART_DRAWING);
                        } else if (getIntent().getStringExtra("from").equals(TYPE_ONLY_FAB)) {
                            url = jsonObject.getJSONObject(RESPONSE_DATA).getString(FAB_DRAWING);
                        } else if (getIntent().getStringExtra("from").equals(TYPE_ONSITE_DOC)) {
                            url = jsonObject.getJSONObject(RESPONSE_DATA).getString(GENERAL_ASSEMBLY);
                        }

                        progressDialog.show();
                        remotePDFViewPager = new RemotePDFViewPager(this, url, this);
                        remotePDFViewPager.setId(R.id.pdfViewPager);

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(this, KEY_ERROR, message);
    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url));
        remotePDFViewPager.setAdapter(adapter);
        updateLayout();
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(Exception e) {
        e.printStackTrace();
        progressDialog.dismiss();
        Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
        onBackPressed();
    }

    @Override
    public void onProgressUpdate(int progress, int total) {

    }

    public void updateLayout() {
        root.removeAllViewsInLayout();
        root.addView(remotePDFViewPager,
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }

    /*Aug 13, 2020 - get drawing according to the TYPE(PART/FILE/View Drawing)*/
    protected void getPartFabDrawing() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ID, getIntent().getStringExtra(ID));

        if (getIntent().getStringExtra("from").equals(TYPE_ONLY_PART)) {
            APIRequest.generalApiRequest(this, this, this, params, ServicesType.partDrawing, true);
        } else if (getIntent().getStringExtra("from").equals(TYPE_ONLY_FAB)) {
            APIRequest.generalApiRequest(this, this, this, params, ServicesType.fabDrawing, true);
        } else if (getIntent().getStringExtra("from").equals(TYPE_ONSITE_DOC)) {

            APIRequest.generalApiRequest(this, this, this, params, ServicesType.getOnsiteDoc, true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (adapter != null) {
            adapter.close();
        }
    }

}