package com.workflow.nikomi.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.fragment.JobFragment;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.JobPartStatusModel;
import com.workflow.nikomi.utils.App;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.Constant;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.App.DEV_BASE_URL;
import static com.workflow.nikomi.utils.App.Live_URL;
import static com.workflow.nikomi.utils.Constant.JOB_PART_STATUS;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOTAL;

public class SplashActivity extends AppCompatActivity implements ServicesListener {

    private final int SPLASH_DISPLAY_LENGTH = 10/*1000*/;
    private Handler mHandler;
    private Runnable mRunnable;
    AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /*Aug 13, 2020 - Set app default run on dev url*/
        //Live_URL = App.DEV_BASE_URL;
    }

    @Override
    public void onResume() {
        super.onResume();
        appSession = new AppSession(this);
        if (appSession.getBaseUrl().isEmpty()){
            appSession.storeBaseUrl(DEV_BASE_URL);
            Live_URL = DEV_BASE_URL;
        }else{
            Live_URL = appSession.getBaseUrl();
        }
       mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {

                if (appSession.getUserDetails()!=null && appSession.isUserLogin()){
                    HashMap<String, String> param = new HashMap<>();
                    APIRequest.generalApiRequest(SplashActivity.this, SplashActivity.this,
                            SplashActivity.this, param, ServicesType.getMasterData, false);
                }else
                    openLoginOrDashbord();

            }
        };

        mHandler.postDelayed(mRunnable, SPLASH_DISPLAY_LENGTH);
    }

    private void openLoginOrDashbord(){
        if (appSession.getUserDetails()!=null && appSession.isUserLogin()) {

            Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();

        } else {

            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void success(String response, ServicesType type) {
        if (type == ServicesType.getMasterData) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                boolean statusCode = jsonObject.getBoolean(STATUS);
                boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                String message = jsonObject.optString(MESSAGE);
                String responseCode = jsonObject.optString(RESPONSE_CODE);

                if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                    Gson gson = new Gson();
                    Type typeGson = new TypeToken<List<JobPartStatusModel>>() {
                    }.getType();

                    ArrayList<JobPartStatusModel> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(JOB_PART_STATUS), typeGson);
                    appSession.storeJobPartStatus(gson.toJson(mJobList));
                    mJobList.clear();

                    //Constant.SHIFT_HOURS = jsonObject.getJSONObject(RESPONSE_DATA).getInt("shift_hours");

                    openLoginOrDashbord();

                } else {
                    if (showMessage)
                        Utility.toaster(SplashActivity.this, KEY_ERROR, message);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Utility.toaster(SplashActivity.this, KEY_ERROR, getString(R.string.something_went_wrong));
            }
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(SplashActivity.this, KEY_ERROR, message);
    }

}