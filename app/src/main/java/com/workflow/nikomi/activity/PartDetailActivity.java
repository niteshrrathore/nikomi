package com.workflow.nikomi.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.adapters.CustomWorkersSpinnerAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityPartDetailBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.CallBackOk;
import com.workflow.nikomi.interfaces.CallBackSignatureSave;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.model.UserDetail;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_COMPLETED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_OPEN;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.CUT_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.CUT_CHECKED_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.DRILL_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.DRILL_CHECKED_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.FILTER;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.PARTS_COMPLETED;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.PART_TYPE_BOTH;
import static com.workflow.nikomi.utils.Constant.PART_TYPE_CUT;
import static com.workflow.nikomi.utils.Constant.PART_TYPE_DRILL;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SELECT;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;
import static com.workflow.nikomi.utils.Constant.TYPE;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_PART;

public class PartDetailActivity extends BaseActivity implements View.OnClickListener, CallBackOk, CallBackSignatureSave, ServicesListener {

    private static final String TAG = "PartDetailActivity";
    ActivityPartDetailBinding binding;
    AppSession appSession;
    PartDetailModel mPartDetailModel;
    ArrayList<UserDetail> mWorkerList = new ArrayList<>();
    static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean somePermissionsForeverDenied = false;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;
    int posToSetBitMapSiganture = 0;
    int intActionPerformed = 0;
    Spinner[] spinnersCategory;
    RelativeLayout[] spinnersbackground;
    UserDetail selectedUserCutBy, selectedUserCutCheckedBy, selectedUserDrillBy, selectedUserDrillCheckedBy;
    Bitmap bitmapSignatureCutBy, bitmapSignatureCutCheckedBy, bitmapSignatureDrillBy, bitmapSignatureDrillCheckedBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_part_detail);
        binding.setActivity(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        checker = new PermissionsChecker(getApplicationContext());
        appSession = new AppSession(this);

        getPartDetailsFromServer();

    }

    private boolean checkPermission() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {
            return true;
        } else {
            if (somePermissionsForeverDenied) {
                View contextView = binding.layoutMain;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
                return false;
            } else{
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                return false;
            }

        }
    }

    private void setDate() {

        binding.tvHeaderTitle.setText(Utility.getJobTitle(this, getIntent().getStringExtra(JOB_NAME), getIntent().getStringExtra(JOB_NUMBER)));
        binding.tvPartNoName.setText(Utility.getPartTitle(this, mPartDetailModel.getName(), mPartDetailModel.getPartNumber()));
        if (mPartDetailModel.getQuantity() != null)
            binding.tvQuantity.setText(String.valueOf(mPartDetailModel.getQuantity()));
           if (mPartDetailModel.getMaterialName() !=null){
               binding.tvMaterialName.setText(String.valueOf(mPartDetailModel.getMaterialName()));
           }else {
               binding.tvMaterialName.setText("");
           }

        binding.tvSectionSize.setText(mPartDetailModel.getSectionSize());
        binding.tvGrade.setText(mPartDetailModel.getGrade());
        binding.tvSGrade.setText(mPartDetailModel.getSGrade());
        if (mPartDetailModel.getPoNumber() != null)
            binding.tvPoNo.setText(String.valueOf(mPartDetailModel.getPoNumber()));
        binding.tvPartCompleted.setText(String.valueOf(mPartDetailModel.getPartsCompleted()));

        if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {
            binding.layoutCut.setVisibility(View.VISIBLE);
            binding.layoutDrill.setVisibility(View.VISIBLE);
            spinnersCategory = new Spinner[]{binding.spinnerCutBy, binding.spinnerCutCheckedBy, binding.spinnerDrillBy, binding.spinnerDrillCheckedBy};
            spinnersbackground = new RelativeLayout[]{binding.rlSpinnerCutByBackground, binding.rlSpinnerCutCheckedByBackground, binding.rlSpinnerDrillByBackground, binding.rlSpinnerDrillCheckedByBackground};
        } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {
            binding.layoutDrill.setVisibility(View.VISIBLE);
            spinnersCategory = new Spinner[]{binding.spinnerDrillBy, binding.spinnerDrillCheckedBy};
            spinnersbackground = new RelativeLayout[]{binding.rlSpinnerDrillByBackground, binding.rlSpinnerDrillCheckedByBackground};
        } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {
            binding.layoutCut.setVisibility(View.VISIBLE);
            spinnersCategory = new Spinner[]{binding.spinnerCutBy, binding.spinnerCutCheckedBy};
            spinnersbackground = new RelativeLayout[]{binding.rlSpinnerCutByBackground, binding.rlSpinnerCutCheckedByBackground};
        }

        switch (mPartDetailModel.getStatusId()) {
            case ACTION_PART_FAB_NOT_FULFILLED:
            case ACTION_PART_FAB_OPEN:
                getAllWorkersFromServer();

                /*for to show both category*/
                for (Spinner score : spinnersCategory) {
                    score.setEnabled(false);
                }
                binding.btnStart.setVisibility(View.VISIBLE);

                break;
            case ACTION_PART_FAB_COMPLETED:

                getAllWorkersFromServer();

                if (mPartDetailModel.getWorkerProductivities()!=null && Integer.parseInt(appSession.getUserDetails().getId())
                        == mPartDetailModel.getWorkerProductivities().get(0).getUserId()){

                    /*for to show enable spinner background*/
                    for (RelativeLayout spinnerBg : spinnersbackground) {
                        spinnerBg.setBackground(getDrawable(R.drawable.spinner_background_white));
                    }

                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.VISIBLE);

                    if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {
                        if (mPartDetailModel.getCutByUserId() != null) {
                            binding.spinnerCutBy.setEnabled(false);
                        }

                        if (mPartDetailModel.getCutCheckedByUserId() != null) {
                            binding.spinnerCutCheckedBy.setEnabled(false);
                        }
                        if (mPartDetailModel.getDrillByUserId() != null) {
                            binding.spinnerDrillBy.setEnabled(false);
                        }
                        if (mPartDetailModel.getDrillCheckedByUserId() != null) {
                            binding.spinnerDrillCheckedBy.setEnabled(false);
                        }

                        if (mPartDetailModel.getCutByUserId() != null && mPartDetailModel.getCutCheckedByUserId() != null
                                && mPartDetailModel.getDrillByUserId() != null && mPartDetailModel.getDrillCheckedByUserId() != null)
                            binding.btnCallToCheck.setEnabled(true);
                        else
                            binding.btnCallToCheck.setEnabled(false);

                    } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {

                        if (mPartDetailModel.getDrillByUserId() != null) {
                            binding.spinnerDrillBy.setEnabled(false);
                        }
                        if (mPartDetailModel.getDrillCheckedByUserId() != null) {
                            binding.spinnerDrillCheckedBy.setEnabled(false);
                        }

                        if (mPartDetailModel.getDrillByUserId() != null && mPartDetailModel.getDrillCheckedByUserId() != null)
                            binding.btnCallToCheck.setEnabled(true);
                        else
                            binding.btnCallToCheck.setEnabled(false);

                    } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {
                        if (mPartDetailModel.getCutByUserId() != null) {
                            binding.spinnerCutBy.setEnabled(false);
                        }
                        if (mPartDetailModel.getCutCheckedByUserId() != null) {
                            binding.spinnerCutCheckedBy.setEnabled(false);
                        }

                        if (mPartDetailModel.getCutByUserId() != null && mPartDetailModel.getCutCheckedByUserId() != null)
                            binding.btnCallToCheck.setEnabled(true);
                        else
                            binding.btnCallToCheck.setEnabled(false);
                    }
                }else{
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.GONE);

                    /*for to show enable spinner background*/
                    for (RelativeLayout spinnerBg : spinnersbackground) {
                        spinnerBg.setEnabled(false);
                    }

                    /*for to show both category*/
                    for (Spinner score : spinnersCategory) {
                        score.setEnabled(false);
                    }
                }

                break;

            case ACTION_PART_FAB_IN_PROGRESS:

                getAllWorkersFromServer();
                /*for to show both category*/
                for (Spinner score : spinnersCategory) {
                    score.setEnabled(false);
                }

                if (mPartDetailModel.getWorkerProductivities()!=null && Integer.parseInt(appSession.getUserDetails().getId()) == mPartDetailModel.getWorkerProductivities().get(0).getUserId()){
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.VISIBLE);
                    binding.btnPartCompleted.setVisibility(View.VISIBLE);
                    appSession.storeUserPartDetail(mPartDetailModel, ACTION_PART_FAB_IN_PROGRESS);
                }else{
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.GONE);
                }

                break;

            case ACTION_PART_FAB_READY_FOR_INSPECT:

                getAllWorkersFromServer();

                //for to show both category
                for (Spinner score : spinnersCategory) {
                    score.setEnabled(false);
                }

                binding.btnStart.setVisibility(View.GONE);
                binding.btnAbort.setVisibility(View.GONE);
                binding.btnPartCompleted.setVisibility(View.GONE);
                binding.btnCallToCheck.setVisibility(View.GONE);

                break;
        }

    }

    /*Aug 13, 2020 - common listener for all the spinner*/
    AdapterView.OnItemSelectedListener scoreListener =
            new AdapterView.OnItemSelectedListener() {

                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (position > 0)
                        if (checkPermission())
                        if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {
                            if (mPartDetailModel.getCutByUserId() == null && parent.getId() == R.id.spinnerCutBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerCutBy);
                            if (mPartDetailModel.getCutCheckedByUserId() == null && parent.getId() == R.id.spinnerCutCheckedBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerCutCheckedBy);
                            if (mPartDetailModel.getDrillByUserId() == null && parent.getId() == R.id.spinnerDrillBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerDrillBy);
                            if (mPartDetailModel.getDrillCheckedByUserId() == null && parent.getId() == R.id.spinnerDrillCheckedBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerDrillCheckedBy);
                        } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {
                            if (mPartDetailModel.getDrillByUserId() == null && parent.getId() == R.id.spinnerDrillBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerDrillBy);
                            if (mPartDetailModel.getDrillCheckedByUserId() == null && parent.getId() == R.id.spinnerDrillCheckedBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerDrillCheckedBy);
                        } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {
                            if (mPartDetailModel.getCutByUserId() == null && parent.getId() == R.id.spinnerCutBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerCutBy);
                            if (mPartDetailModel.getCutCheckedByUserId() == null && parent.getId() == R.id.spinnerCutCheckedBy)
                                Utility.getSignatureDialog(PartDetailActivity.this, PartDetailActivity.this,
                                        parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerCutCheckedBy);
                        } else {
                            removeSignatureForIndexZero(parent.getId());
                        }


                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    // TODO Auto-generated method stub.
                }
            };

    private void getAllWorkersFromServer() {
        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(TYPE, appSession.getUserDetails().getType());
                params.put(FILTER, jsonObject.toString());

                APIRequest.generalApiRequest(this, this, this, params, ServicesType.getAllWorkers, true);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateSignatureToServer(ArrayList<File> imagesArray) {
        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> param = new HashMap<String, String>();
            param.put(PART_ID, mPartDetailModel.getId().toString());
            //param.put(INSPECT_BY_USER_ID, "");

            if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {

                if (selectedUserCutBy != null)
                    param.put(CUT_BY_USER_ID, selectedUserCutBy.getId());

                if (selectedUserCutCheckedBy != null)
                    param.put(CUT_CHECKED_BY_USER_ID, selectedUserCutCheckedBy.getId());

                if (selectedUserDrillBy != null)
                    param.put(DRILL_BY_USER_ID, selectedUserDrillBy.getId());

                if (selectedUserDrillCheckedBy != null)
                    param.put(DRILL_CHECKED_BY_USER_ID, selectedUserDrillCheckedBy.getId());

            } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {

                if (selectedUserDrillBy != null)
                    param.put(DRILL_BY_USER_ID, selectedUserDrillBy.getId());

                if (selectedUserDrillCheckedBy != null)
                    param.put(DRILL_CHECKED_BY_USER_ID, selectedUserDrillCheckedBy.getId());

            } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {
                if (selectedUserCutBy != null)
                    param.put(CUT_BY_USER_ID, selectedUserCutBy.getId());

                if (selectedUserCutCheckedBy != null)
                    param.put(CUT_CHECKED_BY_USER_ID, selectedUserCutCheckedBy.getId());
            }

            APIRequest.makeRequestToServerNewJsonArrayParameter(true, getApplicationContext(), PartDetailActivity.this,
                    ServicesType.updateUserWithSignatureForPart, PartDetailActivity.this, imagesArray, param);


        }
    }

    private void getPartDetailsFromServer() {
        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();

            params.put(ID, getIntent().getStringExtra(PART_ID));
            APIRequest.generalApiRequest(this, this, this, params, ServicesType.getPartDetails, true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnStart:

                HashMap<String, String> paramStart = new HashMap<>();
                paramStart.put(PART_ID, getIntent().getStringExtra(PART_ID));
                paramStart.put("assignment_type", "PART");

                APIRequest.generalApiRequest(this, this, this, paramStart, ServicesType.workerProductivities, true);

                break;

            case R.id.btnAbort:
                intActionPerformed = 2;
                Utility.numberOfPartCompletedDialog(this, this);
                break;

            case R.id.btnPartCompleted:
                if (checkPermission()){
                    intActionPerformed = 3;

                    /*commented - Aug 01, 2020
                     * Not updating the Completed status over the server*/
                    /*HashMap<String, String> paramPartCompleted = new HashMap<>();
                    paramPartCompleted.put(ID, getIntent().getStringExtra(PART_ID));
                    paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_COMPLETED));

                    APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);*/
                    appSession.storeUserPartDetail(mPartDetailModel, ACTION_PART_FAB_IN_PROGRESS);//Aug 01, 2020 replace- ACTION_PART_FAB_COMPLETE status with ACTION_PART_FAB_IN_PROGRESS
                    /*for to show enable spinner background*/
                    for (RelativeLayout spinnerBg : spinnersbackground) {
                        spinnerBg.setBackground(getDrawable(R.drawable.spinner_background_white));
                    }

                    /*for to show both category*/
                    for (Spinner score : spinnersCategory) {
                        score.setEnabled(true);
                    }

                    /*Aug 01, 2020- Add the below both button over here from the ServicesType.updateFabAction*/
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.btnCallToCheck:
                intActionPerformed = 4;

                HashMap<String, String> paramCallToCheck = new HashMap<>();
                paramCallToCheck.put(ID, getIntent().getStringExtra(PART_ID));
                paramCallToCheck.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_READY_FOR_INSPECT));

                APIRequest.generalApiRequest(this, this, this, paramCallToCheck, ServicesType.updatePartAction, true);

                break;

            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.btnViewDrawing:
                if (checkPermission())
                startActivity(new Intent(this, ViewDrawingActivity.class)
                .putExtra("from", TYPE_ONLY_PART).putExtra(ID, getIntent().getStringExtra(PART_ID)));
                break;

        }

    }

    @Override
    public void onOkClicked(String strNoPartCompleted) {

        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            if (mPartDetailModel.getQuantity() < Integer.parseInt(strNoPartCompleted)) {
                Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.invalid_quantity));
            } else {
                HashMap<String, String> params = new HashMap<>();
                params.put(ID, getIntent().getStringExtra(PART_ID));
                params.put(PARTS_COMPLETED, strNoPartCompleted);
                params.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));

                APIRequest.generalApiRequest(this, this, this, params, ServicesType.updatePartAction, true);
                appSession.storeUserPartDetail(mPartDetailModel, ACTION_PART_FAB_NOT_FULFILLED);
                appSession.setPartAbortClicked(true);
            }

        }
    }

    /*Aug 13, 2020 - method to get the signature from the signature dialoge along with the spinnerId and user detail param*/
    @Override
    public void onOkClicked(Bitmap bitmapSignature, int spinnerId, UserDetail mUserDetail) {
        if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {
            switch (spinnerId) {
                case R.id.spinnerCutBy:
                    posToSetBitMapSiganture = 1;
                    selectedUserCutBy = mUserDetail;
                    bitmapSignatureCutBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureCutBy));
                    break;
                case R.id.spinnerCutCheckedBy:
                    posToSetBitMapSiganture = 2;
                    selectedUserCutCheckedBy = mUserDetail;
                    bitmapSignatureCutCheckedBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureCutCheckedBy));
                    break;
                case R.id.spinnerDrillBy:
                    posToSetBitMapSiganture = 3;
                    selectedUserDrillBy = mUserDetail;
                    bitmapSignatureDrillBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureDrillBy));
                    break;
                case R.id.spinnerDrillCheckedBy:
                    posToSetBitMapSiganture = 4;
                    selectedUserDrillCheckedBy = mUserDetail;
                    bitmapSignatureDrillCheckedBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureDrillCheckedBy));
                    break;
            }
        } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {
            switch (spinnerId) {
                case R.id.spinnerDrillBy:
                    posToSetBitMapSiganture = 3;
                    selectedUserDrillBy = mUserDetail;
                    bitmapSignatureDrillBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureDrillBy));
                    break;
                case R.id.spinnerDrillCheckedBy:
                    posToSetBitMapSiganture = 4;
                    selectedUserDrillCheckedBy = mUserDetail;
                    bitmapSignatureDrillCheckedBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureDrillCheckedBy));
                    break;
            }
        } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {
            switch (spinnerId) {
                case R.id.spinnerCutBy:
                    posToSetBitMapSiganture = 1;
                    selectedUserCutBy = mUserDetail;
                    bitmapSignatureCutBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureCutBy));
                    break;
                case R.id.spinnerCutCheckedBy:
                    posToSetBitMapSiganture = 2;
                    selectedUserCutCheckedBy = mUserDetail;
                    bitmapSignatureCutCheckedBy = bitmapSignature;
                    enableCallToCheckButton();
                    updateSignatureToServer(Utility.bitmapToFile(PartDetailActivity.this, bitmapSignatureCutCheckedBy));
                    break;
            }
        }
    }

    /*Aug 13, 2020 - method to get the signature only from the signature dialoge*/
    @Override
    public void onOkClicked(Bitmap bitmapSignature) {
        //only used to capture the image. we are using the above method with complete details
    }

    /*Aug 13, 2020 - method to set to default on cross click of the signature dialoge*/
    @Override
    public void onDismiss(Spinner mSpinner) {
        mSpinner.setSelection(0);
    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {
            case getAllWorkers:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<List<UserDetail>>() {
                        }.getType();

                        ArrayList<UserDetail> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(LIST), typeGson);

                        if (mJobList != null && !mJobList.isEmpty()) {

                            UserDetail objUserDetail = new UserDetail();
                            objUserDetail.setId("-1");
                            objUserDetail.setName(SELECT);
                            this.mWorkerList.add(objUserDetail);
                            this.mWorkerList.addAll(mJobList);

                            CustomWorkersSpinnerAdapter spinnerAdapter = new CustomWorkersSpinnerAdapter(this, R.layout.spinner_text, mWorkerList);

                            /*for to show both category*/
                            for (Spinner score : spinnersCategory) {
                                score.setAdapter(spinnerAdapter);
                                score.setOnItemSelectedListener(scoreListener);
                            }

                            switch (mPartDetailModel.getStatusId()) {

                                case ACTION_PART_FAB_COMPLETED:
                                case ACTION_PART_FAB_READY_FOR_INSPECT:
                                case ACTION_PART_FAB_IN_PROGRESS:

                                    setSignatureWithUser();

                                    break;
                            }

                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }

                break;

            case updateUserWithSignatureForPart:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        if (selectedUserCutBy!=null)
                        selectedUserCutBy = null;

                        if (selectedUserCutCheckedBy!=null)
                            selectedUserCutCheckedBy = null;

                        if (selectedUserDrillBy!=null)
                            selectedUserDrillBy = null;

                        if (selectedUserDrillCheckedBy!=null)
                            selectedUserDrillCheckedBy = null;

                        switch (posToSetBitMapSiganture) {
                            case 1:
                                binding.imgSignatureCutBy.setImageBitmap(bitmapSignatureCutBy);
                                break;
                            case 2:
                                binding.imgSignatureCutCheckedBy.setImageBitmap(bitmapSignatureCutCheckedBy);
                                break;
                            case 3:
                                binding.imgSignatureDrillBy.setImageBitmap(bitmapSignatureDrillBy);
                                break;
                            case 4:
                                binding.imgSignatureDrillCheckedBy.setImageBitmap(bitmapSignatureDrillCheckedBy);
                                break;
                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case updatePartAction:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        if (intActionPerformed == 1) {
                            binding.btnStart.setVisibility(View.GONE);
                            binding.btnAbort.setVisibility(View.VISIBLE);
                            binding.btnPartCompleted.setVisibility(View.VISIBLE);
                        } else if (intActionPerformed == 3) {
                            binding.btnPartCompleted.setVisibility(View.GONE);
                            binding.btnCallToCheck.setVisibility(View.VISIBLE);
                        } else if (intActionPerformed == 2) {
                            startActivity(new Intent(PartDetailActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else if (intActionPerformed == 4) {
                            if (showMessage)
                                Utility.toaster(this, KEY_SUCCESS, message);
                            appSession.storeUserPartDetail(mPartDetailModel, ACTION_PART_FAB_READY_FOR_INSPECT);
                            startActivity(new Intent(PartDetailActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case workerProductivities:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        appSession.storeUserPartDetail(mPartDetailModel, ACTION_PART_FAB_IN_PROGRESS);
                        if (appSession.isPartAbortClicked())
                            appSession.setPartAbortClicked(false);

                        binding.btnStart.setVisibility(View.GONE);
                        binding.btnAbort.setVisibility(View.VISIBLE);
                        binding.btnPartCompleted.setVisibility(View.VISIBLE);

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case getPartDetails:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<PartDetailModel>() {
                        }.getType();

                        mPartDetailModel = gson.fromJson(jsonObject.getJSONArray(RESPONSE_DATA).get(0).toString(), typeGson);
                        if (mPartDetailModel != null) {
                            setDate();
                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(this, KEY_ERROR, message);
    }

    /*Aug 13, 2020 - Method used to enable Call to Check button*/
    private void enableCallToCheckButton() {
        if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {

            if (bitmapSignatureCutBy == null && mPartDetailModel.getCutBySignature() == null
            || bitmapSignatureDrillBy == null && mPartDetailModel.getDrillBySignature() != null)
                binding.btnCallToCheck.setEnabled(false);
            /*else if (bitmapSignatureCutCheckedBy == null && mPartDetailModel.getCutCheckedBySignature() == null)
                binding.btnCallToCheck.setEnabled(false);
            else if (bitmapSignatureDrillBy == null && mPartDetailModel.getDrillBySignature() != null)
                binding.btnCallToCheck.setEnabled(false);
            else if (bitmapSignatureDrillCheckedBy == null && mPartDetailModel.getDrillCheckedBySignature() == null)
                binding.btnCallToCheck.setEnabled(false);*/
            else
                binding.btnCallToCheck.setEnabled(true);

        } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {

            if (bitmapSignatureDrillBy == null && mPartDetailModel.getDrillBySignature() == null)
                binding.btnCallToCheck.setEnabled(false);
            /*else if (bitmapSignatureDrillCheckedBy == null && mPartDetailModel.getDrillCheckedBySignature() == null)
                binding.btnCallToCheck.setEnabled(false);*/
            else
                binding.btnCallToCheck.setEnabled(true);

        } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {

            if (bitmapSignatureCutBy == null && mPartDetailModel.getCutBySignature() == null)
                binding.btnCallToCheck.setEnabled(false);
            /*else if (bitmapSignatureCutCheckedBy == null && mPartDetailModel.getCutCheckedBySignature() == null)
                binding.btnCallToCheck.setEnabled(false);*/
            else
                binding.btnCallToCheck.setEnabled(true);

        }
    }

    private void removeSignatureForIndexZero(int spinnerId) {
        if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {
            switch (spinnerId) {
                case R.id.spinnerCutBy:
                    selectedUserCutBy = null;
                    bitmapSignatureCutBy = null;
                    binding.imgSignatureCutBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
                case R.id.spinnerCutCheckedBy:
                    selectedUserCutCheckedBy = null;
                    bitmapSignatureCutCheckedBy = null;
                    binding.imgSignatureCutCheckedBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
                case R.id.spinnerDrillBy:
                    selectedUserDrillBy = null;
                    bitmapSignatureDrillBy = null;
                    binding.imgSignatureDrillBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
                case R.id.spinnerDrillCheckedBy:
                    selectedUserDrillCheckedBy = null;
                    bitmapSignatureDrillCheckedBy = null;
                    binding.imgSignatureDrillCheckedBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
            }
        } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {
            switch (spinnerId) {
                case R.id.spinnerDrillBy:
                    selectedUserDrillBy = null;
                    bitmapSignatureDrillBy = null;
                    binding.imgSignatureDrillBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
                case R.id.spinnerDrillCheckedBy:
                    selectedUserDrillCheckedBy = null;
                    bitmapSignatureDrillCheckedBy = null;
                    binding.imgSignatureDrillCheckedBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
            }
        } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {
            switch (spinnerId) {
                case R.id.spinnerCutBy:
                    selectedUserCutBy = null;
                    bitmapSignatureCutBy = null;
                    binding.imgSignatureCutBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
                case R.id.spinnerCutCheckedBy:
                    selectedUserCutCheckedBy = null;
                    bitmapSignatureCutCheckedBy = null;
                    binding.imgSignatureCutCheckedBy.setImageBitmap(null);
                    enableCallToCheckButton();
                    break;
            }
        }
    }
    /*https://stackoverflow.com/questions/6834354/how-to-capture-human-signature*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE_PERMISSIONS:

                if (!somePermissionsForeverDenied) {
                    if (checker.hasAllPermissionsGranted(grantResults)) {
                        somePermissionsForeverDenied = false;
                        /*permission granted*/
                        //openDialogForUploadPhoto();
                    } else {

                        for (String permission : permissions) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                                //denied
                                Log.e("denied", permission);
                            } else {
                                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                                    //allowed
                                    Log.e("allowed", permission);
                                } else {
                                    //set to never ask again
                                    Log.e("set to never ask again", permission);
                                    somePermissionsForeverDenied = true;

                                    break;
                                }
                            }
                        }

                    }
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }

    }

    /*Aug 13, 2020 - set signature with selected user in the spinner*/
    private void setSignatureWithUser() {

        if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {

            for (int i = 0; i < mWorkerList.size(); i++) {

                if (mPartDetailModel.getCutByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getCutByUserId()) {
                    binding.spinnerCutBy.setEnabled(false);
                    binding.spinnerCutBy.setSelection(i);
                }

                if (mPartDetailModel.getCutCheckedByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getCutCheckedByUserId()) {
                    binding.spinnerCutCheckedBy.setEnabled(false);
                    binding.spinnerCutCheckedBy.setSelection(i);
                }

                if (mPartDetailModel.getDrillByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getDrillByUserId()) {
                    binding.spinnerDrillBy.setEnabled(false);
                    binding.spinnerDrillBy.setSelection(i);
                }

                if (mPartDetailModel.getDrillCheckedByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getDrillCheckedByUserId()) {
                    binding.spinnerDrillCheckedBy.setEnabled(false);
                    binding.spinnerDrillCheckedBy.setSelection(i);
                }

            }

            if (mPartDetailModel.getCutBySignature() != null && !mPartDetailModel.getCutBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getCutBySignature(), binding.imgSignatureCutBy);

            if (mPartDetailModel.getCutCheckedBySignature() != null && !mPartDetailModel.getCutCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getCutCheckedBySignature(), binding.imgSignatureCutCheckedBy);

            if (mPartDetailModel.getDrillBySignature() != null && !mPartDetailModel.getDrillBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getDrillBySignature(), binding.imgSignatureDrillBy);

            if (mPartDetailModel.getDrillCheckedBySignature() != null && !mPartDetailModel.getDrillCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getDrillCheckedBySignature(), binding.imgSignatureDrillCheckedBy);

        } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {

            for (int i = 0; i < mWorkerList.size(); i++) {

                if (mPartDetailModel.getDrillByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getDrillByUserId()) {
                    binding.spinnerDrillBy.setEnabled(false);
                    binding.spinnerDrillBy.setSelection(i);
                }

                if (mPartDetailModel.getDrillCheckedByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getDrillCheckedByUserId()) {
                    binding.spinnerDrillCheckedBy.setEnabled(false);
                    binding.spinnerDrillCheckedBy.setSelection(i);
                }

            }

            if (mPartDetailModel.getDrillBySignature() != null && !mPartDetailModel.getDrillBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getDrillBySignature(), binding.imgSignatureDrillBy);

            if (mPartDetailModel.getDrillCheckedBySignature() != null && !mPartDetailModel.getDrillCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getDrillCheckedBySignature(), binding.imgSignatureDrillCheckedBy);

        } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {

            for (int i = 0; i < mWorkerList.size(); i++) {

                if (mPartDetailModel.getCutByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getCutByUserId()) {
                    binding.spinnerCutBy.setEnabled(false);
                    binding.spinnerCutBy.setSelection(i);
                }

                if (mPartDetailModel.getCutCheckedByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mPartDetailModel.getCutCheckedByUserId()) {
                    binding.spinnerCutCheckedBy.setEnabled(false);
                    binding.spinnerCutCheckedBy.setSelection(i);
                }

            }

            if (mPartDetailModel.getCutBySignature() != null && !mPartDetailModel.getCutBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getCutBySignature(), binding.imgSignatureCutBy);
            Log.e(TAG, "setSignatureWithUser: "+mPartDetailModel.getCutBySignature() );

            if (mPartDetailModel.getCutCheckedBySignature() != null && !mPartDetailModel.getCutCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailActivity.this, mPartDetailModel.getCutCheckedBySignature(), binding.imgSignatureCutCheckedBy);
            Log.e(TAG, "setSignatureWithUser: "+mPartDetailModel.getCutCheckedBySignature() );
        }

        enableCallToCheckButton();
    }

}