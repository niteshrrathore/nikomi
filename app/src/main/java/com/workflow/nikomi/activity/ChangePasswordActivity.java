package com.workflow.nikomi.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityChangePasswordBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.CustomSemiBoldTextView;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.workflow.nikomi.utils.Constant.EMAIL;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.OLD_PASSWORD;
import static com.workflow.nikomi.utils.Constant.PASSWORD;
import static com.workflow.nikomi.utils.Constant.PASSWORD_CONFIRMATION;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOKEN;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener, ServicesListener {

    ActivityChangePasswordBinding binding;
    boolean isChangeBtnClick = false;
    AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        * commented - Aug 13, 2020 Create custom header*/
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.header);
        View view = getSupportActionBar().getCustomView();

        ((CustomSemiBoldTextView) view.findViewById(R.id.tvHeaderTitle)).setText(getString(R.string.change_password));

        ImageView imageButton = (ImageView) view.findViewById(R.id.imgBack);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        binding.setActivity(this);
        appSession = new AppSession(this);

        setWatcherOnEditText();
    }

    private void setWatcherOnEditText() {

        binding.txtInputCurrentPassword.setTypeface(typeface);
        binding.txtInputCurrentPassword.getEditText().setTypeface(typeface);
        binding.txtInputNewPassword.setTypeface(typeface);
        binding.txtInputNewPassword.getEditText().setTypeface(typeface);
        binding.txtInputConfirmPassword.setTypeface(typeface);
        binding.txtInputConfirmPassword.getEditText().setTypeface(typeface);

        binding.etCurrentPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isChangeBtnClick) {
                    if (binding.etCurrentPassword.getText().toString().isEmpty()) {
                        binding.txtInputCurrentPassword.setError(getResources().getString(R.string.please_enter_password));
                    } else {
                        binding.txtInputCurrentPassword.setError(null);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

        binding.etNewPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isChangeBtnClick) {
                    if (binding.etNewPassword.getText().toString().isEmpty()) {
                        binding.txtInputNewPassword.setError(getResources().getString(R.string.please_enter_password));
                    } else if (!Utility.isValidPassword(binding.etNewPassword.getText().toString().trim())) {
                        binding.txtInputNewPassword.setError(getResources().getString(R.string.please_enter_valid_password));
                    } else
                        binding.txtInputNewPassword.setError(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

        binding.etConfirmPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isChangeBtnClick) {
                    if (!binding.txtInputNewPassword.getEditText().getText().toString().trim()
                            .equals(binding.txtInputConfirmPassword.getEditText().getText().toString().trim())) {
                        binding.txtInputConfirmPassword.setError(getString(R.string.confirm_password_not_match));
                    } else
                        binding.txtInputConfirmPassword.setError(null);

                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChange:
                isChangeBtnClick = true;
                if (checkValidData()){
                    HashMap<String, String> params = new HashMap<>();

                    params.put(OLD_PASSWORD, binding.etCurrentPassword.getText().toString().trim());
                    params.put(PASSWORD_CONFIRMATION, binding.etConfirmPassword.getText().toString().trim());
                    params.put(PASSWORD, binding.etNewPassword.getText().toString().trim());

                    APIRequest.generalApiRequest(getApplicationContext(), ChangePasswordActivity.this,
                            this, params, ServicesType.changePassword, true);

                    Utility.hideSoftKeyboard(ChangePasswordActivity.this);
                }
                break;

                case R.id.tvResetLink:
                isChangeBtnClick = false;
                    clearFields();
                break;
        }

    }

    private void clearFields(){
        binding.etCurrentPassword.setText("");
        binding.etNewPassword.setText("");
        binding.etConfirmPassword.setText("");
        binding.txtInputCurrentPassword.setError(null);
        binding.txtInputNewPassword.setError(null);
        binding.etCurrentPassword.requestFocus();
    }

    private boolean checkValidData() {

        if (binding.etCurrentPassword.getText().toString().isEmpty()) {

            binding.txtInputCurrentPassword.setError(getResources().getString(R.string.please_enter_current_password));
            return false;

        } else if (binding.etNewPassword.getText().toString().isEmpty()) {

            binding.txtInputNewPassword.setError(getResources().getString(R.string.please_enter_new_password));
            return false;

        } else if (!Utility.isValidPassword(binding.etNewPassword.getText().toString().trim())) {
            binding.txtInputNewPassword.setError(getResources().getString(R.string.please_enter_valid_password));
            return false;

        } else if (binding.etConfirmPassword.getText().toString().isEmpty()) {

            binding.txtInputConfirmPassword.setError(getResources().getString(R.string.please_enter_confirm_password));
            return false;

        }else if (!binding.txtInputNewPassword.getEditText().getText().toString().trim()
                .equals(binding.txtInputConfirmPassword.getEditText().getText().toString().trim())) {
            binding.txtInputConfirmPassword.setError(getResources().getString(R.string.confirm_password_not_match));
            return false;
        } else if (!Utility.isConnectingToInternet(getApplicationContext())) {

            Utility.toaster(ChangePasswordActivity.this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
            return false;

        }else {
            binding.txtInputCurrentPassword.setError(null);
            binding.txtInputNewPassword.setError(null);
            binding.txtInputConfirmPassword.setError(null);
            return true;
        }
    }

    @Override
    public void success(String response, ServicesType type) {
    switch (type){
        case changePassword:
            try {
                JSONObject jsonObject = new JSONObject(response);

                boolean statusCode = jsonObject.getBoolean(STATUS);
                boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                String message = jsonObject.optString(MESSAGE);
                String responseCode = jsonObject.optString(RESPONSE_CODE);

                if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {
                    JSONArray resultArray = jsonObject.getJSONArray(RESPONSE_DATA);
                    appSession.storeSessionToken(resultArray.getJSONObject(0).getString(TOKEN));

                    if (showMessage)
                        Utility.toaster(ChangePasswordActivity.this, KEY_SUCCESS, message);
                    isChangeBtnClick = true;
                    clearFields();

                } else {
                    if (showMessage)
                        Utility.toaster(ChangePasswordActivity.this, KEY_ERROR, message);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            break;
    }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(ChangePasswordActivity.this, KEY_ERROR, message);
    }

}