package com.workflow.nikomi.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.soundcloud.android.crop.Crop;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.adapters.InspectedPhotosAdapter;
import com.workflow.nikomi.adapters.PhotosAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityInspectPartDetailBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.CallBackSignatureSave;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.PartInspectPhotoModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.model.UserDetail;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.CompressFile;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_ONSITE;
import static com.workflow.nikomi.utils.Constant.FABRICATION_ID;
import static com.workflow.nikomi.utils.Constant.FILE_TYPE;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.INSPECT_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.PART_TYPE_BOTH;
import static com.workflow.nikomi.utils.Constant.PART_TYPE_CUT;
import static com.workflow.nikomi.utils.Constant.PART_TYPE_DRILL;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_FAB;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_PART;

public class PartDetailForInspectionActivity extends BaseActivity implements View.OnClickListener, CallBackSignatureSave, ServicesListener {

    private static final String TAG = "PartDetailForInspection";
    ActivityInspectPartDetailBinding binding;
    PartDetailModel mPartDetailModel;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean somePermissionsForeverDenied = false;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;
    public static final int TAKE_PIC_REQUEST_CODE = 4;
    public static final int CHOOSE_PIC_REQUEST_CODE = 3;
    ArrayList<File> imagesArray = new ArrayList<>();
    PhotosAdapter photosAdapter;
    InspectedPhotosAdapter mInspectedPhotosAdapter;
    AppSession appSession;
    Bitmap bitmapSignatureInspectBy;
    boolean checkIsInspectionDoneClicked = false;
    boolean checkIsUploadPictureClicked = false;
    boolean checkIsViewDrawingClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_inspect_part_detail);
        binding.setActivity(this);

        appSession = new AppSession(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        checker = new PermissionsChecker(getApplicationContext());

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.recyclerView.setLayoutManager(mLayoutManager);

        getPartDetailsFromServer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnInspectionDone:
                lacksPermissions = checker.lacksPermissions(PERMISSIONS);
                if (!lacksPermissions) {
                    if (imagesArray.isEmpty())
                        Utility.toaster(this, KEY_ERROR, getString(R.string.at_least_one_picture_required_for_inspect));
                    else
                        Utility.getSignatureDialog(this, this);
                } else {
                    checkIsInspectionDoneClicked = true;
                    checkIsUploadPictureClicked = false;
                    checkIsViewDrawingClicked = false;

                    if (somePermissionsForeverDenied) {
                        View contextView = binding.layoutMain;
                        Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(getColor(R.color.colorRed));
                        TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                        tv.setTextSize(14f);
                        snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", getPackageName(), null));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });

                        snackbar.show();
                    } else
                        requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                }

                break;

            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.btnUploadPicture:
                checkIsInspectionDoneClicked = false;
                checkIsUploadPictureClicked = true;
                checkIsViewDrawingClicked = false;
                openDialogForUploadPhoto();
                break;

            case R.id.btnViewDrawing:
                lacksPermissions = checker.lacksPermissions(PERMISSIONS);
                if (!lacksPermissions) {
                    startActivity(new Intent(this, ViewDrawingActivity.class)
                            .putExtra("from", TYPE_ONLY_PART)
                            .putExtra(ID, getIntent().getStringExtra(PART_ID)));
                } else {

                    checkIsInspectionDoneClicked = false;
                    checkIsUploadPictureClicked = false;
                    checkIsViewDrawingClicked = true;

                    if (somePermissionsForeverDenied) {
                        View contextView = binding.layoutMain;
                        Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(getColor(R.color.colorRed));
                        TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                        tv.setTextSize(14f);
                        snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", getPackageName(), null));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });

                        snackbar.show();
                    } else
                        requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                }

                break;

        }
    }

    private void getPartDetailsFromServer() {
        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();

            params.put(ID, getIntent().getStringExtra(PART_ID));
            APIRequest.generalApiRequest(this, this, this, params, ServicesType.getPartDetails, true);
        }
    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {

            case updateUserWithSignatureForPart:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {
                        HashMap<String, String> params = new HashMap<>();
                        params.put(ID, getIntent().getStringExtra(PART_ID));
                        params.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_READY_FOR_ONSITE));

                        APIRequest.generalApiRequest(this, this, this, params, ServicesType.updatePartAction, true);
                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case updatePartAction:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        startActivity(new Intent(PartDetailForInspectionActivity.this, MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case uploadPhotosForPart:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        HashMap<String, String> param = new HashMap<String, String>();
                        param.put(PART_ID, mPartDetailModel.getId().toString());
                        param.put(INSPECT_BY_USER_ID, appSession.getUserDetails().getId());
                        APIRequest.makeRequestToServerNewJsonArrayParameter(true, getApplicationContext(), PartDetailForInspectionActivity.this,
                                ServicesType.updateUserWithSignatureForPart, PartDetailForInspectionActivity.this, Utility.bitmapToFile(PartDetailForInspectionActivity.this, bitmapSignatureInspectBy), param);

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case getPartDetails:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<PartDetailModel>() {
                        }.getType();

                        mPartDetailModel = gson.fromJson(jsonObject.getJSONArray(RESPONSE_DATA).get(0).toString(), typeGson);
                        if (mPartDetailModel != null) {
                            setDate();
                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;
        }
    }

    private void setDate() {
        binding.tvHeaderTitle.setText(Utility.getJobTitle(this, getIntent().getStringExtra(JOB_NAME), getIntent().getStringExtra(JOB_NUMBER)));
        binding.tvPartNoName.setText(Utility.getPartTitle(this, mPartDetailModel.getName(), mPartDetailModel.getPartNumber()));
        if (mPartDetailModel.getQuantity() != null)
            binding.tvQuantity.setText(String.valueOf(mPartDetailModel.getQuantity()));
        if (mPartDetailModel.getMaterialName() !=null){
            binding.tvMaterialName.setText(String.valueOf(mPartDetailModel.getMaterialName()));
        }else {
            binding.tvMaterialName.setText("");
        }
        binding.tvSectionSize.setText(mPartDetailModel.getSectionSize());
        binding.tvGrade.setText(mPartDetailModel.getGrade());
        binding.tvSGrade.setText(mPartDetailModel.getSGrade());
        if (mPartDetailModel.getPoNumber() != null)
            binding.tvPoNo.setText(String.valueOf(mPartDetailModel.getPoNumber()));
        binding.tvPartCompleted.setText(String.valueOf(mPartDetailModel.getPartsCompleted()));

        /*Aug 13, 2020 - check part type*/
        if (mPartDetailModel.getType().equals(PART_TYPE_BOTH)) {
            binding.layoutCut.setVisibility(View.VISIBLE);
            binding.layoutDrill.setVisibility(View.VISIBLE);

            binding.tvCutByUserName.setText(mPartDetailModel.getCutByUser().getName());
            binding.tvCutCheckedByUserName.setText(mPartDetailModel.getCutCheckedByUser().getName());
            if(mPartDetailModel.getDrillByUser()!=null)
            binding.tvDrillByUserName.setText(mPartDetailModel.getDrillByUser().getName());
            if(mPartDetailModel.getDrillCheckedByUser()!=null)
            binding.tvDrillCheckedByUserName.setText(mPartDetailModel.getDrillCheckedByUser().getName());

            if (mPartDetailModel.getCutBySignature() != null && !mPartDetailModel.getCutBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this,
                        mPartDetailModel.getCutBySignature(), binding.imgSignatureCutBy);

            if (mPartDetailModel.getCutCheckedBySignature() != null && !mPartDetailModel.getCutCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this,
                        mPartDetailModel.getCutCheckedBySignature(), binding.imgSignatureCutCheckedBy);

            if (mPartDetailModel.getDrillBySignature() != null && !mPartDetailModel.getDrillBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this,
                        mPartDetailModel.getDrillBySignature(), binding.imgSignatureDrillBy);

            if (mPartDetailModel.getDrillCheckedBySignature() != null && !mPartDetailModel.getDrillCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this,
                        mPartDetailModel.getDrillCheckedBySignature(), binding.imgSignatureDrillCheckedBy);

        } else if (mPartDetailModel.getType().equals(PART_TYPE_DRILL)) {
            binding.layoutDrill.setVisibility(View.VISIBLE);
            binding.viewSeprator.setVisibility(View.GONE);
            binding.layoutCut.setVisibility(View.GONE);

            binding.tvDrillByUserName.setText(mPartDetailModel.getDrillByUser().getName());
            if(mPartDetailModel.getDrillCheckedByUser()!=null)
            binding.tvDrillCheckedByUserName.setText(mPartDetailModel.getDrillCheckedByUser().getName());

            if (mPartDetailModel.getDrillBySignature() != null && !mPartDetailModel.getDrillBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this, mPartDetailModel.getDrillBySignature(), binding.imgSignatureDrillBy);

            if (mPartDetailModel.getDrillCheckedBySignature() != null && !mPartDetailModel.getDrillCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this, mPartDetailModel.getDrillCheckedBySignature(), binding.imgSignatureDrillCheckedBy);

        } else if (mPartDetailModel.getType().equals(PART_TYPE_CUT)) {
            binding.layoutCut.setVisibility(View.VISIBLE);
            binding.viewSeprator.setVisibility(View.GONE);
            binding.layoutDrill.setVisibility(View.GONE);

            binding.tvCutByUserName.setText(mPartDetailModel.getCutByUser().getName());
            if(mPartDetailModel.getCutCheckedByUser()!=null)
            binding.tvCutCheckedByUserName.setText(mPartDetailModel.getCutCheckedByUser().getName());

            if (mPartDetailModel.getCutBySignature() != null && !mPartDetailModel.getCutBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this, mPartDetailModel.getCutBySignature(), binding.imgSignatureCutBy);

            if (mPartDetailModel.getCutCheckedBySignature() != null && !mPartDetailModel.getCutCheckedBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this, mPartDetailModel.getCutCheckedBySignature(), binding.imgSignatureCutCheckedBy);

        }

        if (mPartDetailModel.getStatusId() == ACTION_PART_FAB_READY_FOR_ONSITE) {
            binding.btnUploadPicture.setVisibility(View.GONE);
            binding.btnInspectionDone.setVisibility(View.GONE);
            binding.layoutInpectedBy.setVisibility(View.VISIBLE);

            binding.tvInspectByUserName.setText(mPartDetailModel.getInspectByUser().getName());

            if (mPartDetailModel.getInspectBySignature() != null && !mPartDetailModel.getInspectBySignature().isEmpty())
                Utility.loadImageFromPicasso(PartDetailForInspectionActivity.this, mPartDetailModel.getInspectBySignature(), binding.imgSignatureInspectBy);

            ArrayList<PartInspectPhotoModel> imagesArray = mPartDetailModel.getPartFiles();
            /*Aug 13, 2020 - adapter to show the image getting in the response*/
            mInspectedPhotosAdapter = new InspectedPhotosAdapter(this, imagesArray);
            binding.recyclerView.setAdapter(mInspectedPhotosAdapter);

        } else {
            /*Aug 13, 2020 - adapter to show the image whose type is File*/
            photosAdapter = new PhotosAdapter(this, imagesArray);
            binding.recyclerView.setAdapter(photosAdapter);
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(this, KEY_ERROR, message);
    }

    /*Aug 13, 2020 - method to get the signature from the signature dialoge along with the spinnerId and user detail param*/
    @Override
    public void onOkClicked(Bitmap bitmapSignature, int spinnerId, UserDetail mUserDetail) {

    }

    /*Aug 13, 2020 - method to get the signature only from the signature dialoge*/
    @Override
    public void onOkClicked(Bitmap bitmapSignature) {
        bitmapSignatureInspectBy = bitmapSignature;
        HashMap<String, String> param = new HashMap<String, String>();
        param.put(PART_ID, mPartDetailModel.getId().toString());
        param.put(FILE_TYPE, "INSPECTION");
        APIRequest.makeRequestToServerNewJsonArrayParameter(true, getApplicationContext(), PartDetailForInspectionActivity.this,
                ServicesType.uploadPhotosForPart, PartDetailForInspectionActivity.this, imagesArray, param);
    }

    /*Aug 13, 2020 - method to set to default on cross click of the signature dialoge*/
    @Override
    public void onDismiss(Spinner mSpinner) {

    }

    private void openDialogForUploadPhoto() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {

            //show dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(PartDetailForInspectionActivity.this,
                    R.style.AlertDialogCustom));
            builder.setTitle(getString(R.string.upload_or_take_photo));
            builder.setPositiveButton(getString(R.string.upload), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(PartDetailForInspectionActivity.this, AlbumSelectActivity.class);
                    intent.setType("image/*");
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                    startActivityForResult(intent, CHOOSE_PIC_REQUEST_CODE);

                }
            });

            builder.setNegativeButton(getString(R.string.take_photo), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, TAKE_PIC_REQUEST_CODE);

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        } else {
            if (somePermissionsForeverDenied) {
                View contextView = binding.layoutMain;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
            } else
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == CHOOSE_PIC_REQUEST_CODE) {
                if (data == null) {
                    Toast.makeText(getApplicationContext(), "Image cannot be null!", Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

                    for (int i = 0; i < images.size(); i++) {
                        File file = new File((images.get(i)).path);
                        imagesArray.add(CompressFile.getCompressedImageFile(file, PartDetailForInspectionActivity.this));
                        /*Uri imageUri = Uri.fromFile(file);
                        beginCrop(imageUri);*/
                        break;

                    }

                    /*uploadData(imagesArray);*/

                }

                photosAdapter.updateList(imagesArray);

            } else if (requestCode == TAKE_PIC_REQUEST_CODE) {

                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        imagesArray.add(CompressFile.getCompressedImageFile(f, PartDetailForInspectionActivity.this));
                        /*Uri imageUri = Uri.fromFile(f);
                        beginCrop(imageUri);*/
                        break;
                    }
                }
                /*if (!imagesArray.isEmpty())
                    uploadData(imagesArray);*/
                photosAdapter.updateList(imagesArray);
            }/*if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }*/

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE_PERMISSIONS:

                if (!somePermissionsForeverDenied) {
                    if (checker.hasAllPermissionsGranted(grantResults)) {
                        somePermissionsForeverDenied = false;
                        if (checkIsInspectionDoneClicked){
                            if (imagesArray.isEmpty())
                                Utility.toaster(this, KEY_ERROR, getString(R.string.at_least_one_picture_required_for_inspect));
                            else
                                Utility.getSignatureDialog(this, this);
                        }else if (checkIsUploadPictureClicked){
                            openDialogForUploadPhoto();
                        }else if (checkIsViewDrawingClicked){
                            startActivity(new Intent(this, ViewDrawingActivity.class)
                                    .putExtra("from", TYPE_ONLY_PART)
                                    .putExtra(ID, getIntent().getStringExtra(PART_ID)));
                        }

                    } else {

                        for (String permission : permissions) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                                //denied
                                Log.e("denied", permission);
                            } else {
                                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                                    //allowed
                                    Log.e("allowed", permission);
                                } else {
                                    //set to never ask again
                                    Log.e("set to never ask again", permission);
                                    somePermissionsForeverDenied = true;

                                    break;
                                }
                            }
                        }

                    }
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }

    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            //binding.imgUserProfile.setImageDrawable(null);
            //binding.imgUserProfile.setImageURI(Crop.getOutput(result));

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}