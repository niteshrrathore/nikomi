package com.workflow.nikomi.activity;


import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.BuildConfig;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityLoginBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.CallbackOKCancelInterface;
import com.workflow.nikomi.interfaces.CallbackOkInterface;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.JobPartStatusModel;
import com.workflow.nikomi.model.UserDetail;
import com.workflow.nikomi.model.VersionCodeModel;
import com.workflow.nikomi.utils.App;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.App.DEV_BASE_URL;
import static com.workflow.nikomi.utils.Constant.ANDROID;
import static com.workflow.nikomi.utils.Constant.AVTAR;
import static com.workflow.nikomi.utils.Constant.CREATED_BY;
import static com.workflow.nikomi.utils.Constant.DELETE_STATUS;
import static com.workflow.nikomi.utils.Constant.DEVICE_TYPE;
import static com.workflow.nikomi.utils.Constant.EMAIL;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.IS_ACTIVE;
import static com.workflow.nikomi.utils.Constant.JOB_PART_STATUS;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.MOBILE;
import static com.workflow.nikomi.utils.Constant.NAME;
import static com.workflow.nikomi.utils.Constant.PASSWORD;
import static com.workflow.nikomi.utils.Constant.PLAY_STORE_REQUEST_CODE_LOGIN;
import static com.workflow.nikomi.utils.Constant.PROFILE_URL;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOKEN;
import static com.workflow.nikomi.utils.Constant.TYPE;
import static com.workflow.nikomi.utils.Constant.UPDATED_AT;
import static com.workflow.nikomi.utils.App.Live_URL;

public class LoginActivity extends BaseActivity implements View.OnClickListener, ServicesListener {

    private static final String TAG = "LoginActivity";
    ActivityLoginBinding binding;
    boolean isLoginBtnClick = false;
    boolean isSubmitBtnClick = false;
    boolean hideShowPass = true;
    HashMap<String, String> params;
    AppSession appSession;
    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //finally change the color
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent));

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setActivity(this);
        mContext = this;
        /*Aug 13, 2020 - Dynamically change the BASE_URL*/
        /*binding.layoutMain.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (changeAppBaseUrl){
                    changeAppBaseUrl = false;
                    appSession.storeBaseUrl(DEV_BASE_URL);
                    Live_URL = DEV_BASE_URL;
                    Utility.toaster(LoginActivity.this,KEY_SUCCESS,"Changed to Dev Env.");
                }else{
                    changeAppBaseUrl = true;
                    appSession.storeBaseUrl(QA_BASE_URL);
                    Live_URL = QA_BASE_URL;
                    Utility.toaster(LoginActivity.this,KEY_SUCCESS,"Changed to QA Env.");
                }

                return false;
            }
        });*/

        /*Aug 13, 2020 - Custom method to manage hide/show password and set icon Acc. to state*/
        setHideShowPass();
        setWatcherOnEditText();

        /*Aug 13, 2020 - Get firebase token */
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (task.isSuccessful()) {
                            // Get new Instance ID token
                            String token = task.getResult().getToken();

                            Log.e(TAG, "token: >>>>>>>>>" + token);

                            // TODO: Implement this method to send token to your app server.
                            appSession.storeUserDeviceToken(token);

                            return;
                        }

                    }
                });

    }

    private void setHideShowPass() {
        if (hideShowPass) {
            binding.inputPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            binding.imgHideShow.setImageDrawable(getDrawable(R.drawable.ic_show_icon_svg));
        } else {
            binding.inputPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            binding.imgHideShow.setImageDrawable(getDrawable(R.drawable.ic_hide_iconsvg));
        }
        binding.inputPassword.setSelection(binding.inputPassword.getText().length());
    }

    private void setWatcherOnEditText() {
        binding.inputEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (binding.lyLogin.getVisibility() == View.VISIBLE && isLoginBtnClick) {
                    if (binding.inputEmail.getText().toString().isEmpty()) {

                        binding.tvEmailError.setVisibility(View.VISIBLE);
                        binding.tvEmailError.setText(getResources().getString(R.string.please_enter_email_address));

                    } else if (!Utility.isValidEmail(binding.inputEmail.getText().toString().trim())) {

                        binding.tvEmailError.setVisibility(View.VISIBLE);
                        binding.tvEmailError.setText(getResources().getString(R.string.please_a_enter_valid_email_address));

                    } else {
                        if (binding.tvEmailError.getVisibility() == View.VISIBLE)
                            binding.tvEmailError.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }

        });

        binding.inputForgotEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (binding.lyForgotPassword.getVisibility() == View.VISIBLE && isSubmitBtnClick) {
                    if (binding.inputForgotEmail.getText().toString().isEmpty()) {

                        binding.tvForgotEmailError.setVisibility(View.VISIBLE);
                        binding.tvForgotEmailError.setText(getResources().getString(R.string.please_enter_email_address));

                    } else if (!Utility.isValidEmail(binding.inputForgotEmail.getText().toString().trim())) {

                        binding.tvForgotEmailError.setVisibility(View.VISIBLE);
                        binding.tvForgotEmailError.setText(getResources().getString(R.string.please_a_enter_valid_email_address));

                    } else {
                        if (binding.tvForgotEmailError.getVisibility() == View.VISIBLE)
                            binding.tvForgotEmailError.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

        binding.inputPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (binding.lyLogin.getVisibility() == View.VISIBLE && isLoginBtnClick) {
                    if (binding.tvPasswordError.getText().toString().isEmpty()) {

                        binding.tvPasswordError.setVisibility(View.VISIBLE);
                        binding.tvPasswordError.setText(getResources().getString(R.string.please_enter_password));

                    } else {
                        if (binding.tvPasswordError.getVisibility() == View.VISIBLE)
                            binding.tvPasswordError.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgHideShow:

                if (hideShowPass)
                    hideShowPass = false;
                else
                    hideShowPass = true;

                setHideShowPass();

                break;

            case R.id.layout_main:

                Utility.hideSoftKeyboard(LoginActivity.this);

                break;

            case R.id.btnLogin:

                isLoginBtnClick = true;
                isSubmitBtnClick = false;
                if (checkValidData()) {
                    params = new HashMap<>();

                    params.put(EMAIL, binding.inputEmail.getText().toString().trim());
                    params.put(PASSWORD, binding.inputPassword.getText().toString());

                    Utility.hideSoftKeyboard(LoginActivity.this);

                    APIRequest.generalApiRequest(getApplicationContext(), LoginActivity.this,
                            this, params, ServicesType.login, true);
                }

                break;

            case R.id.tvForgetLink:
                isLoginBtnClick = false;
                isSubmitBtnClick = false;
                if (binding.tvEmailError.getVisibility() == View.VISIBLE)
                    binding.tvEmailError.setVisibility(View.GONE);

                if (binding.tvPasswordError.getVisibility() == View.VISIBLE)
                    binding.tvPasswordError.setVisibility(View.GONE);

                changeLayout(false);

                break;

            case R.id.tvBackToLogin:
                isLoginBtnClick = false;
                isSubmitBtnClick = false;
                if (binding.tvForgotEmailError.getVisibility() == View.VISIBLE)
                    binding.tvForgotEmailError.setVisibility(View.INVISIBLE);

                changeLayout(true);

                break;

            case R.id.btnSubmit:
                isLoginBtnClick = false;
                isSubmitBtnClick = true;

                if (checkForgotPassValidation()) {

                    params = new HashMap<>();

                    params.put(EMAIL, binding.inputForgotEmail.getText().toString().trim());

                    Utility.hideSoftKeyboard(LoginActivity.this);

                    APIRequest.generalApiRequest(getApplicationContext(), LoginActivity.this,
                            this, params, ServicesType.forgotPassword, true);

                }

                break;
        }
    }

    private void changeLayout(boolean showLogin) {

        if (showLogin) {
            binding.tvHeaderTitle.setText(getString(R.string.login));
            binding.lyForgotPassword.setVisibility(View.GONE);
            binding.inputForgotEmail.setText("");
            binding.lyLogin.setVisibility(View.VISIBLE);
        } else {
            binding.tvHeaderTitle.setText(getString(R.string.forgot_password_title));
            binding.lyLogin.setVisibility(View.GONE);
            binding.lyForgotPassword.setVisibility(View.VISIBLE);
        }

    }

    private boolean checkForgotPassValidation() {

        if (binding.inputForgotEmail.getText().toString().isEmpty()) {

            binding.tvForgotEmailError.setVisibility(View.VISIBLE);
            binding.tvForgotEmailError.setText(getResources().getString(R.string.please_enter_email_address));
            return false;

        } else if (!Utility.isValidEmail(binding.inputForgotEmail.getText().toString().trim())) {

            binding.tvForgotEmailError.setVisibility(View.VISIBLE);
            binding.tvForgotEmailError.setText(getResources().getString(R.string.please_a_enter_valid_email_address));
            return false;

        } else if (!Utility.isConnectingToInternet(getApplicationContext())) {

            Utility.toaster(LoginActivity.this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
            return false;

        } else {

            if (binding.tvForgotEmailError.getVisibility() == View.VISIBLE)
                binding.tvForgotEmailError.setVisibility(View.INVISIBLE);
            return true;

        }

    }

    private boolean checkValidData() {

        if (binding.inputEmail.getText().toString().isEmpty()) {

            binding.tvEmailError.setVisibility(View.VISIBLE);
            binding.tvEmailError.setText(getResources().getString(R.string.please_enter_email_address));
            return false;

        } else if (!Utility.isValidEmail(binding.inputEmail.getText().toString().trim())) {

            binding.tvEmailError.setVisibility(View.VISIBLE);
            binding.tvEmailError.setText(getResources().getString(R.string.please_a_enter_valid_email_address));
            return false;

        } else if (binding.inputPassword.getText().toString().isEmpty()) {

            binding.tvPasswordError.setVisibility(View.VISIBLE);
            binding.tvPasswordError.setText(getResources().getString(R.string.please_enter_password));
            return false;

        } else if (!Utility.isConnectingToInternet(getApplicationContext())) {

            Utility.toaster(LoginActivity.this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
            return false;

        } else {
            if (binding.tvEmailError.getVisibility() == View.VISIBLE)
                binding.tvEmailError.setVisibility(View.GONE);

            if (binding.tvPasswordError.getVisibility() == View.VISIBLE)
                binding.tvPasswordError.setVisibility(View.GONE);

            return true;
        }

    }

    private void geVersionUpdateDetails() {
        HashMap<String, String> params = new HashMap<>();
        params.put(DEVICE_TYPE, ANDROID);
        // params.put(PASSWORD, binding.inputPassword.getText().toString());
        APIRequest.generalApiRequest(getApplicationContext(), this,
                this, params, ServicesType.getVersionCodeDetails, true);
    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {
            case login:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        JSONArray resultArray = jsonObject.getJSONArray(RESPONSE_DATA);

                        if (resultArray.length() > 0) {

                            UserDetail objUserDetail = new UserDetail();
                            objUserDetail.setId(resultArray.getJSONObject(0).getString(ID));
                            objUserDetail.setName(resultArray.getJSONObject(0).getString(NAME));
                            objUserDetail.setEmail(resultArray.getJSONObject(0).getString(EMAIL));
                            objUserDetail.setType(resultArray.getJSONObject(0).getString(TYPE));
                            objUserDetail.setAvatar(resultArray.getJSONObject(0).getString(AVTAR));
                            objUserDetail.setCreated_by(resultArray.getJSONObject(0).getInt(CREATED_BY));
                            objUserDetail.setDelete_status(resultArray.getJSONObject(0).getInt(DELETE_STATUS));
                            objUserDetail.setIs_active(resultArray.getJSONObject(0).getInt(IS_ACTIVE));
                            objUserDetail.setUpdated_at(resultArray.getJSONObject(0).getString(UPDATED_AT));
                            objUserDetail.setProfile_url(resultArray.getJSONObject(0).getString(PROFILE_URL));
                            objUserDetail.setMobile(resultArray.getJSONObject(0).getString(MOBILE));

                            appSession.storeSessionToken(resultArray.getJSONObject(0).getString(TOKEN));
                            appSession.storeUser(objUserDetail);
                            appSession.setUserLogin(true);

                            HashMap<String, String> param = new HashMap<>();
                            APIRequest.generalApiRequest(LoginActivity.this, LoginActivity.this,
                                    LoginActivity.this, param, ServicesType.getMasterData, false);


                        }

                        if (showMessage)
                            Utility.toaster(LoginActivity.this, KEY_SUCCESS, message);

                    } else {
                        if (showMessage)
                            Utility.toaster(LoginActivity.this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case forgotPassword:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        if (showMessage)
                            Utility.toaster(LoginActivity.this, KEY_SUCCESS, message);
                        changeLayout(true);

                    } else {
                        if (showMessage)
                            Utility.toaster(LoginActivity.this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case getMasterData:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<List<JobPartStatusModel>>() {
                        }.getType();

                        ArrayList<JobPartStatusModel> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(JOB_PART_STATUS), typeGson);
                        appSession.storeJobPartStatus(gson.toJson(mJobList));
                        mJobList.clear();

                        //Constant.SHIFT_HOURS = 2/*jsonObject.getJSONObject(RESPONSE_DATA).getInt("shift_hours")*/;
                        //startService(new Intent(this, AutoSignoutPopupBroadcastService.class));

                        long shiftHoursInMiliSec = jsonObject.getJSONObject(RESPONSE_DATA).getInt("shift_hours") * 60 * 60 * 1000;
                        //  long shiftHoursInMiliSec = 1 * 60*60*1000;
                        shiftHoursInMiliSec = System.currentTimeMillis() + shiftHoursInMiliSec;
                        //appSession.storeShiftHour(String.valueOf(jsonObject.getJSONObject(RESPONSE_DATA).getInt("shift_hours")));
                        appSession.storeShiftHour(shiftHoursInMiliSec + "");
                        startAutoSignoutService();

                            /*startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
*/                     // callHomeScreen();
                       geVersionUpdateDetails();
                    } else {
                        if (showMessage)
                            Utility.toaster(LoginActivity.this, KEY_ERROR, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(LoginActivity.this, KEY_ERROR, getString(R.string.something_went_wrong));
                }

                break;

            case getVersionCodeDetails:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {
                        JSONObject responseObject = jsonObject.getJSONObject(RESPONSE_DATA);
                        long versionCode = Utility.getVersionCode(this);
                        long hardVersionCode = responseObject.getLong("hard_version");
                        long softVersionCode = responseObject.getLong("soft_version");

                        if (hardVersionCode > versionCode) {

                            Utility.showAlertYesCallBackForUpdate(mContext, mContext.getString(R.string.app_update_hard), mContext.getString(R.string.update), new CallbackOkInterface() {
                                @Override
                                public void onSuccess(boolean isSuccess) {
                                    openPlayStore();
                                }
                            });
                            return;

                        } else if (softVersionCode > versionCode) {

                            Utility.showAlertYesNoCallBackForUpdate(mContext, mContext.getString(R.string.app_update_soft), mContext.getString(R.string.update), mContext.getString(R.string.cancel), new CallbackOKCancelInterface() {
                                @Override
                                public void onSuccess(boolean isSuccess) {
                                    Utility.openPlayStoreSoftUpdate(mContext, LoginActivity.this);
                                }

                                @Override
                                public void onFailure(boolean isSuccess) {
                                    callHomeScreen();
                                }
                            });

                        } else {
                            callHomeScreen();

                        }
                    } else {
                        if (showMessage)
                            Utility.toaster(LoginActivity.this, KEY_ERROR, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(LoginActivity.this, KEY_ERROR, getString(R.string.something_went_wrong));
                }

                break;
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(LoginActivity.this, KEY_ERROR, message);
    }

    @Override
    public void onResume() {
        super.onResume();
        appSession = new AppSession(getApplicationContext());
        if (appSession.getBaseUrl().isEmpty()) {
            appSession.storeBaseUrl(DEV_BASE_URL);
            Live_URL = DEV_BASE_URL;
        } else {
            Live_URL = appSession.getBaseUrl();
        }
    }

    private void callHomeScreen() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLAY_STORE_REQUEST_CODE_LOGIN) {
            try {
                getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_ACTIVITIES);
                geVersionUpdateDetails();
                //App Installed
            } catch (PackageManager.NameNotFoundException e) {
                //App doesn't installed
                geVersionUpdateDetails();
            }
        }
    }
}