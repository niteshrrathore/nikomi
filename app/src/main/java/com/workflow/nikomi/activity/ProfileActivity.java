package com.workflow.nikomi.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.material.snackbar.Snackbar;
import com.soundcloud.android.crop.Crop;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityProfileBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.UserDetail;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.CompressFile;
import com.workflow.nikomi.utils.CustomSemiBoldTextView;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.workflow.nikomi.utils.Constant.AVTAR;
import static com.workflow.nikomi.utils.Constant.CREATED_BY;
import static com.workflow.nikomi.utils.Constant.DELETE_STATUS;
import static com.workflow.nikomi.utils.Constant.EMAIL;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.IS_ACTIVE;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.MOBILE;
import static com.workflow.nikomi.utils.Constant.NAME;
import static com.workflow.nikomi.utils.Constant.PASSWORD;
import static com.workflow.nikomi.utils.Constant.PROFILE_URL;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOKEN;
import static com.workflow.nikomi.utils.Constant.TYPE;
import static com.workflow.nikomi.utils.Constant.UPDATED_AT;

public class ProfileActivity extends BaseActivity implements View.OnClickListener, ServicesListener {

    private static final String TAG = "ProfileActivity";

    ActivityProfileBinding binding;
    AppSession appSession;

    static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean somePermissionsForeverDenied = false;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;
    public static final int TAKE_PIC_REQUEST_CODE = 4;
    public static final int CHOOSE_PIC_REQUEST_CODE = 3;

    SpannableString mSpannableUnderlinedText;
    ArrayList<File> imagesArray;
    boolean isButtonSaveClicked = false;


    private void setProfileDataFromIntent() {

        binding.txtInputFullName.setTypeface(typeface);
        binding.txtInputFullName.getEditText().setTypeface(typeface);
        binding.txtInputEmail.setTypeface(typeface);
        binding.txtInputEmail.getEditText().setTypeface(typeface);
        binding.txtInputMobile.setTypeface(typeface);
        binding.txtInputMobile.getEditText().setTypeface(typeface);

        binding.etFullName.setText(appSession.getUserDetails().getName());
        binding.etEmail.setText(appSession.getUserDetails().getEmail());
        binding.etMobile.setText(appSession.getUserDetails().getMobile());

        if(appSession.getUserDetails().getProfile_url().length()>0){
            mSpannableUnderlinedText = new SpannableString(getString(R.string.change_pic));
            Utility.loadImageFromPicasso(this,appSession.getUserDetails().getProfile_url(), getDrawable(R.drawable.profile_placeholder), binding.imgUserProfile);
        }else
            mSpannableUnderlinedText = new SpannableString(getString(R.string.upload_pic));

        mSpannableUnderlinedText.setSpan(new UnderlineSpan(), 0, mSpannableUnderlinedText.length(), 0);
        binding.tvChangePic.setText(mSpannableUnderlinedText);

        binding.etFullName.setSelection(binding.etFullName.getText().length());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        /*Aug 13, 2020 - Create custom header*/
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.header);
        View view = getSupportActionBar().getCustomView();

        ((CustomSemiBoldTextView) view.findViewById(R.id.tvHeaderTitle)).setText(getString(R.string.profile));

        ImageView imageButton = (ImageView) view.findViewById(R.id.imgBack);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        binding.setActivity(this);
        //Crop.pickImage(this);
        appSession = new AppSession(this);
        checker = new PermissionsChecker(getApplicationContext());
        getProfileData();
        setWatcherOnEditText();
    }

    private void getProfileData() {

        APIRequest.generalApiRequest(getApplicationContext(), ProfileActivity.this,
                ProfileActivity.this, new HashMap<String, String>(), ServicesType.getProfile, true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSave:
                isButtonSaveClicked = true;
                if (checkValidData()){
                    if (imagesArray == null)
                        imagesArray = new ArrayList<>();

                    uploadData(imagesArray);
                }

                break;

                case R.id.tvChangePic:
                    openDialogForUploadPhoto();
                break;
        }
    }

    private void openDialogForUploadPhoto(){
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {

            //show dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ProfileActivity.this,
                    R.style.AlertDialogCustom));
            builder.setTitle(getString(R.string.upload_or_take_photo));
            builder.setPositiveButton(getString(R.string.upload), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(ProfileActivity.this, AlbumSelectActivity.class);
                    intent.setType("image/*");
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                    startActivityForResult(intent, CHOOSE_PIC_REQUEST_CODE);

                }
            });

            builder.setNegativeButton(getString(R.string.take_photo), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, TAKE_PIC_REQUEST_CODE);

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        }else{
            if (somePermissionsForeverDenied){
                View contextView = binding.content;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
            }else
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == CHOOSE_PIC_REQUEST_CODE) {
                if (data == null) {
                    Toast.makeText(getApplicationContext(), "Image cannot be null!", Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                    imagesArray = new ArrayList<>();
                    for (int i = 0; i < images.size(); i++) {
                        File file = new File((images.get(i)).path);
                        imagesArray.add(CompressFile.getCompressedImageFile(file, ProfileActivity.this));
                        Uri imageUri = Uri.fromFile(file);
                        beginCrop(imageUri);
                        Log.e(TAG, "onActivityResult: " + imageUri);
                        break;

                    }
                    /*uploadData(imagesArray);*/

                }

            } else if (requestCode == TAKE_PIC_REQUEST_CODE) {
                imagesArray = new ArrayList<>();
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        imagesArray.add(CompressFile.getCompressedImageFile(f, ProfileActivity.this));
                        Uri imageUri = Uri.fromFile(f);
                        beginCrop(imageUri);
                        Log.e(TAG, "onActivityResult: " + imageUri);
                        break;
                    }
                }
                /*if (!imagesArray.isEmpty())
                    uploadData(imagesArray);*/

            }if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }

            //updateViews();
        }

    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            binding.imgUserProfile.setImageDrawable(null);
            binding.imgUserProfile.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE_PERMISSIONS:

                if (!somePermissionsForeverDenied) {
                    if (checker.hasAllPermissionsGranted(grantResults)) {
                        somePermissionsForeverDenied = false;
                        openDialogForUploadPhoto();
                    } else {

                        for (String permission : permissions) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                                //denied
                                Log.e("denied", permission);
                            } else {
                                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                                    //allowed
                                    Log.e("allowed", permission);
                                } else {
                                    //set to never ask again
                                    Log.e("set to never ask again", permission);
                                    somePermissionsForeverDenied = true;

                                    break;
                                }
                            }
                        }

                    }
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }

    }

    private void uploadData(ArrayList<File> imagesArray) {

        HashMap<String, String> param = new HashMap<String, String>();
        param.put(NAME, binding.etFullName.getText().toString().trim());
        param.put(EMAIL, binding.etEmail.getText().toString().trim());
        param.put(MOBILE, binding.etMobile.getText().toString().trim());

        APIRequest.makeRequestToServerNewJsonArrayParameter(true, getApplicationContext(), ProfileActivity.this,
                ServicesType.updateProfile, ProfileActivity.this, imagesArray, param);
    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {
            case updateProfile:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        JSONArray resultArray = jsonObject.getJSONArray(RESPONSE_DATA);

                        if (resultArray.length() > 0) {

                            UserDetail objUserDetail = new UserDetail();
                            objUserDetail.setId(resultArray.getJSONObject(0).getString(ID));
                            objUserDetail.setName(resultArray.getJSONObject(0).getString(NAME));
                            objUserDetail.setEmail(resultArray.getJSONObject(0).getString(EMAIL));
                            objUserDetail.setMobile(resultArray.getJSONObject(0).getString(MOBILE));
                            objUserDetail.setType(resultArray.getJSONObject(0).getString(TYPE));
                            objUserDetail.setAvatar(resultArray.getJSONObject(0).getString(AVTAR));
                            objUserDetail.setCreated_by(resultArray.getJSONObject(0).getInt(CREATED_BY));
                            objUserDetail.setDelete_status(resultArray.getJSONObject(0).getInt(DELETE_STATUS));
                            objUserDetail.setIs_active(resultArray.getJSONObject(0).getInt(IS_ACTIVE));
                            objUserDetail.setUpdated_at(resultArray.getJSONObject(0).getString(UPDATED_AT));
                            //objUserDetail.setProfile_url("https://sugardate.me/upload/gallery/159291456847506.jpg");
                            objUserDetail.setProfile_url(resultArray.getJSONObject(0).getString(PROFILE_URL));


                            appSession.storeUser(objUserDetail);
                            setProfileDataFromIntent();
                        }

                        if (showMessage)
                            Utility.toaster(ProfileActivity.this, KEY_SUCCESS, message);

                    } else {
                        if (showMessage)
                            Utility.toaster(ProfileActivity.this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case getProfile:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        JSONArray resultArray = jsonObject.getJSONArray(RESPONSE_DATA);

                        if (resultArray.length() > 0) {

                            UserDetail objUserDetail = new UserDetail();
                            objUserDetail.setId(resultArray.getJSONObject(0).getString(ID));
                            objUserDetail.setName(resultArray.getJSONObject(0).getString(NAME));
                            objUserDetail.setEmail(resultArray.getJSONObject(0).getString(EMAIL));
                            objUserDetail.setMobile(resultArray.getJSONObject(0).getString(MOBILE));
                            objUserDetail.setType(resultArray.getJSONObject(0).getString(TYPE));
                            objUserDetail.setAvatar(resultArray.getJSONObject(0).getString(AVTAR));
                            objUserDetail.setCreated_by(resultArray.getJSONObject(0).getInt(CREATED_BY));
                            objUserDetail.setDelete_status(resultArray.getJSONObject(0).getInt(DELETE_STATUS));
                            objUserDetail.setIs_active(resultArray.getJSONObject(0).getInt(IS_ACTIVE));
                            objUserDetail.setUpdated_at(resultArray.getJSONObject(0).getString(UPDATED_AT));
                            //objUserDetail.setProfile_url("https://sugardate.me/upload/gallery/159291456847506.jpg");
                            objUserDetail.setProfile_url(resultArray.getJSONObject(0).getString(PROFILE_URL));

                            appSession.storeUser(objUserDetail);

                            setProfileDataFromIntent();

                        }

                    } else {
                        setProfileDataFromIntent();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    setProfileDataFromIntent();
                }
                break;

        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(ProfileActivity.this, KEY_ERROR, message);
    }

    private boolean checkValidData() {

        if (binding.etFullName.getText().toString().isEmpty()) {
            binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_fullname));
            return false;
        } else if (!Utility.isValidUserName(binding.etFullName.getText().toString().trim())) {
            binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_valid_fullname));
            return false;
        } if (binding.etEmail.getText().toString().isEmpty()) {
            binding.txtInputEmail.setError(getResources().getString(R.string.please_enter_email_address));
            return false;
        } else if (!Utility.isValidEmail(binding.etEmail.getText().toString().trim())) {
            binding.txtInputEmail.setError(getResources().getString(R.string.please_a_enter_valid_email_address));
            return false;
        } if (binding.etMobile.getText().toString().isEmpty()) {
            binding.txtInputMobile.setError(getResources().getString(R.string.please_enter_mobile_number));
            return false;
        } else if (!Utility.isValidMobile(binding.etMobile.getText().toString().trim())) {
            binding.txtInputMobile.setError(getResources().getString(R.string.please_a_enter_valid_mobile_number));
            return false;
        } else if (!Utility.isConnectingToInternet(getApplicationContext())) {
            Utility.toaster(ProfileActivity.this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
            return false;

        }else {
            binding.txtInputFullName.setError(null);
            binding.txtInputEmail.setError(null);
            binding.txtInputMobile.setError(null);
            return true;
        }
    }

    private void setWatcherOnEditText(){

        binding.etFullName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isButtonSaveClicked) {
                    if (binding.etFullName.getText().toString().isEmpty()) {
                        binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_fullname));
                    } else if (!Utility.isValidUserName(binding.etFullName.getText().toString().trim())) {
                        binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_valid_fullname));
                    } else {
                        binding.txtInputFullName.setError(null);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

        binding.etEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isButtonSaveClicked) {
                    if (binding.etEmail.getText().toString().isEmpty()) {
                        binding.txtInputEmail.setError(getResources().getString(R.string.please_enter_email_address));
                    } else if (!Utility.isValidEmail(binding.etEmail.getText().toString().trim())) {
                        binding.txtInputEmail.setError(getResources().getString(R.string.please_a_enter_valid_email_address));
                    } else
                        binding.txtInputEmail.setError(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

        binding.etMobile.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isButtonSaveClicked) {
                    if (binding.etMobile.getText().toString().isEmpty()) {
                        binding.txtInputMobile.setError(getResources().getString(R.string.please_enter_mobile_number));
                    } else if (!Utility.isValidMobile(binding.etMobile.getText().toString().trim())) {
                        binding.txtInputMobile.setError(getResources().getString(R.string.please_a_enter_valid_mobile_number));
                    } else
                        binding.txtInputMobile.setError(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });
    }
}