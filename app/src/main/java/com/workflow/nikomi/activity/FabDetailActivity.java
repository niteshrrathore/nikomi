package com.workflow.nikomi.activity;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.method.KeyListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.adapters.CustomWorkersSpinnerAdapter;
import com.workflow.nikomi.adapters.FabPartIncludedListAdapter;
import com.workflow.nikomi.adapters.FabPartIncludedSheetListAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityFabDetailBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.CallBackSignatureSave;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.FabDetailModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.model.UserDetail;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_COMPLETED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_OPEN;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.FABRICATION_ID;
import static com.workflow.nikomi.utils.Constant.FAB_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.FAB_CHECKED_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.FILTER;
import static com.workflow.nikomi.utils.Constant.GAS;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.PRIMER;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SELECT;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;
import static com.workflow.nikomi.utils.Constant.TYPE;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_FAB;
import static com.workflow.nikomi.utils.Constant.WELD_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.WELD_CHECKED_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.WIRE;

public class FabDetailActivity extends BaseActivity implements View.OnClickListener, ServicesListener, CallBackSignatureSave {

    ActivityFabDetailBinding binding;
    AppSession appSession;
    FabDetailModel mFabDetailModel;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean somePermissionsForeverDenied = false;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;
    ArrayList<PartDetailModel> mFebPartIncludedList = new ArrayList<>();
    FabPartIncludedListAdapter adapter;
    FabPartIncludedSheetListAdapter adapterBottomSheet;
    BottomSheetBehavior mBottomSheetBehavior;
    ArrayList<UserDetail> mWorkerList = new ArrayList<>();
    int intActionPerformed = 0;
    int posToSetBitMapSiganture = 0;
    Spinner[] spinnersCategory;
    RelativeLayout[] spinnersbackground;
    UserDetail selectedUserFabBy, selectedUserFabCheckedBy, selectedUserWeldBy, selectedUserWeldCheckedBy;
    Bitmap bitmapSignatureFabBy, bitmapSignatureFabCheckedBy, bitmapSignatureWeldBy, bitmapSignatureWeldCheckedBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fab_detail);
        binding.setActivity(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        checker = new PermissionsChecker(getApplicationContext());
        appSession = new AppSession(this);
        mBottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet);

        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.recyclerView.setNestedScrollingEnabled(false);

        binding.recyclerViewParts.setLayoutManager(new GridLayoutManager(this, 5));

        binding.layoutFab.setVisibility(View.VISIBLE);
        binding.layoutWeld.setVisibility(View.VISIBLE);
        spinnersCategory = new Spinner[]{binding.spinnerFabBy, binding.spinnerFabCheckedBy, binding.spinnerWeldBy, binding.spinnerWeldCheckedBy};
        spinnersbackground = new RelativeLayout[]{binding.rlSpinnerFabByBackground, binding.rlSpinnerFabCheckedByBackground, binding.rlSpinnerWeldByBackground, binding.rlSpinnerWeldCheckedByBackground};

        geFabDetailsFromServer();

    }

    private void geFabDetailsFromServer() {

        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();

            params.put(ID, getIntent().getStringExtra(FABRICATION_ID));
            APIRequest.generalApiRequest(this, this, this, params, ServicesType.getFabDetails, true);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnStart:

                HashMap<String, String> paramStart = new HashMap<>();
                paramStart.put(FABRICATION_ID, getIntent().getStringExtra(FABRICATION_ID));
                paramStart.put("assignment_type", "FAB");

                APIRequest.generalApiRequest(this, this, this, paramStart, ServicesType.workerProductivities, true);

                break;

            case R.id.btnAbort:
                intActionPerformed = 2;
                HashMap<String, String> paramAbort = new HashMap<>();
                paramAbort.put(ID, getIntent().getStringExtra(FABRICATION_ID));
                paramAbort.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));

                APIRequest.generalApiRequest(this, this, this, paramAbort, ServicesType.updateFabAction, true);
                break;

            case R.id.btnPartCompleted:
                if (checkPermission()) {

                    intActionPerformed = 3;

                    /*commented - Aug 01, 2020
                    * Not updating the Completed status over the server*/
                    /*HashMap<String, String> paramPartCompleted = new HashMap<>();
                    paramPartCompleted.put(ID, getIntent().getStringExtra(FABRICATION_ID));
                    paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_COMPLETED));

                    APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updateFabAction, true);*/
                    appSession.storeUserFabDetail(mFabDetailModel, ACTION_PART_FAB_IN_PROGRESS); //Aug 01, 2020 replace- ACTION_PART_FAB_COMPLETE status with ACTION_PART_FAB_IN_PROGRESS
                    /*for to show enable spinner background*/
                    for (RelativeLayout spinnerBg : spinnersbackground) {
                        spinnerBg.setBackground(getDrawable(R.drawable.spinner_background_white));
                    }

                    /*for to show both category*/
                    for (Spinner score : spinnersCategory) {
                        score.setEnabled(true);
                    }

                    binding.btnAbort.setVisibility(View.GONE);

                    /*Aug 01, 2020- Add the below both button over here from the ServicesType.updateFabAction*/
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.VISIBLE);
                }

                break;

            case R.id.btnCallToCheck:
                intActionPerformed = 4;

                HashMap<String, String> paramCallToCheck = new HashMap<>();
                paramCallToCheck.put(ID, getIntent().getStringExtra(FABRICATION_ID));
                paramCallToCheck.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_READY_FOR_INSPECT));

                APIRequest.generalApiRequest(this, this, this, paramCallToCheck, ServicesType.updateFabAction, true);

                break;

            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.btnViewDrawing:
                if (checkPermission())
                startActivity(new Intent(this, ViewDrawingActivity.class)
                        .putExtra("from", TYPE_ONLY_FAB).putExtra(ID, getIntent().getStringExtra(FABRICATION_ID)));
                break;

            case R.id.btnSave:
                if (binding.btnSave.getText().equals(getString(R.string.edit))){
                    enableFieldsForEdit();
                }else{
                    intActionPerformed = 1;
                    HashMap<String, String> params = new HashMap<>();
                    params.put(ID, getIntent().getStringExtra(FABRICATION_ID));
                    params.put(WIRE, binding.edtWire.getText().toString());
                    params.put(GAS, binding.edtGas.getText().toString());
                    params.put(PRIMER, binding.edtPrimer.getText().toString());

                    APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateFabAction, true);

                }
                break;

            case R.id.tvViewMore:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;

            case R.id.imgCrossBottomSheet:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                break;

        }
    }

    private boolean checkPermission() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {
            return true;
        } else {
            if (somePermissionsForeverDenied) {

                View contextView = binding.layoutMain;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();

                return false;

            } else {
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                return false;
            }

        }
    }

    private void setDate() {

        binding.tvHeaderTitle.setText(Utility.getJobTitle(this, getIntent().getStringExtra(JOB_NAME), getIntent().getStringExtra(JOB_NUMBER)));
        binding.tvItemNumName.setText(mFabDetailModel.getItemNumber() + "/" + mFabDetailModel.getName());
        binding.edtWPS.setText(mFabDetailModel.getWps());
        binding.edtWire.setText(mFabDetailModel.getWire());
        binding.edtGas.setText(mFabDetailModel.getGas());
        binding.edtPrimer.setText(mFabDetailModel.getPrimer());

        if (mFabDetailModel.getGalv() == 1)
            binding.edtGalv.setText(getString(R.string.yes));
        else
            binding.edtGalv.setText(getString(R.string.no));
        if (mFabDetailModel.getPaintModel() != null){
            binding.edtPaint.setText(mFabDetailModel.getPaintModel().getName());
            binding.llPanitColor.setBackgroundColor(Color.parseColor(mFabDetailModel.getPaintModel().getColor()));
            binding.llmainpaint.setVisibility(View.VISIBLE);
        }

        mFebPartIncludedList.addAll(mFabDetailModel.getParts());

        /*Aug 13, 2020 FabPartIncludedListAdapter adapter all included parts over the main screen*/
        adapter = new FabPartIncludedListAdapter(this, mFebPartIncludedList);

        /*Aug 13, 2020 FabPartIncludedSheetListAdapter adapter for bottomsheet to show all the parts*/
        adapterBottomSheet = new FabPartIncludedSheetListAdapter(this, mFebPartIncludedList);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerViewParts.setAdapter(adapterBottomSheet);

        binding.tvPartsIncluded.setText(getString(R.string.parts_included) + " (" + mFabDetailModel.getParts().size() + ")");
        if (mFebPartIncludedList.size() > 10) {
            binding.tvViewMore.setVisibility(View.VISIBLE);
        } else
            binding.tvViewMore.setVisibility(View.GONE);

        switch (mFabDetailModel.getStatusId()) {
            case ACTION_PART_FAB_NOT_FULFILLED:
            case ACTION_PART_FAB_OPEN:
                getAllWorkersFromServer();

                /*for to show both category*/
                for (Spinner score : spinnersCategory) {
                    score.setEnabled(false);
                }
                binding.btnStart.setVisibility(View.VISIBLE);
                binding.btnSave.setVisibility(View.GONE);
                disableFields();

                break;
            case ACTION_PART_FAB_COMPLETED:

                getAllWorkersFromServer();

                /*Aug 13, 2020 - check current logged-In user has worked on it or not*/
                if (mFabDetailModel.getWorkerProductivities() != null && Integer.parseInt(appSession.getUserDetails().getId())
                        == mFabDetailModel.getWorkerProductivities().get(0).getUserId()) {

                    /*for to show enable spinner background*/
                    for (RelativeLayout spinnerBg : spinnersbackground) {
                        spinnerBg.setBackground(getDrawable(R.drawable.spinner_background_white));
                    }

                    binding.btnSave.setVisibility(View.VISIBLE);
                    binding.btnSave.setText(getString(R.string.edit));
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.VISIBLE);

                    if (mFabDetailModel.getFabByUserId() != null) {
                        binding.spinnerFabBy.setEnabled(false);
                    }

                    if (mFabDetailModel.getFabCheckedByUser() != null) {
                        binding.spinnerFabCheckedBy.setEnabled(false);
                    }
                    if (mFabDetailModel.getWeldByUserId() != null) {
                        binding.spinnerWeldBy.setEnabled(false);
                    }
                    if (mFabDetailModel.getWeldCheckedByUser() != null) {
                        binding.spinnerWeldCheckedBy.setEnabled(false);
                    }

                    if (mFabDetailModel.getFabByUserId() != null && mFabDetailModel.getFabCheckedByUserId() != null
                            && mFabDetailModel.getWeldByUserId() != null && mFabDetailModel.getWeldCheckedByUserId() != null)
                        binding.btnCallToCheck.setEnabled(true);
                    else
                        binding.btnCallToCheck.setEnabled(false);

                } else {
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.GONE);

                    /*Aug 13, 2020 - for to show enable spinner background*/
                    for (RelativeLayout spinnerBg : spinnersbackground) {
                        spinnerBg.setEnabled(false);
                    }

                    /*Aug 13, 2020 - for to show both category*/
                    for (Spinner score : spinnersCategory) {
                        score.setEnabled(false);
                    }
                }

                break;

            case ACTION_PART_FAB_IN_PROGRESS:

                getAllWorkersFromServer();
                /*Aug 13, 2020 - for to show both category*/
                for (Spinner score : spinnersCategory) {
                    score.setEnabled(false);
                }

                if (mFabDetailModel.getWorkerProductivities() != null
                        && Integer.parseInt(appSession.getUserDetails().getId()) == mFabDetailModel.getWorkerProductivities().get(0).getUserId()) {
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.VISIBLE);
                    binding.btnSave.setVisibility(View.VISIBLE);
                    binding.btnSave.setText(R.string.edit);
                    binding.btnPartCompleted.setVisibility(View.VISIBLE);
                    disableFields();
                } else {
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnCallToCheck.setVisibility(View.GONE);
                }

                break;

            case ACTION_PART_FAB_READY_FOR_INSPECT:

                getAllWorkersFromServer();

                //for to show both category
                for (Spinner score : spinnersCategory) {
                    score.setEnabled(false);
                }

                binding.btnStart.setVisibility(View.GONE);
                binding.btnSave.setVisibility(View.GONE);
                binding.btnAbort.setVisibility(View.GONE);
                binding.btnPartCompleted.setVisibility(View.GONE);
                binding.btnCallToCheck.setVisibility(View.GONE);

                disableFields();

                break;
        }

    }

    /*Aug 13, 2020 - common listener for all the spinner*/
    AdapterView.OnItemSelectedListener scoreListener =
            new AdapterView.OnItemSelectedListener() {

                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (position > 0 && checkPermission()){

                        if (mFabDetailModel.getFabByUserId() == null && parent.getId() == R.id.spinnerFabBy)
                            Utility.getSignatureDialog(FabDetailActivity.this, FabDetailActivity.this,
                                    parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerFabBy);
                        if (mFabDetailModel.getFabCheckedByUserId() == null && parent.getId() == R.id.spinnerFabCheckedBy)
                            Utility.getSignatureDialog(FabDetailActivity.this, FabDetailActivity.this,
                                    parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerFabCheckedBy);
                        if (mFabDetailModel.getWeldByUserId() == null && parent.getId() == R.id.spinnerWeldBy)
                            Utility.getSignatureDialog(FabDetailActivity.this, FabDetailActivity.this,
                                    parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerWeldBy);
                        if (mFabDetailModel.getWeldCheckedByUserId() == null && parent.getId() == R.id.spinnerWeldCheckedBy)
                            Utility.getSignatureDialog(FabDetailActivity.this, FabDetailActivity.this,
                                    parent.getId(), (UserDetail) parent.getItemAtPosition(position), binding.spinnerWeldCheckedBy);

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    // TODO Auto-generated method stub.
                }

            };

    private void getAllWorkersFromServer() {
        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(TYPE, appSession.getUserDetails().getType());
                params.put(FILTER, jsonObject.toString());

                APIRequest.generalApiRequest(this, this, this, params, ServicesType.getAllWorkers, true);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {
            case getAllWorkers:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<List<UserDetail>>() {
                        }.getType();

                        ArrayList<UserDetail> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(LIST), typeGson);

                        if (mJobList != null && !mJobList.isEmpty()) {

                            /*Aug 13, 2020 - add default select object to the spinner*/
                            UserDetail objUserDetail = new UserDetail();
                            objUserDetail.setId("-1");
                            objUserDetail.setName(SELECT);
                            this.mWorkerList.add(objUserDetail);
                            this.mWorkerList.addAll(mJobList);

                            CustomWorkersSpinnerAdapter spinnerAdapter = new CustomWorkersSpinnerAdapter(this, R.layout.spinner_text, mWorkerList);
                            spinnersCategory = new Spinner[]{binding.spinnerFabBy, binding.spinnerFabCheckedBy, binding.spinnerWeldBy, binding.spinnerWeldCheckedBy};
                            spinnersbackground = new RelativeLayout[]{binding.rlSpinnerFabByBackground, binding.rlSpinnerFabCheckedByBackground, binding.rlSpinnerWeldByBackground, binding.rlSpinnerWeldCheckedByBackground};
                            /*Aug 13, 2020 - for to show both category*/
                            for (Spinner score : spinnersCategory) {
                                score.setAdapter(spinnerAdapter);
                                score.setOnItemSelectedListener(scoreListener);
                            }

                            switch (mFabDetailModel.getStatusId()) {

                                case ACTION_PART_FAB_COMPLETED:
                                case ACTION_PART_FAB_READY_FOR_INSPECT:
                                case ACTION_PART_FAB_IN_PROGRESS:
                                    /*Aug 13, 2020 - set all the signature related to the user selected in the spinner*/
                                    setSignatureWithUser();

                                    break;
                            }

                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }

                break;

            case updateUserWithSignatureForFab:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        if (selectedUserFabBy != null)
                            selectedUserFabBy = null;

                        if (selectedUserFabCheckedBy != null)
                            selectedUserFabCheckedBy = null;

                        if (selectedUserWeldBy != null)
                            selectedUserWeldBy = null;

                        if (selectedUserWeldCheckedBy != null)
                            selectedUserWeldCheckedBy = null;

                        switch (posToSetBitMapSiganture) {
                            case 1:
                                binding.imgSignatureFabBy.setImageBitmap(bitmapSignatureFabBy);
                                break;
                            case 2:
                                binding.imgSignatureFabCheckedBy.setImageBitmap(bitmapSignatureFabCheckedBy);
                                break;
                            case 3:
                                binding.imgSignatureWeldBy.setImageBitmap(bitmapSignatureWeldBy);
                                break;
                            case 4:
                                binding.imgSignatureWeldCheckedBy.setImageBitmap(bitmapSignatureWeldCheckedBy);
                                break;
                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case updateFabAction:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        if (intActionPerformed == 1) {
                            disableFields();
                            binding.btnSave.setText(getString(R.string.edit));
                        } else if (intActionPerformed == 3) {
                            binding.btnPartCompleted.setVisibility(View.GONE);
                            binding.btnCallToCheck.setVisibility(View.VISIBLE);
                        } else if (intActionPerformed == 2) {
                            startActivity(new Intent(FabDetailActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else if (intActionPerformed == 4) {
                            if (showMessage)
                                Utility.toaster(this, KEY_SUCCESS, message);
                            appSession.storeUserFabDetail(mFabDetailModel, ACTION_PART_FAB_READY_FOR_INSPECT);
                            startActivity(new Intent(FabDetailActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }


                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case getFabDetails:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<FabDetailModel>() {
                        }.getType();

                        mFabDetailModel = gson.fromJson(jsonObject.getJSONArray(RESPONSE_DATA).get(0).toString(), typeGson);
                        if (mFabDetailModel != null) {
                            setDate();
                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case workerProductivities:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        /*store mFabDetailModel to local with latest status*/
                        appSession.storeUserFabDetail(mFabDetailModel, ACTION_PART_FAB_IN_PROGRESS);

                        /*commented - Aug 13, 2020 check Abort clicked or not*/
                        if (appSession.isPartAbortClicked())
                            appSession.setPartAbortClicked(false);

                        binding.btnStart.setVisibility(View.GONE);
                        binding.btnSave.setVisibility(View.VISIBLE);
                        binding.btnSave.setText(getString(R.string.edit));
                        binding.btnAbort.setVisibility(View.VISIBLE);
                        binding.btnPartCompleted.setVisibility(View.VISIBLE);

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(this, KEY_ERROR, message);
    }

    /*Aug 13, 2020 - method to get the signature from the signature dialoge along with the spinnerId and user detail param*/
    @Override
    public void onOkClicked(Bitmap bitmapSignature, int spinnerId, UserDetail mUserDetail) {
        switch (spinnerId) {
            case R.id.spinnerFabBy:
                posToSetBitMapSiganture = 1;
                selectedUserFabBy = mUserDetail;
                bitmapSignatureFabBy = bitmapSignature;
                enableCallToCheckButton();
                updateSignatureWithUserToServer(Utility.bitmapToFile(FabDetailActivity.this, bitmapSignatureFabBy));
                break;
            case R.id.spinnerFabCheckedBy:
                posToSetBitMapSiganture = 2;
                selectedUserFabCheckedBy = mUserDetail;
                bitmapSignatureFabCheckedBy = bitmapSignature;
                enableCallToCheckButton();
                updateSignatureWithUserToServer(Utility.bitmapToFile(FabDetailActivity.this, bitmapSignatureFabCheckedBy));
                break;
            case R.id.spinnerWeldBy:
                posToSetBitMapSiganture = 3;
                selectedUserWeldBy = mUserDetail;
                bitmapSignatureWeldBy = bitmapSignature;
                enableCallToCheckButton();
                updateSignatureWithUserToServer(Utility.bitmapToFile(FabDetailActivity.this, bitmapSignatureWeldBy));
                break;
            case R.id.spinnerWeldCheckedBy:
                posToSetBitMapSiganture = 4;
                selectedUserWeldCheckedBy = mUserDetail;
                bitmapSignatureWeldCheckedBy = bitmapSignature;
                enableCallToCheckButton();
                updateSignatureWithUserToServer(Utility.bitmapToFile(FabDetailActivity.this, bitmapSignatureWeldCheckedBy));
                break;
        }
    }

    /*Aug 13, 2020 - method to get the signature only from the signature dialoge*/
    @Override
    public void onOkClicked(Bitmap bitmapSignature) {

    }

    /*Aug 13, 2020 - method to set to default on cross click of the signature dialoge*/
    @Override
    public void onDismiss(Spinner spinner) {
        spinner.setSelection(0);
    }

    private void updateSignatureWithUserToServer(ArrayList<File> imagesArray) {
        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> param = new HashMap<String, String>();
            param.put(FABRICATION_ID, mFabDetailModel.getId().toString());

            if (selectedUserFabBy != null)
                param.put(FAB_BY_USER_ID, selectedUserFabBy.getId());

            if (selectedUserFabCheckedBy != null)
                param.put(FAB_CHECKED_BY_USER_ID, selectedUserFabCheckedBy.getId());

            if (selectedUserWeldBy != null)
                param.put(WELD_BY_USER_ID, selectedUserWeldBy.getId());

            if (selectedUserWeldCheckedBy != null)
                param.put(WELD_CHECKED_BY_USER_ID, selectedUserWeldCheckedBy.getId());

            APIRequest.makeRequestToServerNewJsonArrayParameter(true, getApplicationContext(), FabDetailActivity.this,
                    ServicesType.updateUserWithSignatureForFab, FabDetailActivity.this, imagesArray, param);


        }
    }

    /*Aug 13, 2020 - Method used to enable Call to Check button*/
    private void enableCallToCheckButton() {
        if (bitmapSignatureFabBy == null && mFabDetailModel.getFabBySignature() == null
                || bitmapSignatureWeldBy == null && mFabDetailModel.getWeldBySignature() == null)
            binding.btnCallToCheck.setEnabled(false);
        /*else if (bitmapSignatureFabCheckedBy == null && mFabDetailModel.getFabCheckedBySignature() == null)
            binding.btnCallToCheck.setEnabled(false);
        else if (bitmapSignatureWeldBy == null && mFabDetailModel.getWeldBySignature() == null)
            binding.btnCallToCheck.setEnabled(false);
        else if (bitmapSignatureWeldCheckedBy == null && mFabDetailModel.getWeldCheckedBySignature() == null)
            binding.btnCallToCheck.setEnabled(false);*/
        else
            binding.btnCallToCheck.setEnabled(true);
    }

    private void removeSignatureForIndexZero(int spinnerId) {
        switch (spinnerId) {
            case R.id.spinnerFabBy:
                selectedUserFabBy = null;
                bitmapSignatureFabBy = null;
                binding.imgSignatureFabBy.setImageBitmap(null);
                enableCallToCheckButton();
                break;
            case R.id.spinnerFabCheckedBy:
                selectedUserFabCheckedBy = null;
                bitmapSignatureFabCheckedBy = null;
                binding.imgSignatureFabCheckedBy.setImageBitmap(null);
                enableCallToCheckButton();
                break;
            case R.id.spinnerWeldBy:
                selectedUserWeldBy = null;
                bitmapSignatureWeldBy = null;
                binding.imgSignatureWeldBy.setImageBitmap(null);
                enableCallToCheckButton();
                break;
            case R.id.spinnerWeldCheckedBy:
                selectedUserWeldCheckedBy = null;
                bitmapSignatureWeldCheckedBy = null;
                binding.imgSignatureWeldCheckedBy.setImageBitmap(null);
                enableCallToCheckButton();
                break;
        }
    }

    /*Aug 13, 2020 - set signature with selected user in the spinner*/
    private void setSignatureWithUser() {

        for (int i = 0; i < mWorkerList.size(); i++) {

            if (mFabDetailModel.getFabByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mFabDetailModel.getFabByUserId()) {
                binding.spinnerFabBy.setEnabled(false);
                binding.spinnerFabBy.setSelection(i);
            }

            if (mFabDetailModel.getFabCheckedByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mFabDetailModel.getFabCheckedByUserId()) {
                binding.spinnerFabCheckedBy.setEnabled(false);
                binding.spinnerFabCheckedBy.setSelection(i);
            }

            if (mFabDetailModel.getWeldByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mFabDetailModel.getWeldByUserId()) {
                binding.spinnerWeldBy.setEnabled(false);
                binding.spinnerWeldBy.setSelection(i);
            }

            if (mFabDetailModel.getWeldCheckedByUserId() != null && Integer.parseInt(mWorkerList.get(i).getId()) == mFabDetailModel.getWeldCheckedByUserId()) {
                binding.spinnerWeldCheckedBy.setEnabled(false);
                binding.spinnerWeldCheckedBy.setSelection(i);
            }

        }

        if (mFabDetailModel.getFabBySignature() != null && !mFabDetailModel.getFabBySignature().isEmpty())
            Utility.loadImageFromPicasso(FabDetailActivity.this, mFabDetailModel.getFabBySignature(), binding.imgSignatureFabBy);

        if (mFabDetailModel.getFabCheckedBySignature() != null && !mFabDetailModel.getFabCheckedBySignature().isEmpty())
            Utility.loadImageFromPicasso(FabDetailActivity.this, mFabDetailModel.getFabCheckedBySignature(), binding.imgSignatureFabCheckedBy);

        if (mFabDetailModel.getWeldBySignature() != null && !mFabDetailModel.getWeldBySignature().isEmpty())
            Utility.loadImageFromPicasso(FabDetailActivity.this, mFabDetailModel.getWeldBySignature(), binding.imgSignatureWeldBy);

        if (mFabDetailModel.getWeldCheckedBySignature() != null && !mFabDetailModel.getWeldCheckedBySignature().isEmpty())
            Utility.loadImageFromPicasso(FabDetailActivity.this, mFabDetailModel.getWeldCheckedBySignature(), binding.imgSignatureWeldCheckedBy);

        if (mFabDetailModel.getFabBySignature() != null && !mFabDetailModel.getFabBySignature().isEmpty()
        && mFabDetailModel.getWeldBySignature() != null && !mFabDetailModel.getWeldBySignature().isEmpty())
            binding.btnCallToCheck.setEnabled(true);

    }

    private void disableFields() {
        /*Disable the editext and display as textview Wire*/
        binding.edtWire.setEnabled(false);
        binding.edtWire.setTextColor(Color.BLACK);
        binding.edtWire.setTag(binding.edtWire.getKeyListener());
        binding.edtWire.setKeyListener(null);
        binding.edtWire.setBackground(null);

        /*Disable the editext and display as textview Gas*/
        binding.edtGas.setEnabled(false);
        binding.edtGas.setTextColor(Color.BLACK);
        binding.edtGas.setTag(binding.edtGas.getKeyListener());
        binding.edtGas.setKeyListener(null);
        binding.edtGas.setBackground(null);

        /*Disable the editext and display as textview Primer*/
        binding.edtPrimer.setEnabled(false);
        binding.edtPrimer.setTextColor(Color.BLACK);
        binding.edtPrimer.setTag(binding.edtPrimer.getKeyListener());
        binding.edtPrimer.setKeyListener(null);
        binding.edtPrimer.setBackground(null);
    }

    private void enableFieldsForEdit() {
        /*Disable the editext and display as textview Wire*/
        binding.edtWire.setEnabled(true);
        binding.edtWire.setTextColor(Color.BLACK);
        binding.edtWire.setTag(binding.edtWire.getTag());
        binding.edtWire.setKeyListener((KeyListener)binding.edtWire.getTag());
        binding.edtWire.setBackgroundDrawable(getDrawable(R.drawable.spinner_background_white));

        /*Disable the editext and display as textview Gas*/
        binding.edtGas.setEnabled(true);
        binding.edtGas.setTextColor(Color.BLACK);
        binding.edtGas.setTag(binding.edtGas.getTag());
        binding.edtGas.setKeyListener((KeyListener)binding.edtGas.getTag());
        binding.edtGas.setBackgroundDrawable(getDrawable(R.drawable.spinner_background_white));

        /*Disable the editext and display as textview Primer*/
        binding.edtPrimer.setEnabled(true);
        binding.edtPrimer.setTextColor(Color.BLACK);
        binding.edtPrimer.setKeyListener((KeyListener)binding.edtPrimer.getTag());
        binding.edtPrimer.setBackgroundDrawable(getDrawable(R.drawable.spinner_background_white));

        binding.btnSave.setText(getString(R.string.save));
    }

}