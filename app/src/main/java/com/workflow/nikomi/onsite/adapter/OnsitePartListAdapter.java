package com.workflow.nikomi.onsite.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.databinding.OnsiteRowLayoutBinding;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.utils.AppSession;

import java.util.ArrayList;

public class OnsitePartListAdapter extends RecyclerView.Adapter<OnsitePartListAdapter.MyViewHolder> {

    Activity mContext;
    JobAdapterListener mListener;
    ArrayList<PartDetailModel> mImgList;
    String mFragmentTag;
    AppSession appSession;
    private static final String TAG = "ImageItemAdapter";

    public OnsitePartListAdapter(Activity applicationContext, ArrayList<PartDetailModel> imgList, JobAdapterListener listener, String fragmentTag) {
        appSession = new AppSession(applicationContext);
        this.mContext = applicationContext;
        this.mImgList = imgList;
        this.mListener = listener;
        this.mFragmentTag = fragmentTag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        OnsiteRowLayoutBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.onsite_row_layout, parent, false);

        return new MyViewHolder(layoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.layoutBinding.setViewModel(mImgList.get(position));

        holder.layoutBinding.tvJobName.setText(mImgList.get(position).getName());
        holder.layoutBinding.tvAndFabNumber.setVisibility(View.VISIBLE);
        holder.layoutBinding.tvAndFabNumber.setText(mContext.getString(R.string.quantity)+ ": " +mImgList.get(position).getQuantity());

        holder.layoutBinding.imgEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClicked(mImgList.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mImgList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        OnsiteRowLayoutBinding layoutBinding;

        public MyViewHolder(OnsiteRowLayoutBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

    public interface JobAdapterListener {
        void onClicked(PartDetailModel post);
    }

}
