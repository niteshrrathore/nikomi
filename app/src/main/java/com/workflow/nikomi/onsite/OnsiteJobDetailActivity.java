package com.workflow.nikomi.onsite;

import android.Manifest;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.PartDetailForInspectionActivity;
import com.workflow.nikomi.activity.ViewDrawingActivity;
import com.workflow.nikomi.adapters.InspectedPhotosAdapter;
import com.workflow.nikomi.adapters.PhotosAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityJobDetailForOnsiteBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.fragment.JobDetailFragment;
import com.workflow.nikomi.fragment.MyDialogFragment;
import com.workflow.nikomi.interfaces.CallBackOk;
import com.workflow.nikomi.interfaces.CallBackSignatureSave;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.DocumentDetailsModel;
import com.workflow.nikomi.model.JobDetailOnsideModel;
import com.workflow.nikomi.model.JobInspectPhotoModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.model.PartInspectPhotoModel;
import com.workflow.nikomi.model.UserDetail;
import com.workflow.nikomi.onsite.adapter.InspectedJobPhotosAdapter;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.CompressFile;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_COMPLETED;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_OPEN;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_OPEN;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_ONSITE;
import static com.workflow.nikomi.utils.Constant.FILE_TYPE;
import static com.workflow.nikomi.utils.Constant.GENERAL_ASSEMBLY_TYPE;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.INSPECT_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.JOB_ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.PARTS_COMPLETED;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_PART;
import static com.workflow.nikomi.utils.Constant.TYPE_ONSITE_DOC;

public class OnsiteJobDetailActivity extends BaseActivity implements View.OnClickListener, ServicesListener, CallBackSignatureSave {

    private static final String TAG = "PartDetailActivity";
    ActivityJobDetailForOnsiteBinding binding;
    AppSession appSession;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};
    static final String[] PERMISSIONS_STORAGE = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean somePermissionsForeverDenied = false;
    boolean storagePermissionsForeverDenied = false;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;
    static final int REQUEST_CODE_PERMISSIONS_FOR_STORAGE = 2;
    public static final int TAKE_PIC_REQUEST_CODE = 4;
    public static final int CHOOSE_PIC_REQUEST_CODE = 3;
    public int jobId, selectedTabPos = 0;
    public String jobName, jobNumber, mKeyFrom;
    ViewPagerAdapter adapter;
    ArrayList<File> imagesArray = new ArrayList<>();
    PhotosAdapter photosAdapter;
    InspectedJobPhotosAdapter mInspectedJobPhotosAdapter;
    JobDetailOnsideModel mJobDetailOnsideModel;
    Bitmap bitmapSignatureForOnsite;
    int intActionPerformed = 0;
    ArrayList<DocumentDetailsModel> documentDetailsModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_detail_for_onsite);
        binding.setActivity(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        checker = new PermissionsChecker(getApplicationContext());
        appSession = new AppSession(this);

        binding.recyclerViewPictures.setLayoutManager(new GridLayoutManager(this, 2));
        binding.recyclerViewPictures.setNestedScrollingEnabled(false);

        jobId = Integer.parseInt(getIntent().getStringExtra(JOB_ID));
        jobName = getIntent().getStringExtra(JOB_NAME);
        jobNumber = getIntent().getStringExtra(JOB_NUMBER);

        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedTabPos = position;
                highLightCurrentTab(selectedTabPos);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        getJobDetailsFromServer();

    }

    private boolean checkPermission() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {
            return true;
        } else {
            if (somePermissionsForeverDenied) {
                View contextView = binding.layoutMain;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
                return false;
            } else {
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                return false;
            }
        }
    }


    private boolean checkPermissionForStorage() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS_STORAGE);
        if (!lacksPermissions) {
            return true;
        } else {
            if (storagePermissionsForeverDenied) {
                View contextView = binding.layoutMain;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
                return false;
            } else {
                requestPermissions(PERMISSIONS_STORAGE, REQUEST_CODE_PERMISSIONS_FOR_STORAGE);
                return false;
            }

        }
    }

    private void setDate() {

        binding.tvHeaderTitle.setText(Utility.getJobTitle(this, getIntent().getStringExtra(JOB_NAME), getIntent().getStringExtra(JOB_NUMBER)));

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        binding.viewpager.setOffscreenPageLimit(1);
        adapter.addFragment(OnsitePartFragment.newInstance(jobId, jobName, jobNumber, mKeyFrom, mJobDetailOnsideModel.getParts()), getResources().getString(R.string.tab_part));
        adapter.addFragment(OnsiteFabFragment.newInstance(jobId, jobName, jobNumber, mKeyFrom, mJobDetailOnsideModel.getFabrications()), getResources().getString(R.string.tab_fabrication));
        binding.viewpager.setAdapter(adapter);
        binding.tabs.setupWithViewPager(binding.viewpager);
        highLightCurrentTab(selectedTabPos);

        switch (mJobDetailOnsideModel.getOnsiteStatusId()) {
            case ACTION_JOB_NOT_FULFILLED:
            case ACTION_JOB_OPEN:

                binding.btnStart.setVisibility(View.VISIBLE);
                binding.btnUploadPicture.setVisibility(View.GONE);

                photosAdapter = new PhotosAdapter(this, imagesArray);
                binding.recyclerViewPictures.setAdapter(photosAdapter);

                break;
            case ACTION_JOB_IN_PROGRESS:

                if (mJobDetailOnsideModel.getWorkerProductivities()!=null && Integer.parseInt(appSession.getUserDetails().getId()) == mJobDetailOnsideModel.getWorkerProductivities().get(0).getUserId()){
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.VISIBLE);
                    binding.btnPartCompleted.setVisibility(View.VISIBLE);
                    binding.btnUploadPicture.setVisibility(View.VISIBLE);
                }else{
                    binding.btnStart.setVisibility(View.GONE);
                    binding.btnAbort.setVisibility(View.GONE);
                    binding.btnPartCompleted.setVisibility(View.GONE);
                    binding.btnUploadPicture.setVisibility(View.GONE);
                }


                photosAdapter = new PhotosAdapter(this, imagesArray);
                binding.recyclerViewPictures.setAdapter(photosAdapter);

                break;

            case ACTION_JOB_READY_FOR_INSPECT:
                binding.btnStart.setVisibility(View.GONE);
                binding.btnAbort.setVisibility(View.GONE);
                binding.btnPartCompleted.setVisibility(View.GONE);
                binding.btnUploadPicture.setVisibility(View.GONE);
                binding.layoutOnsiteUserDetail.setVisibility(View.VISIBLE);

                ArrayList<JobInspectPhotoModel> imagesArray = mJobDetailOnsideModel.getJobFiles();
                mInspectedJobPhotosAdapter = new InspectedJobPhotosAdapter(this, imagesArray);
                binding.recyclerViewPictures.setAdapter(mInspectedJobPhotosAdapter);

                binding.tvOnsiteUserName.setText(appSession.getUserDetails().getName());
                Utility.loadImageFromPicasso(OnsiteJobDetailActivity.this, mJobDetailOnsideModel.getInspectBySignature(), binding.imgSignatureOnsiteUser);

                break;
        }

    }

    private void getJobDetailsFromServer() {
        if (!Utility.isConnectingToInternet(this)) {

            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();

            params.put(ID, getIntent().getStringExtra(JOB_ID));
            APIRequest.generalApiRequest(this, this, this, params, ServicesType.getJobDetails, true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnStart:

                HashMap<String, String> paramStart = new HashMap<>();
                paramStart.put(JOB_ID, getIntent().getStringExtra(JOB_ID));
                paramStart.put("assignment_type", "JOB");

                APIRequest.generalApiRequest(this, this, this, paramStart, ServicesType.workerProductivities, true);


                break;

            case R.id.btnAbort:
                if (!Utility.isConnectingToInternet(this)) {

                    Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

                } else {
                    intActionPerformed = 1;
                    HashMap<String, String> params = new HashMap<>();
                    params.put(ID, getIntent().getStringExtra(JOB_ID));
                    params.put("onsite_status_id", String.valueOf(ACTION_JOB_NOT_FULFILLED));

                    APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateJobAction, true);
                    appSession.storeOnsiteJobDetail(mJobDetailOnsideModel, ACTION_JOB_NOT_FULFILLED);
                    appSession.setPartAbortClicked(true);

                }
                break;

            case R.id.btnPartCompleted:
                if (checkPermission()) {
                    if (imagesArray.isEmpty())
                        Utility.toaster(this, KEY_ERROR, getString(R.string.at_least_one_picture_required_for_inspect));
                    else
                        Utility.getSignatureDialog(this, this);
                }

                break;

            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.btnUploadPicture:
                    openDialogForUploadPhoto();
                break;

            case R.id.btnViewDrawing:
                if (checkPermissionForStorage())
                    /*startActivity(new Intent(this, ViewDrawingActivity.class)
                            .putExtra("from", TYPE_ONSITE_DOC).putExtra(ID, getIntent().getStringExtra(JOB_ID)));*/
                    getGADocumentFromServer(""+jobId);
                break;
        }
    }

    private void callViewGADocumentDialogFragment(ArrayList<DocumentDetailsModel> documentDetailsModelArrayList){

        FragmentTransaction ft = ((FragmentActivity)this).getSupportFragmentManager().beginTransaction();
        // FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFragment = new MyDialogFragment(this, documentDetailsModelArrayList);
        dialogFragment.show(ft, "dialog");
    }

     private void getGADocumentFromServer(String jobId) {
         if (!Utility.isConnectingToInternet(this)) {

             Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

         } else {

             HashMap<String, String> params = new HashMap<>();
             params.put(JOB_ID, jobId);
             params.put(FILE_TYPE, GENERAL_ASSEMBLY_TYPE);

             APIRequest.generalApiRequest(this,this,
                     this, params, ServicesType.getGADocument, true);


         }
     }
    @Override
    public void success(String response, ServicesType type) {
        switch (type) {

            case getGADocument:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<List<DocumentDetailsModel>>() {
                        }.getType();

                        if (!documentDetailsModelArrayList.isEmpty()){
                            documentDetailsModelArrayList.clear();
                        }

                        ArrayList<DocumentDetailsModel> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(LIST), typeGson);
                        documentDetailsModelArrayList.addAll(mJobList);

                        if (documentDetailsModelArrayList != null){
                            if (!documentDetailsModelArrayList.isEmpty()){
                                callViewGADocumentDialogFragment(documentDetailsModelArrayList);
                            }else {
                                Utility.toaster(this, KEY_ERROR, getString(R.string.no_record_found));
                            }
                        }else {
                            Utility.toaster(this, KEY_ERROR, getString(R.string.no_record_found));
                        }



                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case updateUserWithSignatureForJob:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {
                        HashMap<String, String> params = new HashMap<>();
                        params.put(ID, getIntent().getStringExtra(JOB_ID));
                        params.put("onsite_status_id", String.valueOf(ACTION_JOB_COMPLETED));

                        APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateJobAction, true);
                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case updateJobAction:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {
                        if (intActionPerformed == 1)
                            onBackPressed();
                        else
                            startActivity(new Intent(OnsiteJobDetailActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;

            case getJobDetails:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<JobDetailOnsideModel>() {
                        }.getType();

                        mJobDetailOnsideModel = gson.fromJson(jsonObject.getJSONArray(RESPONSE_DATA).get(0).toString(), typeGson);
                        if (mJobDetailOnsideModel != null) {
                            setDate();
                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }

                break;

            case workerProductivities:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        appSession.storeOnsiteJobDetail(mJobDetailOnsideModel, ACTION_JOB_IN_PROGRESS);
                        if (appSession.isPartAbortClicked())
                            appSession.setPartAbortClicked(false);

                        binding.btnStart.setVisibility(View.GONE);
                        binding.btnAbort.setVisibility(View.VISIBLE);
                        binding.btnPartCompleted.setVisibility(View.VISIBLE);
                        binding.btnUploadPicture.setVisibility(View.VISIBLE);

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }

                break;

            case uploadPhotosForJob:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        HashMap<String, String> param = new HashMap<String, String>();
                        param.put(JOB_ID, getIntent().getStringExtra(JOB_ID));
                        APIRequest.makeRequestToServerNewJsonArrayParameter(true, getApplicationContext(), OnsiteJobDetailActivity.this,
                                ServicesType.updateUserWithSignatureForJob, OnsiteJobDetailActivity.this, Utility.bitmapToFile(OnsiteJobDetailActivity.this, bitmapSignatureForOnsite), param);

                    } else {
                        if (showMessage)
                            Utility.toaster(this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(this, KEY_ERROR, getString(R.string.something_went_wrong));
                }

                break;
        }

    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(this, KEY_ERROR, message);
    }

    /*https://stackoverflow.com/questions/6834354/how-to-capture-human-signature*/
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE_PERMISSIONS:

                if (!somePermissionsForeverDenied) {
                    if (checker.hasAllPermissionsGranted(grantResults)) {
                        somePermissionsForeverDenied = false;
                        /*permission granted*/
                        //openDialogForUploadPhoto();
                    } else {

                        for (String permission : permissions) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                                //denied
                                Log.e("denied", permission);
                            } else {
                                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                                    //allowed
                                    Log.e("allowed", permission);
                                } else {
                                    //set to never ask again
                                    Log.e("set to never ask again", permission);
                                    somePermissionsForeverDenied = true;

                                    break;
                                }
                            }
                        }

                    }
                }

                break;

            case REQUEST_CODE_PERMISSIONS_FOR_STORAGE:

                if (!storagePermissionsForeverDenied) {
                    if (checker.hasAllPermissionsGranted(grantResults)) {
                        storagePermissionsForeverDenied = false;
                        /*permission granted*/
                        //openDialogForUploadPhoto();
                        getGADocumentFromServer(""+jobId);
                    } else {

                        for (String permission : permissions) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                                //denied
                                Log.e("denied", permission);
                            } else {
                                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                                    //allowed
                                    Log.e("allowed", permission);
                                } else {
                                    //set to never ask again
                                    Log.e("set to never ask again", permission);
                                    storagePermissionsForeverDenied = true;

                                    break;
                                }
                            }
                        }

                    }
                }


            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == CHOOSE_PIC_REQUEST_CODE) {
                if (data == null) {
                    Toast.makeText(getApplicationContext(), "Image cannot be null!", Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

                    for (int i = 0; i < images.size(); i++) {
                        File file = new File((images.get(i)).path);
                        imagesArray.add(CompressFile.getCompressedImageFile(file, OnsiteJobDetailActivity.this));
                        /*Uri imageUri = Uri.fromFile(file);
                        beginCrop(imageUri);*/
                        break;

                    }

                    /*uploadData(imagesArray);*/

                }

                photosAdapter.updateList(imagesArray);

            } else if (requestCode == TAKE_PIC_REQUEST_CODE) {

                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        imagesArray.add(CompressFile.getCompressedImageFile(f, OnsiteJobDetailActivity.this));
                        /*Uri imageUri = Uri.fromFile(f);
                        beginCrop(imageUri);*/
                        break;
                    }
                }
                /*if (!imagesArray.isEmpty())
                    uploadData(imagesArray);*/
                photosAdapter.updateList(imagesArray);
            }/*if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }*/

        }

    }

    private void highLightCurrentTab(int position) {

        for (int i = 0; i < binding.tabs.getTabCount(); i++) {
            TabLayout.Tab tab = binding.tabs.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }

        TabLayout.Tab tab = binding.tabs.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
    }

    @Override
    public void onOkClicked(Bitmap bitmapSignature, int spinnerId, UserDetail mUserDetail) {

    }

    @Override
    public void onOkClicked(Bitmap bitmapSignature) {
        bitmapSignatureForOnsite = bitmapSignature;
        HashMap<String, String> param = new HashMap<String, String>();
        param.put(JOB_ID, getIntent().getStringExtra(JOB_ID));
        param.put(FILE_TYPE, "INSPECTION");
        APIRequest.makeRequestToServerNewJsonArrayParameter(true, getApplicationContext(), OnsiteJobDetailActivity.this,
                ServicesType.uploadPhotosForJob, OnsiteJobDetailActivity.this, imagesArray, param);

    }

    @Override
    public void onDismiss(Spinner mSpinner) {

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> mFragmentList = new ArrayList<>();
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return mFragmentTitleList.get(position);
            return null;
        }

        public View getTabView(int position) {
            View view = LayoutInflater.from(OnsiteJobDetailActivity.this).inflate(R.layout.custom_tab_with_count, null);
            TextView tabTextView = view.findViewById(R.id.tvtab1);
            TextView tvCount = view.findViewById(R.id.tvCount);
            tabTextView.setText(mFragmentTitleList.get(position));
            tvCount.setVisibility(View.GONE);

            return view;
        }

        public View getSelectedTabView(int position) {
            View view = LayoutInflater.from(OnsiteJobDetailActivity.this).inflate(R.layout.custom_tab_with_count, null);
            TextView tabTextView = view.findViewById(R.id.tvtab1);
            TextView tvCount = view.findViewById(R.id.tvCount);
            tabTextView.setText(mFragmentTitleList.get(position));
            tabTextView.setTextColor(ContextCompat.getColor(OnsiteJobDetailActivity.this, R.color.colorRed));
            tvCount.setVisibility(View.GONE);

            return view;
        }

    }

    private void openDialogForUploadPhoto() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {

            //show dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(OnsiteJobDetailActivity.this,
                    R.style.AlertDialogCustom));
            builder.setTitle(getString(R.string.upload_or_take_photo));
            builder.setPositiveButton(getString(R.string.upload), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(OnsiteJobDetailActivity.this, AlbumSelectActivity.class);
                    intent.setType("image/*");
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5);
                    startActivityForResult(intent, CHOOSE_PIC_REQUEST_CODE);

                }
            });

            builder.setNegativeButton(getString(R.string.take_photo), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, TAKE_PIC_REQUEST_CODE);

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        } else {
            if (somePermissionsForeverDenied) {
                View contextView = binding.layoutMain;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
            } else
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

}