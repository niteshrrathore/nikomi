package com.workflow.nikomi.onsite;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.ViewDrawingActivity;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.FragmentOnsiteFabBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.FabDetailModel;
import com.workflow.nikomi.model.JobDetailModel;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.onsite.adapter.OnsiteFabListAdapter;
import com.workflow.nikomi.onsite.adapter.OnsitePartListAdapter;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.Constant.FILTER;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.JOB_ID;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_FROM;
import static com.workflow.nikomi.utils.Constant.LIMIT;
import static com.workflow.nikomi.utils.Constant.LIMIT_COUNT;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.OFFSET;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOTAL;
import static com.workflow.nikomi.utils.Constant.TYPE;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_FAB;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_PART;

public class OnsiteFabFragment extends Fragment implements OnsiteFabListAdapter.JobAdapterListener,
        ServicesListener {

    private static final String TAG = "OnsiteFabFragment";
    AppSession appSession;
    FragmentOnsiteFabBinding binding;
    public String jobName, jobNumber, mKeyFrom;
    public int jobId;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean somePermissionsForeverDenied = false;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;
    OnsiteFabListAdapter adapter;
    ArrayList<FabDetailModel> mJobList = new ArrayList<>();

    public static OnsiteFabFragment newInstance(int jobId, String jobName, String jobNumber, String mKeyFrom, List<FabDetailModel> fabrications) {

        Bundle args = new Bundle();
        args.putString("job_name", jobName);
        args.putString("job_number", jobNumber);
        args.putInt("job_id", jobId);
        args.putString(KEY_FROM, mKeyFrom);
        args.putSerializable("list", (Serializable) fabrications);
        OnsiteFabFragment fragment = new OnsiteFabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSession = new AppSession(getActivity());
        checker = new PermissionsChecker(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_onsite_fab, container, false);
        binding.setFragment(this);

        if (getArguments() != null) {
            jobId = getArguments().getInt("job_id");
            jobName = getArguments().getString("job_name");
            jobNumber = getArguments().getString("job_number");
            mKeyFrom = getArguments().getString(KEY_FROM);
            mJobList = (ArrayList<FabDetailModel>) getArguments().getSerializable("list");
        }

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        if (mJobList!=null && !mJobList.isEmpty()){
            binding.recyclerView.setVisibility(View.VISIBLE);
            adapter = new OnsiteFabListAdapter(getActivity(), mJobList, this, TAG);
            binding.recyclerView.setAdapter(adapter);
        }else{
            binding.tvNoRecordFound.setVisibility(View.VISIBLE);
        }

        return binding.getRoot();
    }

    @Override
    public void success(String response, ServicesType type) {
        if (type == ServicesType.getAllParts) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                boolean statusCode = jsonObject.getBoolean(STATUS);
                boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                String message = jsonObject.optString(MESSAGE);
                String responseCode = jsonObject.optString(RESPONSE_CODE);

                if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                } else {
                    if (showMessage)
                        Utility.toaster(getActivity(), KEY_ERROR, message);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Utility.toaster(getActivity(), KEY_ERROR, getString(R.string.something_went_wrong));
            }
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(getActivity(), KEY_ERROR, message);
    }

    @Override
    public void onClicked(FabDetailModel post) {
        if (!Utility.isConnectingToInternet(getActivity())) {
            Utility.toaster(getActivity(), KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
        } else {

            if (checkPermission())
                startActivity(new Intent(getContext(), ViewDrawingActivity.class)
                        .putExtra("from", TYPE_ONLY_FAB).putExtra(ID, String.valueOf(post.getId())));

        }
    }

    private boolean checkPermission() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {
            return true;
        } else {
            if (somePermissionsForeverDenied) {
                View contextView = binding.layoutMain;

                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getActivity().getColor(R.color.colorRed));

                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);

                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getActivity().getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
                return false;
            } else {
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                return false;
            }

        }
    }
}