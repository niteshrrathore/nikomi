package com.workflow.nikomi.interfaces;

public interface CallbackOKCancelInterface {
    public void onSuccess(boolean isSuccess);
    public void onFailure(boolean isSuccess);
}
