package com.workflow.nikomi.interfaces;

import android.graphics.Bitmap;
import android.widget.Spinner;

import com.workflow.nikomi.model.UserDetail;

public interface CallBackSignatureSave {
    void onOkClicked(Bitmap bitmapSignature, int spinnerId, UserDetail mUserDetail);
    void onOkClicked(Bitmap bitmapSignature);
    void onDismiss(Spinner mSpinner);
}
