package com.workflow.nikomi.interfaces;

import com.workflow.nikomi.enums.ServicesType;

public interface ServicesListener {

    void success(String response, ServicesType type);

    void failed(String message, ServicesType type);

}