package com.workflow.nikomi.interfaces;

public interface CallBackOk {
    void onOkClicked(String strNoPartCompleted);
}
