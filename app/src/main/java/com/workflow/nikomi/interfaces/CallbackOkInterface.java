package com.workflow.nikomi.interfaces;

public interface CallbackOkInterface {
    public void onSuccess(boolean isSuccess);
}
