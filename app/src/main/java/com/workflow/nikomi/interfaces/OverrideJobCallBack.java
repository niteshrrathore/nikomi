package com.workflow.nikomi.interfaces;

public interface OverrideJobCallBack {
    void onYes();
    void onNo();
}
