package com.workflow.nikomi.interfaces;

public interface UpdatePagerFragment {
    void update(String xyzData, int selectedFrag);
}
