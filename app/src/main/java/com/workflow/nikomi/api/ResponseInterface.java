package com.workflow.nikomi.api;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public interface ResponseInterface {

    @FormUrlEncoded
    @POST("api/login")
    Call<JsonObject> getLogin(@HeaderMap Map<String, String> headers, @FieldMap Map<String, String> otherData);

    @FormUrlEncoded
    @POST("api/forgot_password")
    Call<JsonObject> forgotPassword(@HeaderMap Map<String, String> headers, @FieldMap Map<String, String> otherData);

    @FormUrlEncoded
    @POST("api/change_password")
    Call<JsonObject> changePassword(@HeaderMap Map<String, String> headers, @FieldMap Map<String, String> otherData);

    @FormUrlEncoded
    @POST("api/worker_productivities")
    Call<JsonObject> workerProductivities(@HeaderMap Map<String, String> headers, @FieldMap Map<String, String> otherData);

    @GET("api/jobs")
    Call<JsonObject> getJobList(@HeaderMap Map<String, String> headers, @QueryMap Map<String, String> otherData);

    @GET("api/parts")
    Call<JsonObject> getAllParts(@HeaderMap Map<String, String> headers, @QueryMap Map<String, String> otherData);

    @GET("api/workers")
    Call<JsonObject> getAllWorkers(@HeaderMap Map<String, String> headers, @QueryMap Map<String, String> otherData);

    @GET("api/parts/{id}")
    Call<JsonObject> getPartDetails(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("api/fabrications/{id}")
    Call<JsonObject> getFabDetails(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("api/fab_drawing/get/{id}")
    Call<JsonObject> getFabDrawing(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("api/jobs/general_assembly/get/{id}")
    Call<JsonObject> getOnsiteDoc(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("api/part_drawing/get/{id}")
    Call<JsonObject> getPartDrawing(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("api/jobs/{id}")
    Call<JsonObject> getJobDetails(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("api/master_data")
    Call<JsonObject> getMasterData(@HeaderMap Map<String, String> headers);

    @GET("api/profile")
    Call<JsonObject> getProfile(@HeaderMap Map<String, String> headers);

    @GET("api/logout")
    Call<JsonObject> logout(@HeaderMap Map<String, String> headers);

    @GET("api/jobs/onsite")
    Call<JsonObject> getJobsForOnsite(@HeaderMap Map<String, String> headers, @QueryMap Map<String, String> otherData);

    @Multipart
    @POST("api/update/profile")
    Call<JsonObject> updateProfile(@HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> otherData);

    @PUT("api/parts/{id}")
    Call<JsonObject> updatePartAction(@HeaderMap Map<String, String> headers, @Path("id") String id, @QueryMap Map<String, String> otherData);

    @PUT("api/jobs/{id}")
    Call<JsonObject> updateJobAction(@HeaderMap Map<String, String> headers, @Path("id") String id, @QueryMap Map<String, String> otherData);

    @PUT("api/fabrications/{id}")
    Call<JsonObject> updateFabAction(@HeaderMap Map<String, String> headers, @Path("id") String id, @QueryMap Map<String, String> otherData);

    @Multipart
    @POST("api/parts/signature/{id}")
    Call<JsonObject> updateUserWithSignatureForPart(@HeaderMap Map<String, String> headers, @Path("id") String id, @PartMap Map<String, RequestBody> otherData);

    @Multipart
    @POST("api/fabrications/signature/{id}")
    Call<JsonObject> updateUserWithSignatureForFab(@HeaderMap Map<String, String> headers, @Path("id") String id, @PartMap Map<String, RequestBody> otherData);

    @Multipart
    @POST("api/jobs/signature/{id}")
    Call<JsonObject> updateSignatureForJob(@HeaderMap Map<String, String> headers, @Path("id") String id, @PartMap Map<String, RequestBody> otherData);

    @Multipart
    @POST("api/part_files")
    Call<JsonObject> uploadPhotosForPart(@HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> otherData);

    @Multipart
    @POST("api/fab_files")
    Call<JsonObject> uploadPhotosForFab(@HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> otherData);

    @Multipart
    @POST("api/job_files")
    Call<JsonObject> uploadPhotosForJob(@HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> otherData);

    @GET("api/timesheet")
    Call<JsonObject> getJobTimesheetForOnsiteWorker(@HeaderMap Map<String, String> headers, @QueryMap Map<String, String> otherData);

    @GET("api/job_files")
    Call<JsonObject> getJobFiles(@HeaderMap Map<String, String> headers, @QueryMap Map<String, String> otherData);

    @GET("api/release_versions")
    Call<JsonObject> getVersionCodeDetails(@HeaderMap Map<String, String> headers, @QueryMap Map<String, String> otherData);


}
