package com.workflow.nikomi.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Base64;
import android.util.Log;

import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.LoginActivity;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.utils.App;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.CustomProgressDialog;
import com.workflow.nikomi.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.workflow.nikomi.enums.ServicesType.noInternetConnection;
import static com.workflow.nikomi.utils.App.DEV_BASE_URL;
import static com.workflow.nikomi.utils.App.Live_URL;
import static com.workflow.nikomi.utils.Constant.CUT_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.CUT_CHECKED_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.DRILL_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.DRILL_CHECKED_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.EMAIL;
import static com.workflow.nikomi.utils.Constant.FABRICATION_ID;
import static com.workflow.nikomi.utils.Constant.FAB_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.FAB_CHECKED_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.FILE_TYPE;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.INSPECT_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.JOB_ID;
import static com.workflow.nikomi.utils.Constant.MOBILE;
import static com.workflow.nikomi.utils.Constant.NAME;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.WELD_BY_USER_ID;
import static com.workflow.nikomi.utils.Constant.WELD_CHECKED_BY_USER_ID;


/**
 * Created by Iziss-N
 */
public class APIRequest {

    static Retrofit retrofit;
    static Retrofit retrofit2;

    /*QA url*/
    //public static final String Live_URL = "http://ec2-3-10-142-63.eu-west-2.compute.amazonaws.com/";

    /*Dev url*/
    //public static final String Live_URL = "http://ec2-3-11-80-80.eu-west-2.compute.amazonaws.com/";
    private static final String TAG = "APIRequest";

    private APIRequest() {

    }

    public static NotificationApiService getApiService() {
        return new Retrofit.Builder()
                .baseUrl("https://fcm.googleapis.com/")
                .client(provideClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NotificationApiService.class);
    }

    private static OkHttpClient provideClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public okhttp3.Response intercept(@NotNull Chain chain) throws IOException {
                Request request = chain.request();
                return chain.proceed(request);
            }
        }).build();
    }

    private static Retrofit provideRestAdapter() {
        if (retrofit == null) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            if (Live_URL!=null && !Live_URL.isEmpty())
            retrofit = new Retrofit.Builder()
                    .baseUrl(Live_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(App.getInstance().getOkHttpClient())
                    .build();
            else
            retrofit = new Retrofit.Builder()
                    .baseUrl(DEV_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(App.getInstance().getOkHttpClient())
                    .build();

            return retrofit;

        } else {
            return retrofit;
        }

    }

    public static ResponseInterface provideInterface() {
        return provideRestAdapter().create(ResponseInterface.class);
    }

    private static Retrofit provideRestAdapterAddress() {
        if (retrofit2 == null)
            retrofit2 = new Retrofit.Builder()
                    .baseUrl(Live_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(App.getInstance().getOkHttpClient())
                    .build();
        return retrofit2;
    }

    /*Common method for most of the requests*/
    public static void generalApiRequest(final Context context, final Activity activity, final ServicesListener listener,
                                         final HashMap<String, String> parms, final ServicesType type, boolean showLoader) {
        try {
            //Loading....
            final CustomProgressDialog dialog = new CustomProgressDialog(activity);
            final AppSession appSession = new AppSession(activity);
            if (showLoader)
                dialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {

                    ResponseInterface userDataRequest = APIRequest.provideInterface();
                    Call<JsonObject> registerUser = null;
                    if (type == ServicesType.login) {
                        registerUser = userDataRequest.getLogin(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.logout) {
                        registerUser = userDataRequest.logout(getHeaderMaps(activity, type));
                    } else if (type == ServicesType.forgotPassword) {
                        registerUser = userDataRequest.forgotPassword(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.changePassword) {
                        registerUser = userDataRequest.changePassword(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.getProfile) {
                        registerUser = userDataRequest.getProfile(getHeaderMaps(activity, type));
                    } else if (type == ServicesType.getMasterData) {
                        registerUser = userDataRequest.getMasterData(getHeaderMaps(activity, type));
                    } else if (type == ServicesType.getAllJobs) {
                        registerUser = userDataRequest.getJobList(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.getAllJobsForOnsite) {
                        registerUser = userDataRequest.getJobsForOnsite(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.getAllParts) {
                        registerUser = userDataRequest.getAllParts(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.getAllWorkers) {
                        registerUser = userDataRequest.getAllWorkers(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.getPartDetails) {
                        registerUser = userDataRequest.getPartDetails(getHeaderMaps(activity, type), parms.get(ID));
                    } else if (type == ServicesType.getJobDetails) {
                        registerUser = userDataRequest.getJobDetails(getHeaderMaps(activity, type), parms.get(ID));
                    } else if (type == ServicesType.partDrawing) {
                        registerUser = userDataRequest.getPartDrawing(getHeaderMaps(activity, type), parms.get(ID));
                    } else if (type == ServicesType.getFabDetails) {
                        registerUser = userDataRequest.getFabDetails(getHeaderMaps(activity, type), parms.get(ID));
                    } else if (type == ServicesType.fabDrawing) {
                        registerUser = userDataRequest.getFabDrawing(getHeaderMaps(activity, type), parms.get(ID));
                    } else if (type == ServicesType.getOnsiteDoc) {
                        registerUser = userDataRequest.getOnsiteDoc(getHeaderMaps(activity, type), parms.get(ID));
                    } else if (type == ServicesType.workerProductivities) {
                        registerUser = userDataRequest.workerProductivities(getHeaderMaps(activity, type), parms);
                    } else if (type == ServicesType.updatePartAction) {
                        String id = parms.get(ID);
                        parms.remove(id);
                        registerUser = userDataRequest.updatePartAction(getHeaderMaps(activity, type), id, parms);
                    } else if (type == ServicesType.updateFabAction) {
                        String id = parms.get(ID);
                        parms.remove(id);
                        registerUser = userDataRequest.updateFabAction(getHeaderMaps(activity, type), id, parms);
                    }else if (type == ServicesType.updateJobAction) {
                        String id = parms.get(ID);
                        parms.remove(id);
                        registerUser = userDataRequest.updateJobAction(getHeaderMaps(activity, type), id, parms);
                    }else if(type == ServicesType.getJobsheetForOnsiteWorker){
                        registerUser = userDataRequest.getJobTimesheetForOnsiteWorker(getHeaderMaps(activity, type), parms);
                    }else if (type == ServicesType.getGADocument){
                        registerUser = userDataRequest.getJobFiles(getHeaderMaps(activity, type), parms);
                    }else if (type == ServicesType.getVersionCodeDetails){
                        registerUser = userDataRequest.getVersionCodeDetails(getHeaderMaps(activity, type), parms);
                    }

                    if (!Utility.isConnectingToInternet(context)) {

                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                listener.failed(activity.getResources().getString(R.string.please_check_your_network_connection_and_try_again), noInternetConnection);
                            }
                        });

                    } else {

                        registerUser.enqueue(new Callback<JsonObject>() {

                            @Override
                            public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {

                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (response.isSuccessful()) {
                                            String responseString = response.body().toString();
                                            Log.e("responseString>>>>", "" + responseString);
                                            try {
                                                int mStatus = new JSONObject(responseString).optInt("status");
                                                if (mStatus != 401) {
                                                    listener.success(responseString, type);
                                                } else {
                                                    BaseActivity.stopAutoSignoutService();
                                                    Intent intent = new Intent(activity, LoginActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    appSession.clearPrefs();
                                                    activity.startActivity(intent);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                BaseActivity.stopAutoSignoutService();
                                                Intent intent = new Intent(activity, LoginActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                appSession.clearPrefs();
                                                activity.startActivity(intent);
                                            }

                                        } else {
                                            try {
                                                if (response.code()==102)
                                                    Utility.showUserInActiveDialog(activity,activity.getString(R.string.account_deactivated_deleted_msg));
                                                else if (response.code()==401){
                                                    Utility.toaster(activity,true,activity.getString(R.string.your_session_is_expired));
                                                    BaseActivity.stopAutoSignoutService();
                                                    Intent intent = new Intent(activity, LoginActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    appSession.clearPrefs();
                                                    activity.startActivity(intent);
                                                }else{
                                                    JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                                    listener.failed(jsonObject.optString("message"), type);
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });

                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                                Log.e("Main Activity", "onFailure() called with: " + "call = [" + call + "], t = [" + t.getMessage() + "]");

                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }

                                listener.failed("Failed" + t.getMessage(), type);

                            }

                        });
                    }

                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void makeRequestToServerNewJsonArrayParameter(final boolean showLoader, final Context mContext, final Activity mActivity,
                                                                final ServicesType requestedMethod, final ServicesListener mOnResponseListener,
                                                                final ArrayList<File> paramsArray,
                                                                final HashMap<String, String> paramsString) {
        try {
            //Loading....
            final CustomProgressDialog dialog = new CustomProgressDialog(mActivity);

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (showLoader)
                        dialog.show();
                }
            });

            new Thread(new Runnable() {
                @Override
                public void run() {
                    MultipartBody.Part body = null;

                    Call<JsonObject> makeRequest = null;

                    try {
                        makeRequest = getCallRequestNewJsonArray(mContext, mActivity, requestedMethod, paramsArray, paramsString, body);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (!Utility.isConnectingToInternet(mContext)) {
                        if (showLoader)
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                        mOnResponseListener.failed(mActivity.getResources().getString(R.string.please_check_your_network_connection_and_try_again), noInternetConnection);

                    } else {
                        if (makeRequest != null) {
                            makeRequest.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {

                                    mActivity.runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            if (response.isSuccessful()) {

                                                if (showLoader)
                                                    if (dialog != null) {
                                                        dialog.dismiss();
                                                    }

                                                String responseString = response.body().toString();

                                                mOnResponseListener.success(responseString, requestedMethod);

                                            } else {
                                                if (dialog != null) {
                                                    dialog.dismiss();
                                                }
                                                try {
                                                    mOnResponseListener.failed(new JSONObject(response.errorBody().string()).optString("message"), requestedMethod);//
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    Log.d("Login Activity", "onFailure() called with: " + "call = [" + call + "], t = [" + t.getMessage() + "]");
                                    mOnResponseListener.failed(t.getMessage(), requestedMethod);
                                    if (showLoader)
                                        if (dialog != null) {
                                            dialog.dismiss();
                                        }
                                }
                            });
                        }
                    }


                }
            }).start();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Call<JsonObject> getCallRequestNewJsonArray(Context context, Activity mActivity, ServicesType mRequestMethod,
                                                              final ArrayList<File> paramsArray,
                                                              final HashMap<String, String> paramsString, final MultipartBody.Part body) {

        ResponseInterface dataRequest = APIRequest.provideInterface();

        Call<JsonObject> request = null;
        switch (mRequestMethod) {

            case updateProfile:
                request = dataRequest.updateProfile(getHeaderMaps(mActivity, mRequestMethod), createMultipartRequestFromJsonArray(context, paramsArray, paramsString));
                break;

            case updateUserWithSignatureForPart:
                request = dataRequest.updateUserWithSignatureForPart(getHeaderMaps(mActivity, mRequestMethod), paramsString.get(PART_ID), createMultipartRequestFromPart(context, paramsArray, paramsString));
                break;

            case updateUserWithSignatureForFab:
                request = dataRequest.updateUserWithSignatureForFab(getHeaderMaps(mActivity, mRequestMethod), paramsString.get(FABRICATION_ID), createMultipartRequestFromFab(context, paramsArray, paramsString));
                break;

            case updateUserWithSignatureForJob:
                request = dataRequest.updateSignatureForJob(getHeaderMaps(mActivity, mRequestMethod), paramsString.get(JOB_ID), createMultipartRequestFromJob(context, paramsArray, paramsString));
                break;

            case uploadPhotosForPart:
                request = dataRequest.uploadPhotosForPart(getHeaderMaps(mActivity, mRequestMethod), createMultipartRequestFromUploadPartPhotos(context, paramsArray, paramsString));
                break;

            case uploadPhotosForFab:
                request = dataRequest.uploadPhotosForFab(getHeaderMaps(mActivity, mRequestMethod), createMultipartRequestFromUploadFabPhotos(context, paramsArray, paramsString));
                break;

            case uploadPhotosForJob:
                request = dataRequest.uploadPhotosForJob(getHeaderMaps(mActivity, mRequestMethod), createMultipartRequestFromUploadJobPhotos(context, paramsArray, paramsString));
                break;
        }

        return request;
    }

    public static HashMap<String, RequestBody> createMultipartRequestFromJsonArray(Context mContext, ArrayList<File> imageArray,
                                                                                   HashMap<String, String> jsonParamsString) {

        HashMap<String, RequestBody> parametersMap = new HashMap<>();
        try {

            parametersMap.put(NAME, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(NAME)));
            parametersMap.put(EMAIL, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(EMAIL)));
            parametersMap.put(MOBILE, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(MOBILE)));

            if (!imageArray.isEmpty()) {

                for (int i = 0; i < imageArray.size(); i++) {

                    RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpg"), imageArray.get(i));
                    parametersMap.put("profile\"; filename=\"" + imageArray.get(i).getName(), fileBody);
                    Log.e(TAG, "getSignUpParams: " + imageArray.get(i));
                }

                return parametersMap;
            } else {
                //parametersMap.put(JSON, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(JSON)));
                return parametersMap;
            }


        } catch (Exception ex) {
            Log.e(TAG, "createMultipartRequestFromJsonArray: " + ex.getMessage());
        }
        return null;
    }

    public static HashMap<String, RequestBody> createMultipartRequestFromUploadPartPhotos(Context mContext, ArrayList<File> imageArray,
                                                                                          HashMap<String, String> jsonParamsString) {

        HashMap<String, RequestBody> parametersMap = new HashMap<>();
        try {

            parametersMap.put(PART_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(PART_ID)));
            parametersMap.put(FILE_TYPE, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(FILE_TYPE)));

            if (!imageArray.isEmpty()) {

                for (int i = 0; i < imageArray.size(); i++) {

                    RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpg"), imageArray.get(i));
                    parametersMap.put("file[]\"; filename=\"" + imageArray.get(i).getName(), fileBody);
                    Log.e(TAG, "getSignUpParams: " + imageArray.get(i));
                }

                return parametersMap;
            } else {
                //parametersMap.put(JSON, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(JSON)));
                return parametersMap;
            }


        } catch (Exception ex) {
            Log.e(TAG, "createMultipartRequestFromJsonArray: " + ex.getMessage());
        }
        return null;
    }

    public static HashMap<String, RequestBody> createMultipartRequestFromUploadFabPhotos(Context mContext, ArrayList<File> imageArray,
                                                                                         HashMap<String, String> jsonParamsString) {

        HashMap<String, RequestBody> parametersMap = new HashMap<>();
        try {

            parametersMap.put(FABRICATION_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(FABRICATION_ID)));
            parametersMap.put(FILE_TYPE, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(FILE_TYPE)));

            if (!imageArray.isEmpty()) {

                for (int i = 0; i < imageArray.size(); i++) {

                    RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpg"), imageArray.get(i));
                    parametersMap.put("file[]\"; filename=\"" + imageArray.get(i).getName(), fileBody);
                    Log.e(TAG, "getSignUpParams: " + imageArray.get(i));
                }

                return parametersMap;
            } else {
                //parametersMap.put(JSON, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(JSON)));
                return parametersMap;
            }


        } catch (Exception ex) {
            Log.e(TAG, "createMultipartRequestFromJsonArray: " + ex.getMessage());
        }
        return null;
    }

    public static HashMap<String, RequestBody> createMultipartRequestFromUploadJobPhotos(Context mContext, ArrayList<File> imageArray,
                                                                                         HashMap<String, String> jsonParamsString) {

        HashMap<String, RequestBody> parametersMap = new HashMap<>();
        try {

            parametersMap.put(JOB_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(JOB_ID)));
            parametersMap.put(FILE_TYPE, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(FILE_TYPE)));

            if (!imageArray.isEmpty()) {

                for (int i = 0; i < imageArray.size(); i++) {

                    RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpg"), imageArray.get(i));
                    parametersMap.put("file[]\"; filename=\"" + imageArray.get(i).getName(), fileBody);
                    Log.e(TAG, "getSignUpParams: " + imageArray.get(i));
                }

                return parametersMap;
            } else {
                //parametersMap.put(JSON, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(JSON)));
                return parametersMap;
            }


        } catch (Exception ex) {
            Log.e(TAG, "createMultipartRequestFromJsonArray: " + ex.getMessage());
        }
        return null;
    }

    public static HashMap<String, RequestBody> createMultipartRequestFromPart(Context mContext, ArrayList<File> imageArray,
                                                                              HashMap<String, String> jsonParamsString) {

        HashMap<String, RequestBody> parametersMap = new HashMap<>();
        try {

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpg"), imageArray.get(0));

            if (jsonParamsString.containsKey(CUT_BY_USER_ID)) {
                parametersMap.put(CUT_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(CUT_BY_USER_ID)));
                parametersMap.put("cut_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(CUT_CHECKED_BY_USER_ID)) {
                parametersMap.put(CUT_CHECKED_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(CUT_CHECKED_BY_USER_ID)));
                parametersMap.put("cut_checked_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(DRILL_BY_USER_ID)) {
                parametersMap.put(DRILL_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(DRILL_BY_USER_ID)));
                parametersMap.put("drill_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(DRILL_CHECKED_BY_USER_ID)) {
                parametersMap.put(DRILL_CHECKED_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(DRILL_CHECKED_BY_USER_ID)));
                parametersMap.put("drill_checked_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(INSPECT_BY_USER_ID)) {
                parametersMap.put(INSPECT_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(INSPECT_BY_USER_ID)));
                parametersMap.put("inspect_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }


            return parametersMap;

        } catch (Exception ex) {
            Log.e(TAG, "createMultipartRequestFromJsonArray: " + ex.getMessage());
        }
        return null;
    }

    public static HashMap<String, RequestBody> createMultipartRequestFromFab(Context mContext, ArrayList<File> imageArray,
                                                                             HashMap<String, String> jsonParamsString) {

        HashMap<String, RequestBody> parametersMap = new HashMap<>();
        try {

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpg"), imageArray.get(0));

            if (jsonParamsString.containsKey(FAB_BY_USER_ID)) {
                parametersMap.put(FAB_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(FAB_BY_USER_ID)));
                parametersMap.put("fab_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(FAB_CHECKED_BY_USER_ID)) {
                parametersMap.put(FAB_CHECKED_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(FAB_CHECKED_BY_USER_ID)));
                parametersMap.put("fab_checked_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(WELD_BY_USER_ID)) {
                parametersMap.put(WELD_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(WELD_BY_USER_ID)));
                parametersMap.put("weld_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(WELD_CHECKED_BY_USER_ID)) {
                parametersMap.put(WELD_CHECKED_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(WELD_CHECKED_BY_USER_ID)));
                parametersMap.put("weld_checked_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }

            if (jsonParamsString.containsKey(INSPECT_BY_USER_ID)) {
                parametersMap.put(INSPECT_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(INSPECT_BY_USER_ID)));
                parametersMap.put("inspect_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }


            return parametersMap;

        } catch (Exception ex) {
            Log.e(TAG, "createMultipartRequestFromJsonArray: " + ex.getMessage());
        }
        return null;
    }

    public static HashMap<String, RequestBody> createMultipartRequestFromJob(Context mContext, ArrayList<File> imageArray,
                                                                             HashMap<String, String> jsonParamsString) {

        HashMap<String, RequestBody> parametersMap = new HashMap<>();
        try {

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpg"), imageArray.get(0));
            parametersMap.put("inspect_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            /*if (jsonParamsString.containsKey(INSPECT_BY_USER_ID)) {
                parametersMap.put(INSPECT_BY_USER_ID, RequestBody.create(MediaType.parse("text/plain"), "" + jsonParamsString.get(INSPECT_BY_USER_ID)));
                parametersMap.put("inspect_by_signature\"; filename=\"" + imageArray.get(0).getName(), fileBody);
            }*/

            return parametersMap;

        } catch (Exception ex) {
            Log.e(TAG, "createMultipartRequestFromJsonArray: " + ex.getMessage());
        }
        return null;
    }

    public static Map<String, String> getHeaderMaps(Context activity, ServicesType type) {

        String deviceVersion = "";
        String releaseVersion = "";

        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            releaseVersion = pInfo.versionName;
            deviceVersion = android.os.Build.VERSION.RELEASE;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        AppSession appSession = new AppSession(activity);

        Map<String, String> params = new HashMap<>();

        if (type == ServicesType.login || type == ServicesType.forgotPassword) {
            Log.e(TAG, "getHeaderMaps:");
        } else {
            params.put("Authorization", "Bearer " + appSession.getSessionToken());
        }

        //params.put("Authorization", getAuthToken());
        //params.put("deviceType", "1");
        params.put("Device-Token", appSession.getUserDeviceToken());
        params.put("App-Version", releaseVersion);
        params.put("Os-Version", deviceVersion);
        params.put("Device-Type", "ANDROID");
        params.put("Accept-Language", "en"/*appSession.getLang().getLanguageName()*/);
        return params;
    }

    public static String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = ("admin" + ":" + "sd@auth").getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

}
