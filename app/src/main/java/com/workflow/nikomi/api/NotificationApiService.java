package com.workflow.nikomi.api;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface NotificationApiService {

    @Headers({
            "Authorization: key="+"AAAA3VC_zLA:APA91bE91ocgn8GJwb0JoS824fIEqCF0weJp-k3d4S6zf00lnVLZFNebuqhl1BJmZuAc8qmyVAhPFZ3m4h_WursChMKBlaMcU-3VxnM9Pk3uEpI9zahjnq-WMjPG1ALveGr4m4rFDGtu" ,
            "Content-Type: application/json"
    })
    @POST("fcm/send")
    Call<JsonObject> sendNotification(@Body JsonObject payload);
}
