package com.workflow.nikomi.enums;

/**
 * Created by iziss on 12/06/2020.
 */
public enum ServicesType {

    login,
    logout,
    noInternetConnection,
    forgotPassword,
    changePassword,
    updateProfile,
    getProfile,
    getAllJobs,
    getAllJobsForOnsite,
    getAllParts,
    getAllWorkers,
    getPartDetails,
    getJobDetails,
    getFabDetails,
    updatePartAction,
    updateFabAction,
    updateJobAction,
    partDrawing,
    fabDrawing,
    getOnsiteDoc,
    updateUserWithSignatureForJob,
    updateUserWithSignatureForPart,
    updateUserWithSignatureForFab,
    uploadPhotosForPart,
    uploadPhotosForFab,
    uploadPhotosForJob,
    workerProductivities,
    getMasterData,
    getJobsheetForWorkshopWorker,
    getJobsheetForOnsiteWorker,
    getGADocument,
    getVersionCodeDetails

}


