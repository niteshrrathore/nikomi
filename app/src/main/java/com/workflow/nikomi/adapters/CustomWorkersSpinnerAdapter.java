package com.workflow.nikomi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.model.UserDetail;

import java.util.ArrayList;


public class CustomWorkersSpinnerAdapter extends ArrayAdapter<UserDetail> {

    ArrayList<UserDetail> countrylist;
    LayoutInflater flater;

    public CustomWorkersSpinnerAdapter(Context context, int resouceId, ArrayList<UserDetail> objects) {
        super(context, resouceId, objects);
        countrylist = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        UserDetail rowItem = getItem(position);

        viewHolder holder;
        View rowview = convertView;
        if (rowview == null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.spinner_text, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(android.R.id.text1);
            rowview.setTag(holder);
        } else {
            holder = (viewHolder) rowview.getTag();
        }

        holder.txtTitle.setText(rowItem.getName());

        return rowview;

    }

    private class viewHolder {
        TextView txtTitle;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        UserDetail rowItem = getItem(position);

        viewHolder holder;
        View rowview = convertView;
        if (rowview == null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.simple_spinner_dropdown, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(R.id.text1);
            rowview.setTag(holder);
        } else {
            holder = (viewHolder) rowview.getTag();
        }

        holder.txtTitle.setText(rowItem.getName());

        return rowview;
    }
}


