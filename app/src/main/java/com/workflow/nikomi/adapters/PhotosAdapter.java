package com.workflow.nikomi.adapters;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.databinding.RowJobLayoutBinding;
import com.workflow.nikomi.databinding.RowPhotoLayoutBinding;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.utils.Utility;

import java.io.File;
import java.util.ArrayList;

import static com.workflow.nikomi.utils.Constant.STATUS_TYPE_JOB;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.MyViewHolder> {

    Activity mContext;
    //JobAdapterListener mListener;
    ArrayList<File> mImgList;
    private static final String TAG = "ImageItemAdapter";

    public PhotosAdapter(Activity applicationContext, ArrayList<File> imgList/*, JobAdapterListener listener*/) {
        this.mContext = applicationContext;
        this.mImgList = imgList;
        //this.mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowPhotoLayoutBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_photo_layout, parent, false);

        return new MyViewHolder(layoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        //holder.layoutBinding.setViewModel(mImgList.get(position));
        holder.layoutBinding.imgPhoto.setImageURI(Uri.fromFile( mImgList.get(position)));

        /*holder.layoutBinding.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClicked(mImgList.get(position));
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {

        return mImgList.size();

    }

    public void updateList(ArrayList<File> mNewImgList){
        this.mImgList = mNewImgList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        RowPhotoLayoutBinding layoutBinding;

        public MyViewHolder(RowPhotoLayoutBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

    /*public interface JobAdapterListener {
        void onClicked(JobModel post);
    }*/
}
