package com.workflow.nikomi.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.databinding.RowJobTimesheetOnsiteBinding;
import com.workflow.nikomi.model.JobTimesheetOnsiteModel;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.Utility;

import java.util.ArrayList;

public class JobTimesheetOnsiteAdapter extends RecyclerView.Adapter<JobTimesheetOnsiteAdapter.MyViewHolder> {

    Activity mContext;
    JobTimesheetOnsiteAdapterListener mListener;
    ArrayList<JobTimesheetOnsiteModel> mImgList;
    String mFragmentTag;
    AppSession appSession;
    private static final String TAG = "ImageItemAdapter";

    public JobTimesheetOnsiteAdapter(Activity applicationContext, ArrayList<JobTimesheetOnsiteModel> imgList, JobTimesheetOnsiteAdapterListener listener, String fragmentTag) {
        appSession = new AppSession(applicationContext);
        this.mContext = applicationContext;
        this.mImgList = imgList;
        this.mListener = listener;
        this.mFragmentTag = fragmentTag;
    }

    @Override
    public JobTimesheetOnsiteAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowJobTimesheetOnsiteBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_job_timesheet_onsite, parent, false);

        return new JobTimesheetOnsiteAdapter.MyViewHolder(layoutBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.layoutBinding.setJobTimesheetOnsiteModel(mImgList.get(position));


        holder.layoutBinding.tvAndFabNumber.setVisibility(View.VISIBLE);
        holder.layoutBinding.tvAndFabNumber.setText(Utility.getPartAndFabNum(mContext,
                mImgList.get(position).getPartsCount(), mImgList.get(position).getFabricationsCount()));

        if (mImgList.get(position).getTotalTimeSpent() !=null){
            holder.layoutBinding.tvJobTime.setText(mImgList.get(position).getTotalTimeSpent());
        }

        holder.layoutBinding.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {

                    mListener.onClicked(mImgList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImgList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowJobTimesheetOnsiteBinding layoutBinding;

        public MyViewHolder(RowJobTimesheetOnsiteBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

    public interface JobTimesheetOnsiteAdapterListener {
        void onClicked(JobTimesheetOnsiteModel post/*, boolean isPartIsInProgress*/);
    }

}
