package com.workflow.nikomi.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.databinding.RowPhotoLayoutBinding;
import com.workflow.nikomi.model.PartInspectPhotoModel;
import com.workflow.nikomi.utils.Utility;

import java.util.ArrayList;

public class InspectedPhotosAdapter extends RecyclerView.Adapter<InspectedPhotosAdapter.MyViewHolder> {

    Activity mContext;
    ArrayList<PartInspectPhotoModel> mImgList;
    private static final String TAG = "ImageItemAdapter";

    public InspectedPhotosAdapter(Activity applicationContext, ArrayList<PartInspectPhotoModel> imgList/*, JobAdapterListener listener*/) {
        this.mContext = applicationContext;
        this.mImgList = imgList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowPhotoLayoutBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_photo_layout, parent, false);

        return new MyViewHolder(layoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        Utility.loadImageFromPicasso(mContext, mImgList.get(position).getFileLocation(),mContext.getDrawable(R.drawable.placeholder),holder.layoutBinding.imgPhoto);

        /*holder.layoutBinding.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClicked(mImgList.get(position));
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {

        return mImgList.size();

    }

    public void updateList(ArrayList<PartInspectPhotoModel> mNewImgList){
        this.mImgList = mNewImgList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        RowPhotoLayoutBinding layoutBinding;

        public MyViewHolder(RowPhotoLayoutBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

}
