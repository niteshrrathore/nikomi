package com.workflow.nikomi.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.ViewDrawingActivity;
import com.workflow.nikomi.databinding.RowFabPartIncludedBinding;
import com.workflow.nikomi.databinding.RowJobDetailLayoutBinding;
import com.workflow.nikomi.model.JobDetailModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.utils.Utility;

import java.util.ArrayList;

import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.STATUS_TYPE_JOB;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_PART;

public class FabPartIncludedListAdapter extends RecyclerView.Adapter<FabPartIncludedListAdapter.MyViewHolder> {

    Activity mContext;
    ArrayList<PartDetailModel> mImgList;

    public FabPartIncludedListAdapter(Activity applicationContext, ArrayList<PartDetailModel> imgList) {
        this.mContext = applicationContext;
        this.mImgList = imgList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowFabPartIncludedBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_fab_part_included, parent, false);

        return new MyViewHolder(layoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        SpannableString content = new SpannableString(mImgList.get(position).getName());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        holder.layoutBinding.tvFabPartIncludedName.setText(content);
        holder.layoutBinding.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, ViewDrawingActivity.class)
                        .putExtra("from", TYPE_ONLY_PART)
                        .putExtra(ID, String.valueOf(mImgList.get(position).getId())));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mImgList.size() > 10)
            return 10;
        else
            return mImgList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowFabPartIncludedBinding layoutBinding;

        public MyViewHolder(RowFabPartIncludedBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

}
