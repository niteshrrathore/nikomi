package com.workflow.nikomi.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.databinding.RowJobLayoutBinding;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.JobPartStatusModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.Utility;

import java.util.ArrayList;

import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.STATUS_TYPE_JOB;
import static com.workflow.nikomi.utils.Constant.WORKSHOP_WORKER;

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.MyViewHolder> {

    Activity mContext;
    JobAdapterListener mListener;
    ArrayList<JobModel> mImgList;
    String mFragmentTag;
    AppSession appSession;
    private static final String TAG = "ImageItemAdapter";

    public JobListAdapter(Activity applicationContext, ArrayList<JobModel> imgList, JobAdapterListener listener, String fragmentTag) {
        appSession = new AppSession(applicationContext);
        this.mContext = applicationContext;
        this.mImgList = imgList;
        this.mListener = listener;
        this.mFragmentTag = fragmentTag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowJobLayoutBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_job_layout, parent, false);

        return new MyViewHolder(layoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.layoutBinding.setViewModel(mImgList.get(position));

        if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER)) {
            holder.layoutBinding.tvAndFabNumber.setVisibility(View.VISIBLE);
            holder.layoutBinding.tvAndFabNumber.setText(Utility.getPartAndFabNum(mContext,
                    mImgList.get(position).getParts_count(), mImgList.get(position).getFabricationsCount()));
        } else {
            holder.layoutBinding.tvAndFabNumber.setVisibility(View.GONE);
        }

        if (mFragmentTag.equals("JobFragment"))
            if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER))
                Utility.getStatus(mContext, mImgList.get(position).getWorkshopStatusId(), STATUS_TYPE_JOB, holder.layoutBinding.tvJobStatus);
            else {
                if (mImgList.get(position).getWorking_on() == 1){
                    holder.layoutBinding.imgGreenDot.setVisibility(View.VISIBLE);
                } else
                    holder.layoutBinding.imgGreenDot.setVisibility(View.GONE);

                Utility.getStatus(mContext, mImgList.get(position).getOnsiteStatusId(), STATUS_TYPE_JOB, holder.layoutBinding.tvJobStatus);
            }
        else
            holder.layoutBinding.tvJobStatus.setVisibility(View.GONE);

        holder.layoutBinding.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {

                    mListener.onClicked(mImgList.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mImgList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowJobLayoutBinding layoutBinding;

        public MyViewHolder(RowJobLayoutBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

    public interface JobAdapterListener {
        void onClicked(JobModel post/*, boolean isPartIsInProgress*/);
    }

}
