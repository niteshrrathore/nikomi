package com.workflow.nikomi.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.databinding.RowJobDetailLayoutBinding;
import com.workflow.nikomi.databinding.RowJobLayoutBinding;
import com.workflow.nikomi.model.JobDetailModel;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.Utility;

import java.util.ArrayList;

import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.STATUS_TYPE_JOB;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_FAB;

public class JobDetailListAdapter extends RecyclerView.Adapter<JobDetailListAdapter.MyViewHolder> {

    Activity mContext;
    JobDetailAdapterListener mListener;
    ArrayList<JobDetailModel> mImgList;
    String mKeyFrom;
    AppSession appSession;
    private static final String TAG = "ImageItemAdapter";

    public JobDetailListAdapter(Activity applicationContext, ArrayList<JobDetailModel> imgList, JobDetailAdapterListener listener, String from) {
        this.mContext = applicationContext;
        this.mImgList = imgList;
        this.mListener = listener;
        this.mKeyFrom = from;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowJobDetailLayoutBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_job_detail_layout, parent, false);

        return new MyViewHolder(layoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.layoutBinding.setViewModel(mImgList.get(position));
        if (mImgList.get(position).getType().equals(TYPE_ONLY_FAB))
            holder.layoutBinding.tvName.setVisibility(View.GONE);
        else {
            holder.layoutBinding.tvName.setVisibility(View.VISIBLE);
            holder.layoutBinding.tvName.setText(mImgList.get(position).getName());
        }

        if (mKeyFrom.equals("JobFragment")) {

            if (mImgList.get(position).getWorkedOn() == 1 && mImgList.get(position).getStatusId() == ACTION_PART_FAB_IN_PROGRESS){
                holder.layoutBinding.imgGreenDot.setVisibility(View.VISIBLE);
            } else
                holder.layoutBinding.imgGreenDot.setVisibility(View.GONE);

            Utility.getStatus(mContext, mImgList.get(position).getStatusId(), STATUS_TYPE_JOB, holder.layoutBinding.tvStatus);
        }


        holder.layoutBinding.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImgList.get(position).getWorkedOn() == 1 && mImgList.get(position).getStatusId() == ACTION_PART_FAB_IN_PROGRESS){
                    appSession = new AppSession(mContext);

                    if (!mImgList.get(position).getType().equals(TYPE_ONLY_FAB) && appSession.getUserPartDetails()==null){
                        PartDetailModel partDetailModel = new PartDetailModel();
                        partDetailModel.setName(mImgList.get(position).getName());
                        appSession.storeUserPartDetail(partDetailModel, ACTION_PART_FAB_IN_PROGRESS);
                    }else {
                        //clicked item type is TYPE_ONLY_FAB
                    }


                }

                mListener.onClicked(mImgList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {

        return mImgList.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowJobDetailLayoutBinding layoutBinding;

        public MyViewHolder(RowJobDetailLayoutBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

    public interface JobDetailAdapterListener {
        void onClicked(JobDetailModel post);
    }
}
