package com.workflow.nikomi.adapters;

import android.app.Activity;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.ViewDrawingActivity;
import com.workflow.nikomi.databinding.RowFabPartIncludedBinding;
import com.workflow.nikomi.databinding.RowViewDetailsDoumentsBinding;
import com.workflow.nikomi.model.DocumentDetailsModel;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.PartDetailModel;

import java.util.ArrayList;

public class DocumentDetailsAdapter extends RecyclerView.Adapter<DocumentDetailsAdapter.MyViewHolder> {

    Activity mContext;
    ArrayList<DocumentDetailsModel> mImgList;
    DocumentDetailsAdapterListener mListener;

    public DocumentDetailsAdapter(Activity applicationContext, ArrayList<DocumentDetailsModel> imgList, DocumentDetailsAdapterListener listener) {
        this.mContext = applicationContext;
        this.mImgList = imgList;
        this.mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        RowViewDetailsDoumentsBinding layoutBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_view_details_douments, parent, false);


        return new MyViewHolder(layoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
      holder.layoutBinding.setViewModel(mImgList.get(position));

      holder.layoutBinding.tvDoumentName.setText(mImgList.get(position).getFileName());

        holder.layoutBinding.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener !=null){
                    mListener.onClicked(mImgList.get(position));
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return  mImgList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowViewDetailsDoumentsBinding layoutBinding;

        public MyViewHolder(RowViewDetailsDoumentsBinding itemView) {
            super(itemView.getRoot());
            this.layoutBinding = itemView;
        }

    }

    public interface DocumentDetailsAdapterListener {
       void onClicked(DocumentDetailsModel post/*, boolean isPartIsInProgress*/);

        void onClickedCrossIcon(boolean isClicked);
    }

}

