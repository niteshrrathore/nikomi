package com.workflow.nikomi.fcm;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.workflow.nikomi.MainActivity;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.FabDetailForInspectionActivity;
import com.workflow.nikomi.activity.PartDetailForInspectionActivity;
import com.workflow.nikomi.utils.AppSession;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.workflow.nikomi.utils.Constant.FABRICATION_ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.WORKSHOP_WORKER;
//{"id":2,"title":"Ready for inspection.","message":"Part no. - 8451 is now ready for inspection.","type":"PART","notification_unique_id":1}
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessagingService";
    AppSession appSession;
    int uniqueNotificationId = 0;

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        appSession = new AppSession(this);
        appSession.storeUserDeviceToken(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        appSession = new AppSession(this);
        Log.d(TAG, "From: " + remoteMessage.getData());
        if (appSession.getUserDetails() != null && appSession.isUserLogin()) {
            // Check if message contains a data payload.

            try {

                JSONObject jsonObject = new JSONObject(remoteMessage.getData());
                JSONObject jsonData = new JSONObject(jsonObject.get("data").toString());

                switch (jsonData.getInt("notification_unique_id")) {
                    case 1:
                        Intent intent;
                        if (jsonData.getString("type").equals("PART"))
                        intent = new Intent(this, PartDetailForInspectionActivity.class)
                                .putExtra(PART_ID, String.valueOf(jsonData.getInt("id")))
                                .putExtra(JOB_NAME, jsonData.getString("name"))
                                .putExtra(JOB_NUMBER, jsonData.getString("job_number"));
                        else
                        intent = new Intent(this, FabDetailForInspectionActivity.class)
                                .putExtra(FABRICATION_ID, String.valueOf(jsonData.getInt("id")))
                                .putExtra(JOB_NAME, jsonData.getString("name"))
                                .putExtra(JOB_NUMBER, jsonData.getString("job_number"));

                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueNotificationId , intent,
                                PendingIntent.FLAG_UPDATE_CURRENT);

                        if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER)){
                            generateNotification(jsonData.getString("title"), jsonData.getString("message"), pendingIntent);
                            appSession.setNotificationIndicator(true);
                            //if (!isForeground("com.workflow.nikomi.MainActivity"))
                            sendBroadcast("1", jsonData.getInt("notification_unique_id"));
                        }
                        break;
                    case 2:

                        Log.e(TAG, "onMessageReceived: >>>>>>>>>>>>>>>>>>>" );
                        PendingIntent pendingIntentForForceCompleteNotifcation = PendingIntent.getActivity(this, uniqueNotificationId , new Intent(),
                                PendingIntent.FLAG_UPDATE_CURRENT);

                        generateNotification(jsonData.getString("title"), jsonData.getString("message"), pendingIntentForForceCompleteNotifcation);

                        break;

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        /*}*/

        // Check if message contains a notification payload.
        /*if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }*/

    }

    // Send an Intent with an action named "custom-event-name". The Intent sent should
// be received by the ReceiverActivity.
    private void sendBroadcast(String updateTab, int uniqueId) {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent("notification_event");
        // You can also include some extra data.
        //intent.putExtra("unique_id", uniqueId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void generateNotification(String title, String messageBody, PendingIntent pendingIntent) {

        String channelId = getString(R.string.app_name);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Nikomi",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);
        }
        int NOTITICATION_ID; //= database.getNoteDao().fetchAllNotification().size()+1;

        notificationManager.notify(uniqueNotificationId, notificationBuilder.build());
    }

    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;

        if (componentInfo.getClassName().equals(myPackage)) {
            return true;
        } else {
            return false;
        }

        //return componentInfo.getClassName().equals(myPackage);
    }

}
