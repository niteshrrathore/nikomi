package com.workflow.nikomi.expandablerecyclerview;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.utils.Utility;

public class PardtFabTimesheetViewHolder extends ChildViewHolder {

    private TextView mMoviesTextView;
    private TextView mJobTimeTextView;
    private Context context;

    public PardtFabTimesheetViewHolder(View itemView, Context mContext) {
        super(itemView);
        mMoviesTextView = (TextView) itemView.findViewById(R.id.tvJobName);
        mJobTimeTextView = (TextView) itemView.findViewById(R.id.tvJobTime);
        context = mContext;
    }

    public void bind(PartFabTimesheet partFabTimesheet) {
        mMoviesTextView.setText(partFabTimesheet.getType()+": "+ partFabTimesheet.getId()+", "+ partFabTimesheet.getName());
        mJobTimeTextView.setText(partFabTimesheet.getTotalTimeSpent());
    }
}