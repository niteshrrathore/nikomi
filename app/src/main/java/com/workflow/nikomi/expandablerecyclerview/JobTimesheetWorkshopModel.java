package com.workflow.nikomi.expandablerecyclerview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobTimesheetWorkshopModel implements ParentListItem {


    @SerializedName("job_name")
    @Expose
    private String jobName;
    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("total_time_spent")
    @Expose
    private String totalTimeSpent;
    @SerializedName("part_fab_timesheet")
    @Expose
    private List<PartFabTimesheet> partFabTimesheet;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTotalTimeSpent() {
        return totalTimeSpent;
    }

    public void setTotalTimeSpent(String totalTimeSpent) {
        this.totalTimeSpent = totalTimeSpent;
    }

    public List<PartFabTimesheet> getPartFabTimesheet() {
        return partFabTimesheet;
    }

    public void setPartFabTimesheet(List<PartFabTimesheet> partFabTimesheet) {
        this.partFabTimesheet = partFabTimesheet;
    }




    @Override
    public List<?> getChildItemList() {
        return partFabTimesheet;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}