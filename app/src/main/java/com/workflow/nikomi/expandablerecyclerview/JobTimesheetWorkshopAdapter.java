package com.workflow.nikomi.expandablerecyclerview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.workflow.nikomi.R;

import java.util.List;

import static es.voghdev.pdfviewpager.library.subscaleview.SubsamplingScaleImageViewConstants.TAG;

public class JobTimesheetWorkshopAdapter extends ExpandableRecyclerAdapter<JobTimesheetWorkshopViewHolder, PardtFabTimesheetViewHolder> {

    private LayoutInflater mInflator;
    private Context mContext;
    List<? extends ParentListItem> listItems;

    public JobTimesheetWorkshopAdapter(Context context, List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        mContext = context;
        mInflator = LayoutInflater.from(context);
    }



    @Override
    public JobTimesheetWorkshopViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View movieCategoryView = mInflator.inflate(R.layout.row_job_timesheet_workshop_jobheading, parentViewGroup, false);
        return new JobTimesheetWorkshopViewHolder(movieCategoryView,mContext);
    }

    @Override
    public PardtFabTimesheetViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View moviesView = mInflator.inflate(R.layout.row_job_timesheet_workshop, childViewGroup, false);
        return new PardtFabTimesheetViewHolder(moviesView,mContext);
    }

    @Override
    public void onBindParentViewHolder(JobTimesheetWorkshopViewHolder jobTimesheetWorkshopViewHolder, int position, ParentListItem parentListItem) {
        JobTimesheetWorkshopModel jobTimesheetWorkshopModel = (JobTimesheetWorkshopModel) parentListItem;
        Log.e(TAG, "onBindParentViewHolder: "+ jobTimesheetWorkshopModel.getJobName() );
        jobTimesheetWorkshopViewHolder.bind(jobTimesheetWorkshopModel);
    }

    @Override
    public void onBindChildViewHolder(PardtFabTimesheetViewHolder pardtFabTimesheetViewHolder, int position, Object childListItem) {
        PartFabTimesheet partFabTimesheet = (PartFabTimesheet) childListItem;
        pardtFabTimesheetViewHolder.bind(partFabTimesheet);
    }


}