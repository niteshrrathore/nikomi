package com.workflow.nikomi.expandablerecyclerview;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.workflow.nikomi.R;
import com.workflow.nikomi.utils.Utility;

public class JobTimesheetWorkshopViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;
    private static final String TAG = "MovieCategoryViewHolder";

    private final ImageView mArrowExpandImageView;
    private TextView mJobNameTextView;
    private TextView mTotalTimeTextView;
    private Context mContext;
    private View viewLine;

    public JobTimesheetWorkshopViewHolder(View itemView, Context context) {
        super(itemView);
        mContext = context;
        mJobNameTextView = (TextView) itemView.findViewById(R.id.tvJobName);
        mTotalTimeTextView = (TextView) itemView.findViewById(R.id.tvTotalTime);

        mArrowExpandImageView = (ImageView) itemView.findViewById(R.id.iv_arrow_expand);
        viewLine = (View) itemView.findViewById(R.id.viewLine);
    }

    public void bind(JobTimesheetWorkshopModel jobTimesheetWorkshopModel) {
        Log.e(TAG, "bind: "+ jobTimesheetWorkshopModel.getJobName() );
        mJobNameTextView.setText(Utility.getJobTitle(mContext, jobTimesheetWorkshopModel.getJobName(), jobTimesheetWorkshopModel.getJobId()));
        mTotalTimeTextView.setText(mContext.getString(R.string.total_hours)+" "+ jobTimesheetWorkshopModel.getTotalTimeSpent());

        if (jobTimesheetWorkshopModel.getPartFabTimesheet().isEmpty() && jobTimesheetWorkshopModel.getPartFabTimesheet().size() ==0){
            mArrowExpandImageView.setVisibility(View.GONE);
        }else {
            mArrowExpandImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);

            if (expanded) {
                mArrowExpandImageView.setRotation(ROTATED_POSITION);
                viewLine.setVisibility(View.GONE);
            } else {
                mArrowExpandImageView.setRotation(INITIAL_POSITION);
                viewLine.setVisibility(View.VISIBLE);
            }

    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);

            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                 rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            mArrowExpandImageView.startAnimation(rotateAnimation);

    }
}

