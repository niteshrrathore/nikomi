package com.workflow.nikomi.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.android.material.snackbar.Snackbar;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.ViewDrawingActivity;
import com.workflow.nikomi.adapters.DocumentDetailsAdapter;
import com.workflow.nikomi.interfaces.CallBackCrossIcon;
import com.workflow.nikomi.model.DocumentDetailsModel;
import com.workflow.nikomi.utils.PermissionsChecker;

import java.util.ArrayList;

import static com.workflow.nikomi.fragment.ProfileFragment.PERMISSIONS;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.TYPE_ONSITE_DOC;

public class MyDialogFragment extends DialogFragment implements DocumentDetailsAdapter.DocumentDetailsAdapterListener {


    Activity activity;
    ArrayList<DocumentDetailsModel> documentDetailsModelArrayList;
    private static final String TAG = "MyDialogFragment";
    boolean lacksPermissions;
    PermissionsChecker checker;
    public MyDialogFragment(Activity activity, ArrayList<DocumentDetailsModel> documentDetailsModelArrayList) {
        this.activity = activity;
        this.documentDetailsModelArrayList = documentDetailsModelArrayList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checker = new PermissionsChecker(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_view_details, container, false);

        // Do all the stuff to initialize your custom view

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.rvGaDocument);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(activity, R.drawable.divider_line));
        recyclerView.addItemDecoration(itemDecorator);

        ImageView imgClose = (ImageView) v.findViewById(R.id.imgClose);
        DocumentDetailsAdapter documentDetailsAdapter = new DocumentDetailsAdapter(activity, documentDetailsModelArrayList, this);
        recyclerView.setAdapter(documentDetailsAdapter);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return v;
    }

    @Override
    public void onClicked(DocumentDetailsModel post) {


            String JOB_ID = "" + post.getId();
            startActivity(new Intent(getActivity(), ViewDrawingActivity.class)
                    .putExtra("from", TYPE_ONSITE_DOC).putExtra(ID, JOB_ID));
            dismiss();


    }


    @Override
    public void onClickedCrossIcon(boolean isClicked) {

    }


}