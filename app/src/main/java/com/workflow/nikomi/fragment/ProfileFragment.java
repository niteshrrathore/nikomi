package com.workflow.nikomi.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.soundcloud.android.crop.Crop;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.ChangePasswordActivity;
import com.workflow.nikomi.activity.ProfileActivity;
import com.workflow.nikomi.adapters.JobListAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.JobFragmentBinding;
import com.workflow.nikomi.databinding.ProfileFragmentBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.UserDetail;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.CompressFile;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.workflow.nikomi.utils.BaseActivity.typeface;
import static com.workflow.nikomi.utils.Constant.AVTAR;
import static com.workflow.nikomi.utils.Constant.CREATED_BY;
import static com.workflow.nikomi.utils.Constant.DELETE_STATUS;
import static com.workflow.nikomi.utils.Constant.EMAIL;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.IS_ACTIVE;
import static com.workflow.nikomi.utils.Constant.JOB_ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.LIMIT;
import static com.workflow.nikomi.utils.Constant.LIMIT_COUNT;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.MOBILE;
import static com.workflow.nikomi.utils.Constant.NAME;
import static com.workflow.nikomi.utils.Constant.OFFSET;
import static com.workflow.nikomi.utils.Constant.PROFILE_URL;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOTAL;
import static com.workflow.nikomi.utils.Constant.TYPE;
import static com.workflow.nikomi.utils.Constant.UPDATED_AT;

public class ProfileFragment extends Fragment implements ServicesListener, View.OnClickListener {

    private static final String TAG = "JobFragment";
    ProfileFragmentBinding binding;
    AppSession appSession;

    static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    PermissionsChecker checker;
    boolean lacksPermissions;
    public static final int TAKE_PIC_REQUEST_CODE = 4;
    public static final int CHOOSE_PIC_REQUEST_CODE = 3;

    SpannableString mSpannableUnderlinedText;
    ArrayList<File> imagesArray;
    boolean isButtonSaveClicked = false;

    private void setProfileDataFromIntent() {

        binding.txtInputFullName.setTypeface(typeface);
        binding.txtInputFullName.getEditText().setTypeface(typeface);
        binding.txtInputEmail.setTypeface(typeface);
        binding.txtInputEmail.getEditText().setTypeface(typeface);
        binding.txtInputMobile.setTypeface(typeface);
        binding.txtInputMobile.getEditText().setTypeface(typeface);
        binding.etFullName.setText(appSession.getUserDetails().getName());
        binding.etEmail.setText(appSession.getUserDetails().getEmail());
        binding.etMobile.setText(appSession.getUserDetails().getMobile());

        if(appSession.getUserDetails().getProfile_url().length()>0){
            mSpannableUnderlinedText = new SpannableString(getActivity().getString(R.string.change_pic));
            Utility.loadImageFromPicasso(getActivity(),appSession.getUserDetails().getProfile_url(), getActivity().getDrawable(R.drawable.profile_placeholder), binding.imgUserProfile);
        }else
            mSpannableUnderlinedText = new SpannableString(getActivity().getString(R.string.upload_pic));

        mSpannableUnderlinedText.setSpan(new UnderlineSpan(), 0, mSpannableUnderlinedText.length(), 0);
        binding.tvChangePic.setText(mSpannableUnderlinedText);
        binding.etFullName.setSelection(binding.etFullName.getText().length());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){

        binding = DataBindingUtil.inflate(inflater, R.layout.profile_fragment, parent, false);
        appSession = new AppSession(getActivity());
        checker = new PermissionsChecker(getActivity());
        binding.layoutHeader.imgBack.setVisibility(View.GONE);
        binding.layoutHeader.tvHeaderTitle.setText(getActivity().getString(R.string.profile));
        binding.btnSave.setOnClickListener(this);
        binding.tvChangePic.setOnClickListener(this);
        binding.tvChangePasswordLink.setOnClickListener(this);
        getProfileData();
        setWatcherOnEditText();

        return binding.getRoot();
    }

    private void getProfileData() {

        APIRequest.generalApiRequest(getActivity(), getActivity(),
                this, new HashMap<String, String>(), ServicesType.getProfile, true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSave:
                isButtonSaveClicked = true;
                if (checkValidData()){
                    if (imagesArray == null)
                        imagesArray = new ArrayList<>();

                    uploadData(imagesArray);
                }
                break;

            case R.id.tvChangePic:
                openDialogForUploadPhoto();
                break;

                case R.id.tvChangePasswordLink:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                break;
        }
    }

    private void openDialogForUploadPhoto(){

        lacksPermissions = checker.lacksPermissions(PERMISSIONS);

        if (!lacksPermissions) {

            //show dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(),
                    R.style.AlertDialogCustom));
            builder.setTitle(getActivity().getString(R.string.upload_or_take_photo));
            builder.setPositiveButton(getActivity().getString(R.string.upload), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(getActivity(), AlbumSelectActivity.class);
                    intent.setType("image/*");
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                    startActivityForResult(intent, CHOOSE_PIC_REQUEST_CODE);

                }
            });

            builder.setNegativeButton(getActivity().getString(R.string.take_photo), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, TAKE_PIC_REQUEST_CODE);

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        }else{

            View contextView = binding.content;
            Snackbar snackbar = Snackbar.make(contextView, getActivity().getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(getActivity().getColor(R.color.colorRed));
            TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
            tv.setTextSize(14f);
            snackbar.setAction(getActivity().getString(R.string.permission_settings), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getActivity().getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

            snackbar.show();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (resultCode == RESULT_OK) {

            if (requestCode == CHOOSE_PIC_REQUEST_CODE) {
                if (data == null) {
                    Toast.makeText(getActivity(), "Image cannot be null!", Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                    imagesArray = new ArrayList<>();
                    for (int i = 0; i < images.size(); i++) {
                        File file = new File((images.get(i)).path);
                        imagesArray.add(CompressFile.getCompressedImageFile(file, getActivity()));
                        Uri imageUri = Uri.fromFile(file);
                        beginCrop(imageUri);
                        Log.e(TAG, "onActivityResult: " + imageUri);
                        break;

                    }
                }

            } else if (requestCode == TAKE_PIC_REQUEST_CODE) {
                imagesArray = new ArrayList<>();
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        imagesArray.add(CompressFile.getCompressedImageFile(f, getActivity()));
                        Uri imageUri = Uri.fromFile(f);
                        beginCrop(imageUri);
                        Log.e(TAG, "onActivityResult: " + imageUri);
                        break;
                    }
                }

            }if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }

        }

    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(getActivity());
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            binding.imgUserProfile.setImageDrawable(null);
            binding.imgUserProfile.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadData(ArrayList<File> imagesArray) {

        HashMap<String, String> param = new HashMap<String, String>();
        param.put(NAME, binding.etFullName.getText().toString().trim());
        param.put(EMAIL, binding.etEmail.getText().toString().trim());
        param.put(MOBILE, binding.etMobile.getText().toString().trim());

        APIRequest.makeRequestToServerNewJsonArrayParameter(true, getActivity(), getActivity(),
                ServicesType.updateProfile, this, imagesArray, param);
    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {
            case updateProfile:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        JSONArray resultArray = jsonObject.getJSONArray(RESPONSE_DATA);

                        if (resultArray.length() > 0) {

                            UserDetail objUserDetail = new UserDetail();
                            objUserDetail.setId(resultArray.getJSONObject(0).getString(ID));
                            objUserDetail.setName(resultArray.getJSONObject(0).getString(NAME));
                            objUserDetail.setEmail(resultArray.getJSONObject(0).getString(EMAIL));
                            objUserDetail.setMobile(resultArray.getJSONObject(0).getString(MOBILE));
                            objUserDetail.setType(resultArray.getJSONObject(0).getString(TYPE));
                            objUserDetail.setAvatar(resultArray.getJSONObject(0).getString(AVTAR));
                            objUserDetail.setCreated_by(resultArray.getJSONObject(0).getInt(CREATED_BY));
                            objUserDetail.setDelete_status(resultArray.getJSONObject(0).getInt(DELETE_STATUS));
                            objUserDetail.setIs_active(resultArray.getJSONObject(0).getInt(IS_ACTIVE));
                            objUserDetail.setUpdated_at(resultArray.getJSONObject(0).getString(UPDATED_AT));
                            //objUserDetail.setProfile_url("https://sugardate.me/upload/gallery/159291456847506.jpg");
                            objUserDetail.setProfile_url(resultArray.getJSONObject(0).getString(PROFILE_URL));

                            appSession.storeUser(objUserDetail);
                            setProfileDataFromIntent();
                        }

                        if (showMessage)
                            Utility.toaster(getActivity(), KEY_SUCCESS, message);

                    } else {
                        if (showMessage)
                            Utility.toaster(getActivity(), KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case getProfile:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        JSONArray resultArray = jsonObject.getJSONArray(RESPONSE_DATA);

                        if (resultArray.length() > 0) {

                            UserDetail objUserDetail = new UserDetail();
                            objUserDetail.setId(resultArray.getJSONObject(0).getString(ID));
                            objUserDetail.setName(resultArray.getJSONObject(0).getString(NAME));
                            objUserDetail.setEmail(resultArray.getJSONObject(0).getString(EMAIL));
                            objUserDetail.setMobile(resultArray.getJSONObject(0).getString(MOBILE));
                            objUserDetail.setType(resultArray.getJSONObject(0).getString(TYPE));
                            objUserDetail.setAvatar(resultArray.getJSONObject(0).getString(AVTAR));
                            objUserDetail.setCreated_by(resultArray.getJSONObject(0).getInt(CREATED_BY));
                            objUserDetail.setDelete_status(resultArray.getJSONObject(0).getInt(DELETE_STATUS));
                            objUserDetail.setIs_active(resultArray.getJSONObject(0).getInt(IS_ACTIVE));
                            objUserDetail.setUpdated_at(resultArray.getJSONObject(0).getString(UPDATED_AT));
                            //objUserDetail.setProfile_url("https://sugardate.me/upload/gallery/159291456847506.jpg");
                            objUserDetail.setProfile_url(resultArray.getJSONObject(0).getString(PROFILE_URL));

                            appSession.storeUser(objUserDetail);

                            setProfileDataFromIntent();

                        }

                    } else {
                        setProfileDataFromIntent();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    setProfileDataFromIntent();
                }
                break;

        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(getActivity(), KEY_ERROR, message);
    }

    private boolean checkValidData() {

        if (binding.etFullName.getText().toString().isEmpty()) {
            binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_fullname));
            return false;
        } else if (!Utility.isValidUserName(binding.etFullName.getText().toString().trim())) {
            binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_valid_fullname));
            return false;
        } if (binding.etEmail.getText().toString().isEmpty()) {
            binding.txtInputEmail.setError(getResources().getString(R.string.please_enter_email_address));
            return false;
        } else if (!Utility.isValidEmail(binding.etEmail.getText().toString().trim())) {
            binding.txtInputEmail.setError(getResources().getString(R.string.please_a_enter_valid_email_address));
            return false;
        } if (binding.etMobile.getText().toString().isEmpty()) {
            binding.txtInputMobile.setError(getResources().getString(R.string.please_enter_mobile_number));
            return false;
        } else if (!Utility.isValidMobile(binding.etMobile.getText().toString().trim())) {
            binding.txtInputMobile.setError(getResources().getString(R.string.please_a_enter_valid_mobile_number));
            return false;
        } else if (!Utility.isConnectingToInternet(getActivity())) {
            Utility.toaster(getActivity(), KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
            return false;
        }else {
            binding.txtInputFullName.setError(null);
            binding.txtInputEmail.setError(null);
            binding.txtInputMobile.setError(null);
            return true;
        }
    }

    private void setWatcherOnEditText(){

        binding.etFullName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isButtonSaveClicked) {
                    if (binding.etFullName.getText().toString().isEmpty()) {
                        binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_fullname));
                    } else if (!Utility.isValidUserName(binding.etFullName.getText().toString().trim())) {
                        binding.txtInputFullName.setError(getResources().getString(R.string.please_enter_valid_fullname));
                    } else {
                        binding.txtInputFullName.setError(null);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

        binding.etEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isButtonSaveClicked) {
                    if (binding.etEmail.getText().toString().isEmpty()) {
                        binding.txtInputEmail.setError(getResources().getString(R.string.please_enter_email_address));
                    } else if (!Utility.isValidEmail(binding.etEmail.getText().toString().trim())) {
                        binding.txtInputEmail.setError(getResources().getString(R.string.please_a_enter_valid_email_address));
                    } else
                        binding.txtInputEmail.setError(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }

        });

        binding.etMobile.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (isButtonSaveClicked) {
                    if (binding.etMobile.getText().toString().isEmpty()) {
                        binding.txtInputMobile.setError(getResources().getString(R.string.please_enter_mobile_number));
                    } else if (!Utility.isValidMobile(binding.etMobile.getText().toString().trim())) {
                        binding.txtInputMobile.setError(getResources().getString(R.string.please_a_enter_valid_mobile_number));
                    } else
                        binding.txtInputMobile.setError(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                //Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });
    }

}