package com.workflow.nikomi.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.R;
import com.workflow.nikomi.onsite.OnsiteJobDetailActivity;
import com.workflow.nikomi.adapters.JobFilterSpinnerAdapter;
import com.workflow.nikomi.adapters.JobListAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.JobFragmentBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.JobPartStatusModel;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.Constant.ACTION_JOB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_OPEN;
import static com.workflow.nikomi.utils.Constant.FILTER;
import static com.workflow.nikomi.utils.Constant.JOB_ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_FROM;
import static com.workflow.nikomi.utils.Constant.LIMIT;
import static com.workflow.nikomi.utils.Constant.LIMIT_COUNT;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.OFFSET;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SEARCH;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOTAL;
import static com.workflow.nikomi.utils.Constant.TYPE_JOB;
import static com.workflow.nikomi.utils.Constant.WORKSHOP_WORKER;

public class JobFragment extends Fragment implements JobListAdapter.JobAdapterListener, ServicesListener {

    private static final String TAG = "JobFragment";
    int totalRecords = 0;
    int countLoadMore = 0;
    int spinnerFilterPosition = 0;
    String strSearch;
    AppSession appSession;
    JobListAdapter adapter;
    ArrayList<JobModel> mJobList = new ArrayList<>();
    ArrayList<JobPartStatusModel> mListJobStatus = new ArrayList<>();
    JobFragmentBinding binding;
    Animation slideinright, slideinleft;
    boolean isCalled = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.job_fragment, parent, false);
        appSession = new AppSession(getActivity());
        AnimationInitialization();

        mJobList.clear();
        mListJobStatus.clear();

        binding.layoutHeader.imgBack.setVisibility(View.GONE);
        binding.layoutHeader.tvHeaderTitle.setText(getResources().getString(R.string.jobs_));

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_line));
        binding.recyclerView.addItemDecoration(itemDecorator);
        adapter = new JobListAdapter(getActivity(), mJobList, this, TAG);
        binding.recyclerView.setAdapter(adapter);

        /*Endless scrolling*/
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int currentSize = mJobList.size();

                if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == currentSize - 1) {

                    if (currentSize < totalRecords) {

                        countLoadMore++;
                        getJobsFromServer();
                    }

                }

            }

        });

        binding.layoutHeader.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.layoutHeader.lySearch.getVisibility() == View.VISIBLE) {
                    performBackPressForSearch();
                }
            }
        });

        binding.layoutHeader.imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.layoutHeader.lySearch.getVisibility() == View.VISIBLE) {
                    performBackPressForSearch();
                } else {
                    binding.layoutHeader.lySearch.startAnimation(slideinright);
                    binding.layoutHeader.lySearch.setVisibility(View.VISIBLE);
                    binding.layoutHeader.imgBack.setVisibility(View.VISIBLE);
                    binding.layoutHeader.tvHeaderTitle.setVisibility(View.GONE);
                    binding.layoutHeader.edtSearchUser.requestFocus();
                    Utility.showSoftKeyboard(getActivity(), binding.layoutHeader.edtSearchUser);
                }
            }
        });

        binding.layoutHeader.edtSearchUser.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (cs.length() > 0) {
                    binding.layoutHeader.imgClearSearch.setVisibility(View.VISIBLE);
                    binding.layoutHeader.imgSearch.setVisibility(View.GONE);
                } else {
                    binding.layoutHeader.imgClearSearch.setVisibility(View.GONE);
                    binding.layoutHeader.imgSearch.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        binding.layoutHeader.edtSearchUser.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (binding.layoutHeader.edtSearchUser.getText().length() > 0) {
                        strSearch = binding.layoutHeader.edtSearchUser.getText().toString().trim();
                        getDataFromBegin();
                    } else {
                        performBackPressForSearch();
                    }

                    return true;
                }
                return false;
            }
        });

        binding.layoutHeader.imgClearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.layoutHeader.edtSearchUser.setText("");
                strSearch = null;
                getDataFromBegin();
            }
        });

        return binding.getRoot();
    }

    private void prepareFilterSpinner() {

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<JobPartStatusModel>>() {
        }.getType();

        ArrayList<JobPartStatusModel> arrPackageData = gson.fromJson(appSession.getJobPartStatus(), type);

        JobPartStatusModel jobPartStatusModel = new JobPartStatusModel();
        jobPartStatusModel.setName(getActivity().getString(R.string.all));
        mListJobStatus.add(jobPartStatusModel);

        for (int i = 0; i < arrPackageData.size(); i++) {

            if (arrPackageData.get(i).getType().equals(TYPE_JOB)) {
                if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER)) {
                    if (arrPackageData.get(i).getId() == ACTION_JOB_OPEN || arrPackageData.get(i).getId() == ACTION_JOB_IN_PROGRESS)
                        mListJobStatus.add(arrPackageData.get(i));
                } else {
                    if (arrPackageData.get(i).getId() == ACTION_JOB_OPEN ||
                            arrPackageData.get(i).getId() == ACTION_JOB_IN_PROGRESS || arrPackageData.get(i).getId() == ACTION_JOB_NOT_FULFILLED)
                        mListJobStatus.add(arrPackageData.get(i));
                }
            }

        }

        if (!mListJobStatus.isEmpty()) {
            JobFilterSpinnerAdapter mAdapterFilter = new JobFilterSpinnerAdapter(getActivity(), R.layout.spinner_text, mListJobStatus);

            mAdapterFilter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
            binding.spinnerFilter.setAdapter(mAdapterFilter);

            binding.spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        spinnerFilterPosition = 0;
                        getDataFromBegin();
                    } else {
                        JobPartStatusModel jobSelectedStatus = (JobPartStatusModel) parent.getItemAtPosition(position);
                        spinnerFilterPosition = jobSelectedStatus.getId();
                        getDataFromBegin();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.e(TAG, "onNothingSelected: spinnerFilter");
                }
            });
        } else {
            binding.rlSpinnerFilter.setVisibility(View.GONE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        prepareFilterSpinner();
    }

    private void getJobsFromServer() {
        if (!Utility.isConnectingToInternet(getActivity())) {

            Utility.toaster(getActivity(), KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();
            JsonObject jsonObject = new JsonObject();
            if (strSearch != null)
                params.put(SEARCH, strSearch);
            //params.put(SORT, );//asc or desc
            params.put(LIMIT, String.valueOf(LIMIT_COUNT));
            params.put(OFFSET, String.valueOf(countLoadMore));
            params.put("all_jobs", "1");
            jsonObject.addProperty("is_publish", "1");


            if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER)) {
                if (spinnerFilterPosition != 0) {
                    jsonObject.addProperty("workshop_status_id", String.valueOf(spinnerFilterPosition));
                }
                params.put(FILTER, jsonObject.toString());//id, name or job_number
                APIRequest.generalApiRequest(getContext(), getActivity(),
                        JobFragment.this, params, ServicesType.getAllJobs, true);
            } else {
                if (spinnerFilterPosition != 0) {
                    jsonObject.addProperty("onsite_status_id", String.valueOf(spinnerFilterPosition));
                }
                params.put(FILTER, jsonObject.toString());//id, name or job_number
                APIRequest.generalApiRequest(getContext(), getActivity(),
                        JobFragment.this, params, ServicesType.getAllJobsForOnsite, true);
            }

        }
    }

    @Override
    public void onClicked(JobModel post/*, boolean isPartIsInProgress*/) {

        if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER)) {

            Bundle args = new Bundle();
            args.putInt(JOB_ID, post.getId());
            args.putString(JOB_NAME, post.getName());
            args.putString(JOB_NUMBER, post.getJobNumber());
            args.putString(KEY_FROM, TAG);
            Fragment mFragment = new JobDetailFragment();
            mFragment.setArguments(args);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frameLayout, mFragment).addToBackStack(TAG).commit();
        } else
            startActivity(new Intent(getActivity(), OnsiteJobDetailActivity.class)
                    .putExtra(JOB_ID, String.valueOf(post.getId())).putExtra(JOB_NAME, post.getName()).putExtra(JOB_NUMBER, post.getJobNumber()).putExtra(KEY_FROM, TAG));

    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {
            case getAllJobsForOnsite:
            case getAllJobs:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        totalRecords = jsonObject.getJSONObject(RESPONSE_DATA).getInt(TOTAL);

                        if (totalRecords > 0) {
                            Gson gson = new Gson();
                            Type typeGson = new TypeToken<List<JobModel>>() {
                            }.getType();

                            ArrayList<JobModel> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(LIST), typeGson);
                            this.mJobList.addAll(mJobList);
                            adapter.notifyDataSetChanged();

                            if (binding.recyclerView.getVisibility() == View.GONE){
                                binding.recyclerView.setVisibility(View.VISIBLE);
                                binding.layoutHeader.imgSearch.setVisibility(View.VISIBLE);
                            }

                            if (binding.tvNoRecordFound.getVisibility() == View.VISIBLE)
                                binding.tvNoRecordFound.setVisibility(View.GONE);

                        } else {
                            if (binding.recyclerView.getVisibility() == View.VISIBLE)
                                binding.recyclerView.setVisibility(View.GONE);
                            if (binding.tvNoRecordFound.getVisibility() == View.GONE){
                                binding.layoutHeader.imgSearch.setVisibility(View.GONE);
                                binding.tvNoRecordFound.setVisibility(View.VISIBLE);
                            }

                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(getActivity(), KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(getActivity(), KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;
        }

    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(getActivity(), KEY_ERROR, message);
    }

    private void performBackPressForSearch() {
        Utility.hideSoftKeyboard(getActivity());
        binding.layoutHeader.tvHeaderTitle.setVisibility(View.VISIBLE);
        binding.layoutHeader.lySearch.startAnimation(slideinleft);
        binding.layoutHeader.lySearch.setVisibility(View.GONE);
        binding.layoutHeader.imgBack.setVisibility(View.GONE);
        binding.layoutHeader.edtSearchUser.setText("");
        strSearch = null;
        getDataFromBegin();
    }

    private void AnimationInitialization() {
        slideinleft = AnimationUtils
                .loadAnimation(getContext(), R.anim.left_swipe_anim);

        slideinright = AnimationUtils.loadAnimation(getContext(),
                R.anim.right_swipe_search_anim);
    }

    private void getDataFromBegin() {
        countLoadMore = 0;
        mJobList.clear();
        getJobsFromServer();
    }
}
