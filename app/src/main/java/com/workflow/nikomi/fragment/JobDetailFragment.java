package com.workflow.nikomi.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.PartDetailActivity;
import com.workflow.nikomi.activity.PartDetailForInspectionActivity;
import com.workflow.nikomi.adapters.JobDetailListAdapter;
import com.workflow.nikomi.adapters.JobFilterSpinnerAdapter;
import com.workflow.nikomi.adapters.JobListAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.JobDetailFragmentBinding;
import com.workflow.nikomi.databinding.JobFragmentBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.CallBackCrossIcon;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.interfaces.UpdatePagerFragment;
import com.workflow.nikomi.model.DocumentDetailsModel;
import com.workflow.nikomi.model.JobDetailModel;
import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.JobPartStatusModel;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.GridSpacingItemDecoration;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_OPEN;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_ONSITE;
import static com.workflow.nikomi.utils.Constant.FILE_TYPE;
import static com.workflow.nikomi.utils.Constant.FILTER;
import static com.workflow.nikomi.utils.Constant.GENERAL_ASSEMBLY_TYPE;
import static com.workflow.nikomi.utils.Constant.JOB_ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_FROM;
import static com.workflow.nikomi.utils.Constant.LIMIT;
import static com.workflow.nikomi.utils.Constant.LIMIT_COUNT;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.OFFSET;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SEARCH;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOTAL;
import static com.workflow.nikomi.utils.Constant.TYPE_JOB;
import static com.workflow.nikomi.utils.Constant.TYPE_PART_FAB;
import static com.workflow.nikomi.utils.Constant.WORKSHOP_WORKER;

public class JobDetailFragment extends Fragment implements ServicesListener {

    private static final String TAG = "JobDetailFragment";
    AppSession appSession;
    public String jobName, jobNumber;
    public int jobId;
    public int selectedTabPos = 0, selectedStatusFilter = 0;
    String mKeyFrom, strSearch;
    JobDetailFragmentBinding binding;
    ViewPagerAdapter adapter;
    Animation slideinright, slideinleft;
    ArrayList<JobPartStatusModel> mListJobStatus = new ArrayList<>();
    AllPartandFabFragment mAllPartandFabFragment;
    FabricationFragment mFabricationFragment;
    PartFragment mPartFragment;
    ArrayList<DocumentDetailsModel> documentDetailsModelArrayList = new ArrayList<>();
    DialogFragment dialogFragment;
    CallBackCrossIcon callBackCrossIcon;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;
    boolean somePermissionsForeverDenied = false;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.job_detail_fragment, parent, false);
        appSession = new AppSession(getActivity());
        checker = new PermissionsChecker(getActivity());
        AnimationInitialization();
        Bundle b = getArguments();
        jobId = b.getInt(JOB_ID);
        jobName = b.getString(JOB_NAME);
        jobNumber = b.getString(JOB_NUMBER);
        mKeyFrom = b.getString(KEY_FROM);

        adapter = new ViewPagerAdapter(getChildFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        binding.viewpager.setOffscreenPageLimit(1);
        adapter.addFragment(AllPartandFabFragment.newInstance(jobId, jobName, jobNumber, mKeyFrom), getResources().getString(R.string.all));
        adapter.addFragment(PartFragment.newInstance(jobId, jobName, jobNumber, mKeyFrom), getResources().getString(R.string.tab_part));
        adapter.addFragment(FabricationFragment.newInstance(jobId, jobName, jobNumber, mKeyFrom), getResources().getString(R.string.tab_fabrication));
        binding.viewpager.setAdapter(adapter);
        binding.tabs.setupWithViewPager(binding.viewpager);
        highLightCurrentTab(selectedTabPos);
        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedTabPos = position;
                highLightCurrentTab(selectedTabPos);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        binding.layoutHeader.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.layoutHeader.lySearch.getVisibility() == View.VISIBLE) {
                    performBackPressForSearch();
                } else {
                    getFragmentManager().popBackStack();
                }
            }
        });

        binding.layoutHeader.imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.layoutHeader.lySearch.getVisibility() == View.VISIBLE) {
                    performBackPressForSearch();
                } else {
                    binding.layoutHeader.lySearch.startAnimation(slideinright);
                    binding.layoutHeader.lySearch.setVisibility(View.VISIBLE);
                    binding.layoutHeader.tvHeaderTitle.setVisibility(View.GONE);
                    binding.layoutHeader.edtSearchUser.requestFocus();
                    Utility.showSoftKeyboard(getActivity(), binding.layoutHeader.edtSearchUser);
                }
            }
        });

        binding.layoutHeader.edtSearchUser.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (cs.length() > 0) {
                    binding.layoutHeader.imgClearSearch.setVisibility(View.VISIBLE);
                    binding.layoutHeader.imgSearch.setVisibility(View.GONE);
                } else {
                    binding.layoutHeader.imgClearSearch.setVisibility(View.GONE);
                    binding.layoutHeader.imgSearch.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        binding.layoutHeader.edtSearchUser.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (binding.layoutHeader.edtSearchUser.getText().length() > 0) {
                        strSearch = binding.layoutHeader.edtSearchUser.getText().toString().trim();
                        updatePagerFrag();
                    } else {
                        performBackPressForSearch();
                    }

                    return true;
                }

                return false;
            }
        });

        binding.layoutHeader.imgClearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.layoutHeader.edtSearchUser.setText("");
                strSearch = null;
                updatePagerFrag();
            }
        });

        binding.layoutHeader.tvHeaderTitle.setText(getResources().getString(R.string.job_details));

        if (mKeyFrom.equals("JobFragment")) {
            binding.rlViewDetails.setVisibility(View.VISIBLE);
            binding.rlSpinnerFilter.setVisibility(View.VISIBLE);
            prepareFilterSpinner();
        } else {
            binding.rlViewDetails.setVisibility(View.GONE);
            binding.rlSpinnerFilter.setVisibility(View.GONE);
        }

        binding.rlViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Utility.openGaDocumentDialog(getActivity(),documentDetailsModelArrayList);

                if (checkPermission())
                    getGADocumentFromServer("" + jobId);


              /*  MyDialogFragment dialog = MyDialogFragment.newInstance();
                dialog.show(getActivity(), "MyDialogFragment");*/
            }
        });

        return binding.getRoot();
    }

    private boolean checkPermission() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {
            return true;
        } else {
            if (somePermissionsForeverDenied) {
                View contextView = binding.layoutMain;

                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getActivity().getColor(R.color.colorRed));

                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);

                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getActivity().getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
                return false;
            } else {
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                return false;
            }

        }
    }

    private void getGADocumentFromServer(String jobId) {
        if (!Utility.isConnectingToInternet(getActivity())) {

            Utility.toaster(getActivity(), KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();
            params.put(JOB_ID, jobId);
            params.put(FILE_TYPE, GENERAL_ASSEMBLY_TYPE);

            APIRequest.generalApiRequest(getContext(), getActivity(),
                    JobDetailFragment.this, params, ServicesType.getGADocument, true);


        }

    }

    private void callViewGADocumentDialogFragment(ArrayList<DocumentDetailsModel> documentDetailsModelArrayList) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        dialogFragment = new MyDialogFragment(getActivity(), documentDetailsModelArrayList);
        dialogFragment.show(ft, "dialog");

    }


    private void performBackPressForSearch() {
        Utility.hideSoftKeyboard(getActivity());
        binding.layoutHeader.tvHeaderTitle.setVisibility(View.VISIBLE);
        binding.layoutHeader.lySearch.startAnimation(slideinleft);
        binding.layoutHeader.lySearch.setVisibility(View.GONE);
        binding.layoutHeader.edtSearchUser.setText("");
        strSearch = null;
        updatePagerFrag();
    }

    private void AnimationInitialization() {
        slideinleft = AnimationUtils
                .loadAnimation(getContext(), R.anim.left_swipe_anim);

        slideinright = AnimationUtils.loadAnimation(getContext(),
                R.anim.right_swipe_search_anim);
    }

    private void prepareFilterSpinner() {

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<JobPartStatusModel>>() {
        }.getType();

        ArrayList<JobPartStatusModel> arrPackageData = gson.fromJson(appSession.getJobPartStatus(), type);

        JobPartStatusModel jobPartStatusModel = new JobPartStatusModel();
        jobPartStatusModel.setName(getActivity().getString(R.string.all));
        mListJobStatus.add(jobPartStatusModel);

        for (int i = 0; i < arrPackageData.size(); i++) {
            if (arrPackageData.get(i).getType().equals(TYPE_PART_FAB)) {
                if (arrPackageData.get(i).getId() == ACTION_PART_FAB_OPEN
                        || arrPackageData.get(i).getId() == ACTION_PART_FAB_IN_PROGRESS
                        || arrPackageData.get(i).getId() == ACTION_PART_FAB_NOT_FULFILLED)
                    mListJobStatus.add(arrPackageData.get(i));
            }
        }

        if (!mListJobStatus.isEmpty()) {
            JobFilterSpinnerAdapter mAdapterFilter = new JobFilterSpinnerAdapter(getActivity(), R.layout.spinner_text, mListJobStatus);

            mAdapterFilter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
            binding.spinnerFilter.setAdapter(mAdapterFilter);

            binding.spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        selectedStatusFilter = 0;
                    } else {
                        JobPartStatusModel jobSelectedStatus = (JobPartStatusModel) parent.getItemAtPosition(position);
                        selectedStatusFilter = jobSelectedStatus.getId();
                    }
                    updatePagerFrag();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.e(TAG, "onNothingSelected: spinnerFilter");
                }

            });
        } else {
            binding.rlSpinnerFilter.setVisibility(View.GONE);
        }

    }

    private void highLightCurrentTab(int position) {

        for (int i = 0; i < binding.tabs.getTabCount(); i++) {
            TabLayout.Tab tab = binding.tabs.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }

        TabLayout.Tab tab = binding.tabs.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
    }


    @Override
    public void success(String response, ServicesType type) {
        switch (type) {

            case getGADocument:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<List<DocumentDetailsModel>>() {
                        }.getType();

                        if (!documentDetailsModelArrayList.isEmpty()) {
                            documentDetailsModelArrayList.clear();
                        }

                        ArrayList<DocumentDetailsModel> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(LIST), typeGson);
                        documentDetailsModelArrayList.addAll(mJobList);

                        if (documentDetailsModelArrayList != null) {
                            if (!documentDetailsModelArrayList.isEmpty()) {
                                callViewGADocumentDialogFragment(documentDetailsModelArrayList);
                            } else {
                                Utility.toaster(getActivity(), KEY_ERROR, getString(R.string.no_record_found));
                            }
                        } else {
                            Utility.toaster(getActivity(), KEY_ERROR, getString(R.string.no_record_found));
                        }


                    } else {
                        if (showMessage)
                            Utility.toaster(getActivity(), KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(getActivity(), KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;
        }

    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(getActivity(), KEY_ERROR, message);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> mFragmentList = new ArrayList<>();
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return mFragmentTitleList.get(position);
            return null;
        }

        public View getTabView(int position) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_with_count, null);
            TextView tabTextView = view.findViewById(R.id.tvtab1);
            TextView tvCount = view.findViewById(R.id.tvCount);
            tabTextView.setText(mFragmentTitleList.get(position));
            tvCount.setVisibility(View.GONE);

            return view;
        }

        public View getSelectedTabView(int position) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_with_count, null);
            TextView tabTextView = view.findViewById(R.id.tvtab1);
            TextView tvCount = view.findViewById(R.id.tvCount);
            tabTextView.setText(mFragmentTitleList.get(position));
            tabTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
            tvCount.setVisibility(View.GONE);

            return view;
        }

    }

    private void updatePagerFrag() {

        ViewPagerAdapter pagerAdapter = (ViewPagerAdapter) binding.viewpager.getAdapter();
        for (int i = 0; i < pagerAdapter.getCount(); i++) {

            Fragment viewPagerFragment = (Fragment) binding.viewpager
                    .getAdapter().instantiateItem(binding.viewpager, i);
            if (viewPagerFragment != null) {

                if (viewPagerFragment instanceof AllPartandFabFragment) {
                    mAllPartandFabFragment = (AllPartandFabFragment) viewPagerFragment;
                    if (viewPagerFragment.isAdded())
                        mAllPartandFabFragment.beginSearch(strSearch, true, selectedStatusFilter);
                    else
                        mAllPartandFabFragment.beginSearch(strSearch, false, selectedStatusFilter);
                } else if (viewPagerFragment instanceof PartFragment) {
                    mPartFragment = (PartFragment) viewPagerFragment;
                    if (viewPagerFragment.isAdded())
                        mPartFragment.beginSearch(strSearch, true, selectedStatusFilter);
                    else
                        mPartFragment.beginSearch(strSearch, false, selectedStatusFilter);
                } else if (viewPagerFragment instanceof FabricationFragment) {
                    mFabricationFragment = (FabricationFragment) viewPagerFragment;
                    if (viewPagerFragment.isAdded())
                        mFabricationFragment.beginSearch(strSearch, true, selectedStatusFilter);
                    else
                        mFabricationFragment.beginSearch(strSearch, false, selectedStatusFilter);
                }

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE_PERMISSIONS:
                if (!somePermissionsForeverDenied) {
                    if (checker.hasAllPermissionsGranted(grantResults)) {
                        somePermissionsForeverDenied = false;
                        /*permission granted*/
                        //openDialogForUploadPhoto();
                     getGADocumentFromServer(""+jobId);
                    } else {
                        for (String permission : permissions) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                                //denied
                                Log.e("denied", permission);
                            } else {
                                if (ActivityCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {
                                    //allowed
                                    Log.e("allowed", permission);
                                } else {
                                    //set to never ask again
                                    Log.e("set to never ask again", permission);
                                    somePermissionsForeverDenied = true;

                                    break;
                                }
                            }
                        }

                    }
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }

    }
}
