package com.workflow.nikomi.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.PartDetailActivity;
import com.workflow.nikomi.activity.PartDetailForInspectionActivity;
import com.workflow.nikomi.adapters.JobDetailListAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.FragmentPartBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.interfaces.UpdatePagerFragment;
import com.workflow.nikomi.model.JobDetailModel;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.GridSpacingItemDecoration;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_OPEN;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_ONSITE;
import static com.workflow.nikomi.utils.Constant.FILTER;
import static com.workflow.nikomi.utils.Constant.INSPECTION_HISTORY;
import static com.workflow.nikomi.utils.Constant.JOB_HISTORY;
import static com.workflow.nikomi.utils.Constant.JOB_ID;
import static com.workflow.nikomi.utils.Constant.JOB_NAME;
import static com.workflow.nikomi.utils.Constant.JOB_NUMBER;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_FROM;
import static com.workflow.nikomi.utils.Constant.LIMIT;
import static com.workflow.nikomi.utils.Constant.LIMIT_COUNT;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.OFFSET;
import static com.workflow.nikomi.utils.Constant.PART_ID;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SEARCH;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;
import static com.workflow.nikomi.utils.Constant.TOTAL;
import static com.workflow.nikomi.utils.Constant.TYPE;
import static com.workflow.nikomi.utils.Constant.TYPE_ALL;
import static com.workflow.nikomi.utils.Constant.TYPE_ONLY_PART;

public class PartFragment extends Fragment implements JobDetailListAdapter.JobDetailAdapterListener,
        ServicesListener {

    private static final String TAG = "PartFragment";
    AppSession appSession;
    FragmentPartBinding binding;
    public String jobName, jobNumber, mKeyFrom, mStringSearch;
    public int jobId, partFabStatusId=0;
    int totalRecords;
    int countLoadMore = 0;
    JobDetailListAdapter adapter;
    ArrayList<JobDetailModel> mJobList = new ArrayList<>();

    public static PartFragment newInstance(int jobId, String jobName, String jobNumber, String mKeyFrom) {

        Bundle args = new Bundle();
        args.putString("job_name", jobName);
        args.putString("job_number", jobNumber);
        args.putInt("job_id", jobId);
        args.putString(KEY_FROM, mKeyFrom);
        PartFragment fragment = new PartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSession = new AppSession(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_part, container, false);
        binding.setFragment(this);

        if (getArguments() != null) {
            jobId = getArguments().getInt("job_id");
            jobName = getArguments().getString("job_name");
            jobNumber = getArguments().getString("job_number");
            mKeyFrom = getArguments().getString(KEY_FROM);
        }

        binding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        int spacing = 25; // 50px
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecoration(spacing));
        mJobList.clear();
        adapter = new JobDetailListAdapter(getActivity(), mJobList, this, mKeyFrom);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                GridLayoutManager GridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

                int currentSize = mJobList.size();

                if (GridLayoutManager != null && GridLayoutManager.findLastCompletelyVisibleItemPosition() == currentSize - 1) {

                    if (currentSize < totalRecords) {

                        countLoadMore++;
                        getAllPartsFromServer();
                    }

                }

            }

        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        countLoadMore = 0;
        mJobList.clear();
        getAllPartsFromServer();
    }

    private void getAllPartsFromServer() {
        if (!Utility.isConnectingToInternet(getActivity())) {

            Utility.toaster(getActivity(), KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {

            HashMap<String, String> params = new HashMap<>();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(JOB_ID, jobId);
                if (partFabStatusId!=0)
                    jsonObject.put(STATUS_ID, partFabStatusId);
                else {
                    if (mKeyFrom.equals("InspectionReadyFragment")){
                        jsonObject.put(STATUS_ID, ACTION_PART_FAB_READY_FOR_INSPECT);
                    }else if (mKeyFrom.equals("JobFragment")){
                        JSONArray jsonArray = new JSONArray();
                        jsonArray.put(ACTION_PART_FAB_OPEN);
                        jsonArray.put(ACTION_PART_FAB_IN_PROGRESS);
                        jsonArray.put(ACTION_PART_FAB_NOT_FULFILLED);
                        jsonObject.put(STATUS_ID, jsonArray);
                    }else if(mKeyFrom.equals("JobHistoryFragment")){
                        params.put(JOB_HISTORY, "1");
                        jsonObject.put(STATUS_ID, ACTION_PART_FAB_READY_FOR_ONSITE);
                    }else if(mKeyFrom.equals("InspectionHistoryFragment")){
                        params.put(INSPECTION_HISTORY, "1");
                        jsonObject.put(STATUS_ID, ACTION_PART_FAB_READY_FOR_ONSITE);
                    }
                }
                if (mStringSearch != null)
                    params.put(SEARCH, mStringSearch);
                params.put(TYPE, TYPE_ONLY_PART);
                params.put(FILTER, jsonObject.toString());
                params.put(LIMIT, String.valueOf(LIMIT_COUNT));
                params.put(OFFSET, String.valueOf(countLoadMore));
                //params.put(SORT_BY, );//id, name or job_number
                //params.put(SORT, );//asc or desc

                APIRequest.generalApiRequest(getContext(), getActivity(),
                        this, params, ServicesType.getAllParts, true);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClicked(JobDetailModel post) {
        if (!Utility.isConnectingToInternet(getActivity())) {
            Utility.toaster(getActivity(), KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
        } else {

            if (post.getStatusId() == ACTION_PART_FAB_READY_FOR_INSPECT) {

                if (/*post.getWorkedOn()==1*/post.getInspectReadyByUserId() == Integer.parseInt(appSession.getUserDetails().getId()))
                    startActivity(new Intent(getActivity(), PartDetailActivity.class)
                            .putExtra(PART_ID, String.valueOf(post.getId()))
                            .putExtra(JOB_NAME, jobName)
                            .putExtra(JOB_NUMBER, jobNumber));
                else
                    startActivity(new Intent(getActivity(), PartDetailForInspectionActivity.class)
                            .putExtra(PART_ID, String.valueOf(post.getId()))
                            .putExtra(JOB_NAME, jobName)
                            .putExtra(JOB_NUMBER, jobNumber));
            } else if (post.getStatusId() == ACTION_PART_FAB_READY_FOR_ONSITE) {
                startActivity(new Intent(getActivity(), PartDetailForInspectionActivity.class)
                        .putExtra(PART_ID, String.valueOf(post.getId()))
                        .putExtra(JOB_NAME, jobName)
                        .putExtra(JOB_NUMBER, jobNumber));
            } else {
                startActivity(new Intent(getActivity(), PartDetailActivity.class)
                        .putExtra(PART_ID, String.valueOf(post.getId()))
                        .putExtra(JOB_NAME, jobName)
                        .putExtra(JOB_NUMBER, jobNumber));
            }

        }

    }

    @Override
    public void success(String response, ServicesType type) {
        if (type == ServicesType.getAllParts) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                boolean statusCode = jsonObject.getBoolean(STATUS);
                boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                String message = jsonObject.optString(MESSAGE);
                String responseCode = jsonObject.optString(RESPONSE_CODE);

                if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                    totalRecords = jsonObject.getJSONObject(RESPONSE_DATA).getInt(TOTAL);

                    if (totalRecords > 0) {

                        Gson gson = new Gson();
                        Type typeGson = new TypeToken<List<JobDetailModel>>() {
                        }.getType();

                        ArrayList<JobDetailModel> mJobList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(LIST), typeGson);
                        this.mJobList.addAll(mJobList);
                        adapter.notifyDataSetChanged();

                        if (binding.recyclerView.getVisibility() == View.GONE)
                            binding.recyclerView.setVisibility(View.VISIBLE);
                        if (binding.tvNoRecordFound.getVisibility() == View.VISIBLE)
                            binding.tvNoRecordFound.setVisibility(View.GONE);

                    } else {
                        if (binding.recyclerView.getVisibility() == View.VISIBLE)
                            binding.recyclerView.setVisibility(View.GONE);
                        if (binding.tvNoRecordFound.getVisibility() == View.GONE)
                            binding.tvNoRecordFound.setVisibility(View.VISIBLE);
                    }

                } else {
                    if (showMessage)
                        Utility.toaster(getActivity(), KEY_ERROR, message);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Utility.toaster(getActivity(), KEY_ERROR, getString(R.string.something_went_wrong));
            }
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(getActivity(), KEY_ERROR, message);
    }

    public void beginSearch(String strToBeSearch, boolean reload, int selectedFilter) {
        mStringSearch = strToBeSearch;
        partFabStatusId = selectedFilter;
        if (reload)
        onResume();
    }
}