package com.workflow.nikomi.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.workflow.nikomi.R;
import com.workflow.nikomi.adapters.JobTimesheetOnsiteAdapter;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.FragmentJobTimesheetOnsiteBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.ServicesListener;

import com.workflow.nikomi.model.JobModel;
import com.workflow.nikomi.model.JobTimesheetOnsiteModel;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_FROM;
import static com.workflow.nikomi.utils.Constant.LIMIT;
import static com.workflow.nikomi.utils.Constant.LIMIT_COUNT;
import static com.workflow.nikomi.utils.Constant.LIST;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.OFFSET;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SEARCH;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.TOTAL;
import static com.workflow.nikomi.utils.Constant.WORKSHOP_WORKER;

public class JobTimesheetOnSiteFragment extends Fragment implements JobTimesheetOnsiteAdapter.JobTimesheetOnsiteAdapterListener, ServicesListener {

    private static final String TAG = "JobTimesheetOnSiteFragment";
    int totalRecords = 0;
    int countLoadMore = 0;
    int spinnerFilterPosition = 0;
    String strSearch;
    AppSession appSession;
    JobTimesheetOnsiteAdapter adapter;
    ArrayList<JobTimesheetOnsiteModel> jobTimesheetOnsiteModelArrayList = new ArrayList<>();
    FragmentJobTimesheetOnsiteBinding binding;
    Animation slideinright, slideinleft;
    boolean isCalled = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_job_timesheet_onsite, parent, false);
        appSession = new AppSession(getActivity());
        AnimationInitialization();

        jobTimesheetOnsiteModelArrayList.clear();

        binding.layoutHeader.imgBack.setVisibility(View.GONE);
        binding.layoutHeader.tvHeaderTitle.setText(getResources().getString(R.string.job_time_sheet));

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_line));
        binding.recyclerView.addItemDecoration(itemDecorator);
        adapter = new JobTimesheetOnsiteAdapter(getActivity(), jobTimesheetOnsiteModelArrayList, this, TAG);
        binding.recyclerView.setAdapter(adapter);

        /*Endless scrolling*/
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int currentSize = jobTimesheetOnsiteModelArrayList.size();


                if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == currentSize - 1) {

                    Log.e(TAG, "currentSize: "+currentSize );
                    Log.e(TAG, "totalRecords: "+totalRecords );


                        if (currentSize < totalRecords) {
                            countLoadMore++;
                            Log.e(TAG, "onScrolled: >>>>>>>>>>>>>>>>>>"+countLoadMore );
                            getJobTimesheetForOnsiteWorkerFromServer();
                        }

                }
            }

        });

        binding.layoutHeader.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.layoutHeader.lySearch.getVisibility() == View.VISIBLE) {
                    performBackPressForSearch();
                }
            }
        });

        binding.layoutHeader.imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.layoutHeader.lySearch.getVisibility() == View.VISIBLE) {
                    performBackPressForSearch();
                } else {
                    binding.layoutHeader.lySearch.startAnimation(slideinright);
                    binding.layoutHeader.lySearch.setVisibility(View.VISIBLE);
                    binding.layoutHeader.imgBack.setVisibility(View.VISIBLE);
                    binding.layoutHeader.tvHeaderTitle.setVisibility(View.GONE);
                    binding.layoutHeader.edtSearchUser.requestFocus();
                    Utility.showSoftKeyboard(getActivity(), binding.layoutHeader.edtSearchUser);
                }
            }
        });

        binding.layoutHeader.edtSearchUser.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (cs.length() > 0) {
                    binding.layoutHeader.imgClearSearch.setVisibility(View.VISIBLE);
                    binding.layoutHeader.imgSearch.setVisibility(View.GONE);
                } else {
                    binding.layoutHeader.imgClearSearch.setVisibility(View.GONE);
                    binding.layoutHeader.imgSearch.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        binding.layoutHeader.edtSearchUser.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (binding.layoutHeader.edtSearchUser.getText().length() > 0) {
                        strSearch = binding.layoutHeader.edtSearchUser.getText().toString().trim();
                        getDataFromBegin();
                    } else {
                        performBackPressForSearch();
                    }

                    return true;
                }
                return false;
            }
        });

        binding.layoutHeader.imgClearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.layoutHeader.edtSearchUser.setText("");
                strSearch = null;
                getDataFromBegin();
            }
        });

        return binding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
        getDataFromBegin();
    }

    private void getJobTimesheetForOnsiteWorkerFromServer() {
        if (!Utility.isConnectingToInternet(getActivity())) {

            Utility.toaster(getActivity(), KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));

        } else {
            HashMap<String, String> params = new HashMap<>();
            JsonObject jsonObject = new JsonObject();
            if (strSearch != null)
                params.put(SEARCH, strSearch);
            //params.put(SORT, );//asc or desc
            params.put(LIMIT, String.valueOf(LIMIT_COUNT));
            int offSet = countLoadMore * LIMIT_COUNT;
            params.put(OFFSET, String.valueOf(offSet));

            APIRequest.generalApiRequest(getContext(), getActivity(),
                    JobTimesheetOnSiteFragment.this, params, ServicesType.getJobsheetForOnsiteWorker, true);

        }
    }


    @Override
    public void success(String response, ServicesType type) {
        switch (type) {

            case getJobsheetForOnsiteWorker:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        totalRecords = jsonObject.getJSONObject(RESPONSE_DATA).getInt(TOTAL);


                        if (totalRecords > 0) {
                            Gson gson = new Gson();
                            Type typeGson = new TypeToken<List<JobTimesheetOnsiteModel>>() {
                            }.getType();

                            ArrayList<JobTimesheetOnsiteModel> mList = gson.fromJson(jsonObject.getJSONObject(RESPONSE_DATA).getString(LIST), typeGson);
                             jobTimesheetOnsiteModelArrayList.addAll(mList);
                            adapter.notifyDataSetChanged();

                            if (binding.recyclerView.getVisibility() == View.GONE) {
                                binding.recyclerView.setVisibility(View.VISIBLE);
                                binding.layoutHeader.imgSearch.setVisibility(View.VISIBLE);
                            }

                            if (binding.tvNoRecordFound.getVisibility() == View.VISIBLE)
                                binding.tvNoRecordFound.setVisibility(View.GONE);

                        } else {
                            if (binding.recyclerView.getVisibility() == View.VISIBLE)
                                binding.recyclerView.setVisibility(View.GONE);
                            if (binding.tvNoRecordFound.getVisibility() == View.GONE) {
                                binding.layoutHeader.imgSearch.setVisibility(View.GONE);
                                binding.tvNoRecordFound.setVisibility(View.VISIBLE);
                            }

                        }

                    } else {
                        if (showMessage)
                            Utility.toaster(getActivity(), KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.toaster(getActivity(), KEY_ERROR, getString(R.string.something_went_wrong));
                }
                break;
        }

    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(getActivity(), KEY_ERROR, message);
    }

    private void performBackPressForSearch() {
        Utility.hideSoftKeyboard(getActivity());
        binding.layoutHeader.tvHeaderTitle.setVisibility(View.VISIBLE);
        binding.layoutHeader.lySearch.startAnimation(slideinleft);
        binding.layoutHeader.lySearch.setVisibility(View.GONE);
        binding.layoutHeader.imgBack.setVisibility(View.GONE);
        binding.layoutHeader.edtSearchUser.setText("");
        strSearch = null;
        getDataFromBegin();
    }

    private void AnimationInitialization() {
        slideinleft = AnimationUtils
                .loadAnimation(getContext(), R.anim.left_swipe_anim);

        slideinright = AnimationUtils.loadAnimation(getContext(),
                R.anim.right_swipe_search_anim);
    }

    private void getDataFromBegin() {
        countLoadMore = 0;
        totalRecords = 0;
        if (!jobTimesheetOnsiteModelArrayList.isEmpty()){
            jobTimesheetOnsiteModelArrayList.clear();
        }

        Log.e(TAG, "getDataFromBegin: "+jobTimesheetOnsiteModelArrayList.size() );
        getJobTimesheetForOnsiteWorkerFromServer();
    }

    @Override
    public void onClicked(JobTimesheetOnsiteModel post) {
        
    }
}