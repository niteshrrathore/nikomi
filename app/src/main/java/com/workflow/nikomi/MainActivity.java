package com.workflow.nikomi;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.material.snackbar.Snackbar;
import com.soundcloud.android.crop.Crop;
import com.workflow.nikomi.activity.LoginActivity;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.databinding.ActivityMainBinding;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.fragment.InspectionHistoryFragment;
import com.workflow.nikomi.fragment.InspectionReadyFragment;
import com.workflow.nikomi.fragment.JobDetailFragment;
import com.workflow.nikomi.fragment.JobFragment;
import com.workflow.nikomi.fragment.JobHistoryFragment;
import com.workflow.nikomi.fragment.JobTimesheetOnSiteFragment;
import com.workflow.nikomi.fragment.JobTimesheetWorkshopFragment;
import com.workflow.nikomi.fragment.ProfileFragment;
import com.workflow.nikomi.interfaces.CallBackOk;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.service.AutoSignoutPopupBroadcastService;
import com.workflow.nikomi.utils.AppSession;
import com.workflow.nikomi.utils.BaseActivity;
import com.workflow.nikomi.utils.PermissionsChecker;
import com.workflow.nikomi.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.workflow.nikomi.utils.Constant.ACTION_JOB_COMPLETED;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_JOB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_COMPLETED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_IN_PROGRESS;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_NOT_FULFILLED;
import static com.workflow.nikomi.utils.Constant.ACTION_PART_FAB_READY_FOR_INSPECT;
import static com.workflow.nikomi.utils.Constant.ID;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.PARTS_COMPLETED;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;
import static com.workflow.nikomi.utils.Constant.STATUS_ID;
import static com.workflow.nikomi.utils.Constant.WORKSHOP_WORKER;

public class MainActivity extends BaseActivity implements View.OnClickListener, ServicesListener, CallBackOk {

    private static final String TAG = "MainActivity";
    AppSession appSession;
    int mSelectedPos;
    ActivityMainBinding binding;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean somePermissionsForeverDenied = false;
    PermissionsChecker checker;
    boolean lacksPermissions;
    static final int REQUEST_CODE_PERMISSIONS = 1;

    private boolean checkPermission() {
        lacksPermissions = checker.lacksPermissions(PERMISSIONS);
        if (!lacksPermissions) {
            return true;
        } else {
            if (somePermissionsForeverDenied) {
                View contextView = binding.layoutMain;
                Snackbar snackbar = Snackbar.make(contextView, getString(R.string.please_enable_the_camera_and_storage_permission_to_use_this_feature), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(getColor(R.color.colorRed));
                TextView tv = (TextView) snackbar.getView().findViewById(R.id.snackbar_text);
                tv.setTextSize(14f);
                snackbar.setAction(getString(R.string.permission_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                snackbar.show();
                return false;
            } else {
                requestPermissions(PERMISSIONS, REQUEST_CODE_PERMISSIONS);
                return false;
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setActivity(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("notification_event"));

        checker = new PermissionsChecker(getApplicationContext());

        appSession = new AppSession(this);
        if (appSession.isFirstLogin())
            appSession.setFirstLogin(false);

        if (appSession.getUserDetails() != null) {
            if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER)) {
                binding.layoutInspectionHistory.setVisibility(View.VISIBLE);
                binding.layoutInspect.setVisibility(View.VISIBLE);
            } else {
                binding.layoutInspectionHistory.setVisibility(View.GONE);
                binding.layoutInspect.setVisibility(View.GONE);
            }

            setButtonView(1);

            if (appSession.isNotification())
                binding.imgInspectIndicator.setVisibility(View.VISIBLE);

        } else
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));

    }

    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String updateTo = intent.getStringExtra("update_status");
            Fragment f = MainActivity.this.getSupportFragmentManager().findFragmentById(R.id.frameLayout);
            if (f instanceof InspectionReadyFragment) {
                binding.imgInspectIndicator.setVisibility(View.GONE);
            } else
                binding.imgInspectIndicator.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.layoutJobs:
                setButtonView(1);
                break;

            case R.id.layoutInspect:
                appSession.setNotificationIndicator(false);
                binding.imgInspectIndicator.setVisibility(View.GONE);
                setButtonView(2);
                break;

            case R.id.layoutJobHistory:
                setButtonView(3);
                break;

            case R.id.layoutInspectionHistory:
                setButtonView(5);
                //Utility.toaster(MainActivity.this, KEY_SUCCESS, getResources().getString(R.string.work_in_progress));
                break;

            case R.id.layoutProfile:
                if (checkPermission())
                    setButtonView(4);
                break;

            case R.id.layoutJobTimeSheet:
                setButtonView(6);
                break;

            case R.id.layoutSignout:

                /*To stop the auto generate popup job if user signout*/
               /* List<JobInfo> allPendingJobs = jobScheduler.getAllPendingJobs();
                String s = "";
                for(JobInfo j : allPendingJobs){
                    int jId = j.getId();
                    jobScheduler.cancel(jId);
                    s += "jobScheduler.cancel(" + jId + " )";
                }*/

                //or
                /*jobScheduler.cancelAll();
                jobScheduler.cancel(101);*/

                jobScheduler.cancelAll();
                stopAutoSignoutService();

                if (!Utility.isConnectingToInternet(getApplicationContext())) {
                    Utility.toaster(MainActivity.this, KEY_ERROR, getResources().getString(R.string.please_check_your_network_connection_and_try_again));
                } else if (appSession.isUserOverrideJob()) {
                    if (appSession.getUserPartDetails() != null) {
                        /*If user working on any part or fab
                         * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                        if (appSession.isPartAbortClicked()) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));
                            paramPartCompleted.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);
                        } else if (appSession.getUserPartDetails().getStatusId() == ACTION_PART_FAB_COMPLETED) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");
                            paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);
                        } else if (appSession.getUserPartDetails().getStatusId() == ACTION_PART_FAB_READY_FOR_INSPECT) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);
                        } else {
                            Utility.numberOfPartCompletedDialog(this, this);
                        }
                    } else if (appSession.getUserFabDetails() != null) {
                        /*If user working on any part or fab
                         * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                        if (appSession.isPartAbortClicked()) {
                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "1");
                            params.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateFabAction, true);
                        } else if (appSession.getUserFabDetails().getStatusId() == ACTION_PART_FAB_COMPLETED ||
                                appSession.getUserFabDetails().getStatusId() == ACTION_PART_FAB_IN_PROGRESS) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");
                            paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updateFabAction, true);
                        } else {
                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "1");

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateFabAction, true);
                        }

                    } else if (appSession.getOnsiteJobDetails() != null) {
                        /*If user working on any part or fab
                         * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                        if (appSession.isPartAbortClicked()) {

                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "1");
                            params.put("onsite_status_id", String.valueOf(ACTION_JOB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateJobAction, true);

                        } else if (appSession.getOnsiteJobDetails().getOnsiteStatusId() == ACTION_JOB_COMPLETED ||
                                appSession.getOnsiteJobDetails().getOnsiteStatusId() == ACTION_JOB_IN_PROGRESS) {

                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");
                            paramPartCompleted.put("onsite_status_id", String.valueOf(ACTION_JOB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updateJobAction, true);

                        } else {

                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateJobAction, true);

                        }

                    } else {
                        /*User not working on any of the part*/
                        HashMap<String, String> params = new HashMap<>();
                        APIRequest.generalApiRequest(this, this, this, params, ServicesType.logout, true);
                    }

                } else {

                    if (appSession.getUserPartDetails() != null) {
                        /*If user working on any part or fab
                         * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                        if (appSession.isPartAbortClicked()) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));
                            paramPartCompleted.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);
                        } else if (appSession.getUserPartDetails().getStatusId() == ACTION_PART_FAB_COMPLETED) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                            paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);
                        } else if (appSession.getUserPartDetails().getStatusId() == ACTION_PART_FAB_READY_FOR_INSPECT) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);
                        } else {
                            Utility.numberOfPartCompletedDialog(this, this);
                        }

                    } else if (appSession.getUserFabDetails() != null) {
                        /*If user working on any part or fab
                         * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                        if (appSession.isPartAbortClicked()) {
                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "0");
                            params.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateFabAction, true);
                        } else if (appSession.getUserFabDetails().getStatusId() == ACTION_PART_FAB_COMPLETED ||
                                appSession.getUserFabDetails().getStatusId() == ACTION_PART_FAB_IN_PROGRESS) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                            paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updateFabAction, true);
                        } else {
                            /*Update fab status with NON fullfil*/
                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getUserFabDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateFabAction, true);
                        }

                    } else if (appSession.getOnsiteJobDetails() != null) {
                        /*If user working on any part or fab
                         * and click on Abort button Or the status of the working part/fab is ready for inspect*/
                        if (appSession.isPartAbortClicked()) {
                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "0");
                            params.put("onsite_status_id", String.valueOf(ACTION_JOB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateJobAction, true);
                        } else if (appSession.getOnsiteJobDetails().getOnsiteStatusId() == ACTION_JOB_COMPLETED ||
                                appSession.getOnsiteJobDetails().getOnsiteStatusId() == ACTION_JOB_IN_PROGRESS) {
                            HashMap<String, String> paramPartCompleted = new HashMap<>();
                            paramPartCompleted.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                            paramPartCompleted.put("signout", "1");
                            paramPartCompleted.put("override", "0");
                            paramPartCompleted.put("onsite_status_id", String.valueOf(ACTION_JOB_NOT_FULFILLED));

                            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updateJobAction, true);
                        } else {
                            HashMap<String, String> params = new HashMap<>();
                            params.put(ID, String.valueOf(appSession.getOnsiteJobDetails().getId()));
                            params.put("signout", "1");
                            params.put("override", "0");

                            APIRequest.generalApiRequest(this, this, this, params, ServicesType.updateJobAction, true);
                        }

                    } else {
                        /*User not working on any of the part*/
                        HashMap<String, String> params = new HashMap<>();
                        APIRequest.generalApiRequest(this, this, this, params, ServicesType.logout, true);
                    }

                }

                break;
        }

    }

    private void setButtonView(int selectedPos) {
        mSelectedPos = selectedPos;
        switch (selectedPos) {
            case 1:

                /*Selected background*/
                binding.layoutJobs.setCardBackgroundColor(getColor(R.color.colorRed));
                binding.imgPortfolio.setBackgroundResource(R.drawable.ic_portfolio_white);
                binding.tvJobs.setTextColor(Color.WHITE);

                /*de-Selected background*/
                binding.layoutInspect.setCardBackgroundColor(Color.WHITE);
                binding.imgInspect.setBackgroundResource(R.drawable.ic_inspect_red);
                binding.tvInspect.setTextColor(Color.BLACK);

                binding.layoutJobHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgJobHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvJobHistory.setTextColor(Color.BLACK);

                binding.layoutInspectionHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgInspectionHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvInspectionHistory.setTextColor(Color.BLACK);

                binding.layoutProfile.setCardBackgroundColor(Color.WHITE);
                binding.imgProfile.setBackgroundResource(R.drawable.ic_user_red);
                binding.tvProfile.setTextColor(Color.BLACK);


                binding.layoutJobTimeSheet.setCardBackgroundColor(Color.WHITE);
                binding.imgJobTimeSheet.setBackgroundResource(R.drawable.ic_job_timesheet_red);
                binding.tvJobTimeSheet.setTextColor(Color.BLACK);


                Fragment mFragment = new JobFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frameLayout, mFragment, "JOB_FRAGMENT").addToBackStack("job_fragment").commit();

                break;

            case 2:

                /*Selected background*/
                binding.layoutInspect.setCardBackgroundColor(getColor(R.color.colorRed));
                binding.imgInspect.setBackgroundResource(R.drawable.ic_inspect_white);
                binding.tvInspect.setTextColor(Color.WHITE);

                /*de-Selected background*/
                binding.layoutJobs.setCardBackgroundColor(Color.WHITE);
                binding.imgPortfolio.setBackgroundResource(R.drawable.ic_portfolio_red);
                binding.tvJobs.setTextColor(Color.BLACK);

                binding.layoutJobHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgJobHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvJobHistory.setTextColor(Color.BLACK);

                binding.layoutInspectionHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgInspectionHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvInspectionHistory.setTextColor(Color.BLACK);

                binding.layoutProfile.setCardBackgroundColor(Color.WHITE);
                binding.imgProfile.setBackgroundResource(R.drawable.ic_user_red);
                binding.tvProfile.setTextColor(Color.BLACK);


                binding.layoutJobTimeSheet.setCardBackgroundColor(Color.WHITE);
                binding.imgJobTimeSheet.setBackgroundResource(R.drawable.ic_job_timesheet_red);
                binding.tvJobTimeSheet.setTextColor(Color.BLACK);

                Fragment mInspectionReadyFragment = new InspectionReadyFragment();
                FragmentManager fmInspectionReadyFragment = getSupportFragmentManager();
                fmInspectionReadyFragment.beginTransaction().replace(R.id.frameLayout, mInspectionReadyFragment, "INSPECT_FRAGMENT").addToBackStack("inspect_fragment").commit();

                break;

            case 3:
                /*Selected background*/
                binding.layoutJobHistory.setCardBackgroundColor(getColor(R.color.colorRed));
                binding.imgJobHistory.setBackgroundResource(R.drawable.ic_time_white);
                binding.tvJobHistory.setTextColor(Color.WHITE);

                /*de-Selected background*/
                binding.layoutJobs.setCardBackgroundColor(Color.WHITE);
                binding.imgPortfolio.setBackgroundResource(R.drawable.ic_portfolio_red);
                binding.tvJobs.setTextColor(Color.BLACK);

                binding.layoutInspect.setCardBackgroundColor(Color.WHITE);
                binding.imgInspect.setBackgroundResource(R.drawable.ic_inspect_red);
                binding.tvInspect.setTextColor(Color.BLACK);

                binding.layoutInspectionHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgInspectionHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvInspectionHistory.setTextColor(Color.BLACK);

                binding.layoutProfile.setCardBackgroundColor(Color.WHITE);
                binding.imgProfile.setBackgroundResource(R.drawable.ic_user_red);
                binding.tvProfile.setTextColor(Color.BLACK);


                binding.layoutJobTimeSheet.setCardBackgroundColor(Color.WHITE);
                binding.imgJobTimeSheet.setBackgroundResource(R.drawable.ic_job_timesheet_red);
                binding.tvJobTimeSheet.setTextColor(Color.BLACK);
                Fragment mJobHistoryFragment = new JobHistoryFragment();
                FragmentManager fmJobHistoryFragment = getSupportFragmentManager();
                fmJobHistoryFragment.beginTransaction().replace(R.id.frameLayout, mJobHistoryFragment, "JOB_HISTORY_FRAGMENT").addToBackStack("job_history_fragment").commit();

                break;

            case 4:

                /*Selected background*/
                binding.layoutProfile.setCardBackgroundColor(getColor(R.color.colorRed));
                binding.imgProfile.setBackgroundResource(R.drawable.ic_user_white);
                binding.tvProfile.setTextColor(Color.WHITE);

                /*de-Selected background*/
                binding.layoutJobs.setCardBackgroundColor(Color.WHITE);
                binding.imgPortfolio.setBackgroundResource(R.drawable.ic_portfolio_red);
                binding.tvJobs.setTextColor(Color.BLACK);

                binding.layoutInspect.setCardBackgroundColor(Color.WHITE);
                binding.imgInspect.setBackgroundResource(R.drawable.ic_inspect_red);
                binding.tvInspect.setTextColor(Color.BLACK);

                binding.layoutInspectionHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgInspectionHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvInspectionHistory.setTextColor(Color.BLACK);

                binding.layoutJobHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgJobHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvJobHistory.setTextColor(Color.BLACK);


                binding.layoutJobTimeSheet.setCardBackgroundColor(Color.WHITE);
                binding.imgJobTimeSheet.setBackgroundResource(R.drawable.ic_job_timesheet_red);
                binding.tvJobTimeSheet.setTextColor(Color.BLACK);

                Fragment mProfileFragment = new ProfileFragment();
                FragmentManager profileFragmentManager = getSupportFragmentManager();
                profileFragmentManager.beginTransaction().replace(R.id.frameLayout, mProfileFragment, "PROFILE_FRAGMENT").addToBackStack("profile_fragment").commit();

                break;

            case 5:

                /*Selected background*/
                binding.layoutInspectionHistory.setCardBackgroundColor(getColor(R.color.colorRed));
                binding.imgInspectionHistory.setBackgroundResource(R.drawable.ic_time_white);
                binding.tvInspectionHistory.setTextColor(Color.WHITE);

                /*de-Selected background*/
                binding.layoutJobs.setCardBackgroundColor(Color.WHITE);
                binding.imgPortfolio.setBackgroundResource(R.drawable.ic_portfolio_red);
                binding.tvJobs.setTextColor(Color.BLACK);

                binding.layoutInspect.setCardBackgroundColor(Color.WHITE);
                binding.imgInspect.setBackgroundResource(R.drawable.ic_inspect_red);
                binding.tvInspect.setTextColor(Color.BLACK);

                binding.layoutJobHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgJobHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvJobHistory.setTextColor(Color.BLACK);

                binding.layoutProfile.setCardBackgroundColor(Color.WHITE);
                binding.imgProfile.setBackgroundResource(R.drawable.ic_user_red);
                binding.tvProfile.setTextColor(Color.BLACK);


                binding.layoutJobTimeSheet.setCardBackgroundColor(Color.WHITE);
                binding.imgJobTimeSheet.setBackgroundResource(R.drawable.ic_job_timesheet_red);
                binding.tvJobTimeSheet.setTextColor(Color.BLACK);

                Fragment mInspectionHistoryFragment = new InspectionHistoryFragment();
                FragmentManager fmInspectionHistoryFragment = getSupportFragmentManager();
                fmInspectionHistoryFragment.beginTransaction().replace(R.id.frameLayout, mInspectionHistoryFragment, "INSPECT_HISTORY_FRAGMENT").addToBackStack("inspect_history_fragment").commit();
                break;

            case 6:

                /*Selected background*/
                binding.layoutJobTimeSheet.setCardBackgroundColor(getColor(R.color.colorRed));
                binding.imgJobTimeSheet.setBackgroundResource(R.drawable.ic_job_timesheet_white);
                binding.tvJobTimeSheet.setTextColor(Color.WHITE);

                /*de-Selected background*/
                binding.layoutJobs.setCardBackgroundColor(Color.WHITE);
                binding.imgPortfolio.setBackgroundResource(R.drawable.ic_portfolio_red);
                binding.tvJobs.setTextColor(Color.BLACK);

                binding.layoutInspect.setCardBackgroundColor(Color.WHITE);
                binding.imgInspect.setBackgroundResource(R.drawable.ic_inspect_red);
                binding.tvInspect.setTextColor(Color.BLACK);

                binding.layoutInspectionHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgInspectionHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvInspectionHistory.setTextColor(Color.BLACK);

                binding.layoutProfile.setCardBackgroundColor(Color.WHITE);
                binding.imgProfile.setBackgroundResource(R.drawable.ic_user_red);
                binding.tvProfile.setTextColor(Color.BLACK);

                binding.layoutJobHistory.setCardBackgroundColor(Color.WHITE);
                binding.imgJobHistory.setBackgroundResource(R.drawable.ic_time_red);
                binding.tvJobHistory.setTextColor(Color.BLACK);

                if (appSession.getUserDetails().getType().equals(WORKSHOP_WORKER)) {

                    Fragment  jobTimesheetWorkshopFragment= new JobTimesheetWorkshopFragment();
                    FragmentManager jobManager = getSupportFragmentManager();
                    jobManager.beginTransaction().replace(R.id.frameLayout, jobTimesheetWorkshopFragment, "JOB_TIMESHEET_WORKSHOP_FRAGMENT").addToBackStack("jobTimesheetWorkshopFragment").commit();
                }else {

                    Fragment jobTimesheetOnSiteFragment = new JobTimesheetOnSiteFragment();
                    FragmentManager jobTimesheetFragmentManager = getSupportFragmentManager();
                    jobTimesheetFragmentManager.beginTransaction().replace(R.id.frameLayout, jobTimesheetOnSiteFragment, "JOB_TIMESHEET_ONSITE_FRAGMENT").addToBackStack("jobTimesheetOnSiteFragment").commit();

                }
                break;


        }
    }

    @Override
    public void success(String response, ServicesType type) {
        switch (type) {
            case logout:
            case updateJobAction:
            case updateFabAction:
            case updatePartAction:
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean statusCode = jsonObject.getBoolean(STATUS);
                    boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                    String message = jsonObject.optString(MESSAGE);
                    String responseCode = jsonObject.optString(RESPONSE_CODE);

                    if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {

                        if (showMessage)
                            Utility.toaster(MainActivity.this, KEY_SUCCESS, message);

                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        appSession.clearPrefs();
                        startActivity(intent);
                        finish();

                    } else {
                        if (showMessage)
                            Utility.toaster(MainActivity.this, KEY_ERROR, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void failed(String message, ServicesType type) {
        Utility.toaster(MainActivity.this, KEY_ERROR, message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Crop.REQUEST_CROP) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {

        Fragment f = MainActivity.this.getSupportFragmentManager().findFragmentById(R.id.frameLayout);
        if (f instanceof JobFragment) {
            this.finish();
        } else if (f instanceof JobDetailFragment) {
            super.onBackPressed();
        } else {
            setButtonView(1);
        }

    }

    @Override
    public void onOkClicked(String strNoPartCompleted) {

        if (appSession.getUserPartDetails().getQuantity() < Integer.parseInt(strNoPartCompleted)) {
            Utility.toaster(this, KEY_ERROR, getResources().getString(R.string.invalid_quantity));
        } else {
            HashMap<String, String> paramPartCompleted = new HashMap<>();
            paramPartCompleted.put(ID, String.valueOf(appSession.getUserPartDetails().getId()));
            paramPartCompleted.put("signout", "1");
            paramPartCompleted.put(PARTS_COMPLETED, strNoPartCompleted);
            paramPartCompleted.put(STATUS_ID, String.valueOf(ACTION_PART_FAB_NOT_FULFILLED));

            if (appSession.isUserOverrideJob())
                paramPartCompleted.put("override", "1");
            else
                paramPartCompleted.put("override", "0");

            APIRequest.generalApiRequest(this, this, this, paramPartCompleted, ServicesType.updatePartAction, true);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE_PERMISSIONS:

                if (!somePermissionsForeverDenied) {
                    if (checker.hasAllPermissionsGranted(grantResults)) {
                        somePermissionsForeverDenied = false;
                        /*permission granted*/
                        //openDialogForUploadPhoto();
                        setButtonView(4);
                    } else {

                        for (String permission : permissions) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                                //denied
                                Log.e("denied", permission);
                            } else {
                                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                                    //allowed
                                    Log.e("allowed", permission);
                                } else {
                                    //set to never ask again
                                    Log.e("set to never ask again", permission);
                                    somePermissionsForeverDenied = true;

                                    break;
                                }
                            }
                        }

                    }
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }

    }
}