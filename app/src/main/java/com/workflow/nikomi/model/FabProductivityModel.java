package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FabProductivityModel {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("fabrication_id")
    @Expose
    private Integer fabricationId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFabricationId() {
        return fabricationId;
    }

    public void setFabricationId(Integer fabricationId) {
        this.fabricationId = fabricationId;
    }
}
