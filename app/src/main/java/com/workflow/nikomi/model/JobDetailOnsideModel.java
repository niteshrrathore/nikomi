package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JobDetailOnsideModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("job_number")
    @Expose
    private String jobNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("onsite_worker_user_id")
    @Expose
    private Integer onsiteWorkerUserId;
    @SerializedName("onsite_worker_ack_timestamp")
    @Expose
    private String onsiteWorkerAckTimestamp;
    @SerializedName("onsite_status_id")
    @Expose
    private Integer onsiteStatusId;
    @SerializedName("workshop_status_id")
    @Expose
    private Integer workshopStatusId;
    @SerializedName("is_publish")
    @Expose
    private Integer isPublish;
    @SerializedName("inspect_by_user_id")
    @Expose
    private Integer inspectByUserId;
    @SerializedName("inspect_by_signature")
    @Expose
    private String inspectBySignature;
    @SerializedName("inspect_by_timestamp")
    @Expose
    private String inspectByTimestamp;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("parts")
    @Expose
    private List<PartDetailModel> parts = null;
    @SerializedName("job_files")
    @Expose
    private ArrayList<JobInspectPhotoModel> jobFiles;
    @SerializedName("fabrications")
    @Expose
    private List<FabDetailModel> fabrications = null;
    @SerializedName("worker_productivities")
    @Expose
    private ArrayList<JobProductivityModel> workerProductivities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getOnsiteWorkerUserId() {
        return onsiteWorkerUserId;
    }

    public void setOnsiteWorkerUserId(Integer onsiteWorkerUserId) {
        this.onsiteWorkerUserId = onsiteWorkerUserId;
    }

    public String getOnsiteWorkerAckTimestamp() {
        return onsiteWorkerAckTimestamp;
    }

    public void setOnsiteWorkerAckTimestamp(String onsiteWorkerAckTimestamp) {
        this.onsiteWorkerAckTimestamp = onsiteWorkerAckTimestamp;
    }

    public Integer getOnsiteStatusId() {
        return onsiteStatusId;
    }

    public void setOnsiteStatusId(Integer onsiteStatusId) {
        this.onsiteStatusId = onsiteStatusId;
    }

    public Integer getWorkshopStatusId() {
        return workshopStatusId;
    }

    public void setWorkshopStatusId(Integer workshopStatusId) {
        this.workshopStatusId = workshopStatusId;
    }

    public Integer getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }

    public Integer getInspectByUserId() {
        return inspectByUserId;
    }

    public void setInspectByUserId(Integer inspectByUserId) {
        this.inspectByUserId = inspectByUserId;
    }

    public String getInspectBySignature() {
        return inspectBySignature;
    }

    public void setInspectBySignature(String inspectBySignature) {
        this.inspectBySignature = inspectBySignature;
    }

    public String getInspectByTimestamp() {
        return inspectByTimestamp;
    }

    public void setInspectByTimestamp(String inspectByTimestamp) {
        this.inspectByTimestamp = inspectByTimestamp;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<PartDetailModel> getParts() {
        return parts;
    }

    public void setParts(List<PartDetailModel> parts) {
        this.parts = parts;
    }

    public ArrayList<JobInspectPhotoModel> getJobFiles() {
        return jobFiles;
    }

    public void setJobFiles(ArrayList<JobInspectPhotoModel> jobFiles) {
        this.jobFiles = jobFiles;
    }

    public List<FabDetailModel> getFabrications() {
        return fabrications;
    }

    public void setFabrications(List<FabDetailModel> fabrications) {
        this.fabrications = fabrications;
    }

    public ArrayList<JobProductivityModel> getWorkerProductivities() {
        return workerProductivities;
    }

    public void setWorkerProductivities(ArrayList<JobProductivityModel> workerProductivities) {
        this.workerProductivities = workerProductivities;
    }
}
