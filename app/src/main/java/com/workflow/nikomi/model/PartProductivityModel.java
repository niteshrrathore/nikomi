package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PartProductivityModel {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("part_id")
    @Expose
    private Integer partId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPartId() {
        return partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }
}
