package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PartDetailModel implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("part_number")
    @Expose
    private String partNumber;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("material_name")
    @Expose
    private String materialName="";

    @SerializedName("section_size")
    @Expose
    private String sectionSize;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("s_grade")
    @Expose
    private String sGrade;
    @SerializedName("po_number")
    @Expose
    private Integer poNumber;
    @SerializedName("part_drawing")
    @Expose
    private String partDrawing;
    @SerializedName("front_sheet")
    @Expose
    private String frontSheet;
    @SerializedName("front_sheet_timestamp")
    @Expose
    private String frontSheetTimestamp;
    @SerializedName("parts_completed")
    @Expose
    private Integer partsCompleted;
    @SerializedName("cut_by_user_id")
    @Expose
    private Integer cutByUserId;
    @SerializedName("cut_by_signature")
    @Expose
    private String cutBySignature;
    @SerializedName("cut_by_timestamp")
    @Expose
    private String cutByTimestamp;
    @SerializedName("cut_checked_by_user_id")
    @Expose
    private Integer cutCheckedByUserId;
    @SerializedName("cut_checked_by_signature")
    @Expose
    private String cutCheckedBySignature;
    @SerializedName("cut_checked_by_timestamp")
    @Expose
    private String cutCheckedByTimestamp;
    @SerializedName("drill_by_user_id")
    @Expose
    private Integer drillByUserId;
    @SerializedName("drill_by_signature")
    @Expose
    private String drillBySignature;
    @SerializedName("drill_by_timestamp")
    @Expose
    private String drillByTimestamp;
    @SerializedName("drill_checked_by_user_id")
    @Expose
    private Integer drillCheckedByUserId;
    @SerializedName("drill_checked_by_signature")
    @Expose
    private String drillCheckedBySignature;
    @SerializedName("drill_checked_by_timestamp")
    @Expose
    private String drillCheckedByTimestamp;
    @SerializedName("inspect_by_user_id")
    @Expose
    private Integer inspectByUserId;
    @SerializedName("inspect_by_signature")
    @Expose
    private String inspectBySignature;
    @SerializedName("inspect_by_timestamp")
    @Expose
    private String inspectByTimestamp;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("is_publish")
    @Expose
    private Integer isPublish;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("cut_by_user")
    @Expose
    private UserDetail cutByUser;
    @SerializedName("cut_checked_by_user")
    @Expose
    private UserDetail cutCheckedByUser;
    @SerializedName("drill_by_user")
    @Expose
    private UserDetail drillByUser;
    @SerializedName("drill_checked_by_user")
    @Expose
    private UserDetail drillCheckedByUser;
    @SerializedName("inspect_by_user")
    @Expose
    private UserDetail inspectByUser;
    @SerializedName("part_files")
    @Expose
    private ArrayList<PartInspectPhotoModel> partFiles;
    @SerializedName("worker_productivities")
    @Expose
    private ArrayList<PartProductivityModel> workerProductivities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSectionSize() {
        return sectionSize;
    }

    public void setSectionSize(String sectionSize) {
        this.sectionSize = sectionSize;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSGrade() {
        return sGrade;
    }

    public void setSGrade(String sGrade) {
        this.sGrade = sGrade;
    }

    public Integer getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(Integer poNumber) {
        this.poNumber = poNumber;
    }

    public String getPartDrawing() {
        return partDrawing;
    }

    public void setPartDrawing(String partDrawing) {
        this.partDrawing = partDrawing;
    }

    public String getFrontSheet() {
        return frontSheet;
    }

    public void setFrontSheet(String frontSheet) {
        this.frontSheet = frontSheet;
    }

    public String getFrontSheetTimestamp() {
        return frontSheetTimestamp;
    }

    public void setFrontSheetTimestamp(String frontSheetTimestamp) {
        this.frontSheetTimestamp = frontSheetTimestamp;
    }

    public Integer getPartsCompleted() {
        return partsCompleted;
    }

    public void setPartsCompleted(Integer partsCompleted) {
        this.partsCompleted = partsCompleted;
    }

    public Integer getCutByUserId() {
        return cutByUserId;
    }

    public void setCutByUserId(Integer cutByUserId) {
        this.cutByUserId = cutByUserId;
    }

    public String getCutBySignature() {
        return cutBySignature;
    }

    public void setCutBySignature(String cutBySignature) {
        this.cutBySignature = cutBySignature;
    }

    public String getCutByTimestamp() {
        return cutByTimestamp;
    }

    public void setCutByTimestamp(String cutByTimestamp) {
        this.cutByTimestamp = cutByTimestamp;
    }

    public Integer getCutCheckedByUserId() {
        return cutCheckedByUserId;
    }

    public void setCutCheckedByUserId(Integer cutCheckedByUserId) {
        this.cutCheckedByUserId = cutCheckedByUserId;
    }

    public String getCutCheckedBySignature() {
        return cutCheckedBySignature;
    }

    public void setCutCheckedBySignature(String cutCheckedBySignature) {
        this.cutCheckedBySignature = cutCheckedBySignature;
    }

    public String getCutCheckedByTimestamp() {
        return cutCheckedByTimestamp;
    }

    public void setCutCheckedByTimestamp(String cutCheckedByTimestamp) {
        this.cutCheckedByTimestamp = cutCheckedByTimestamp;
    }

    public Integer getDrillByUserId() {
        return drillByUserId;
    }

    public void setDrillByUserId(Integer drillByUserId) {
        this.drillByUserId = drillByUserId;
    }

    public String getDrillBySignature() {
        return drillBySignature;
    }

    public void setDrillBySignature(String drillBySignature) {
        this.drillBySignature = drillBySignature;
    }

    public String getDrillByTimestamp() {
        return drillByTimestamp;
    }

    public void setDrillByTimestamp(String drillByTimestamp) {
        this.drillByTimestamp = drillByTimestamp;
    }

    public Integer getDrillCheckedByUserId() {
        return drillCheckedByUserId;
    }

    public void setDrillCheckedByUserId(Integer drillCheckedByUserId) {
        this.drillCheckedByUserId = drillCheckedByUserId;
    }

    public String getDrillCheckedBySignature() {
        return drillCheckedBySignature;
    }

    public void setDrillCheckedBySignature(String drillCheckedBySignature) {
        this.drillCheckedBySignature = drillCheckedBySignature;
    }

    public String getDrillCheckedByTimestamp() {
        return drillCheckedByTimestamp;
    }

    public void setDrillCheckedByTimestamp(String drillCheckedByTimestamp) {
        this.drillCheckedByTimestamp = drillCheckedByTimestamp;
    }

    public Integer getInspectByUserId() {
        return inspectByUserId;
    }

    public void setInspectByUserId(Integer inspectByUserId) {
        this.inspectByUserId = inspectByUserId;
    }

    public String getInspectBySignature() {
        return inspectBySignature;
    }

    public void setInspectBySignature(String inspectBySignature) {
        this.inspectBySignature = inspectBySignature;
    }

    public String getInspectByTimestamp() {
        return inspectByTimestamp;
    }

    public void setInspectByTimestamp(String inspectByTimestamp) {
        this.inspectByTimestamp = inspectByTimestamp;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getsGrade() {
        return sGrade;
    }

    public void setsGrade(String sGrade) {
        this.sGrade = sGrade;
    }

    public UserDetail getCutByUser() {
        return cutByUser;
    }

    public void setCutByUser(UserDetail cutByUser) {
        this.cutByUser = cutByUser;
    }

    public UserDetail getCutCheckedByUser() {
        return cutCheckedByUser;
    }

    public void setCutCheckedByUser(UserDetail cutCheckedByUser) {
        this.cutCheckedByUser = cutCheckedByUser;
    }

    public UserDetail getDrillByUser() {
        return drillByUser;
    }

    public void setDrillByUser(UserDetail drillByUser) {
        this.drillByUser = drillByUser;
    }

    public UserDetail getDrillCheckedByUser() {
        return drillCheckedByUser;
    }

    public void setDrillCheckedByUser(UserDetail drillCheckedByUser) {
        this.drillCheckedByUser = drillCheckedByUser;
    }

    public UserDetail getInspectByUser() {
        return inspectByUser;
    }

    public void setInspectByUser(UserDetail inspectByUser) {
        this.inspectByUser = inspectByUser;
    }

    public ArrayList<PartInspectPhotoModel> getPartFiles() {
        return partFiles;
    }

    public void setPartFiles(ArrayList<PartInspectPhotoModel> partFiles) {
        this.partFiles = partFiles;
    }

    public ArrayList<PartProductivityModel> getWorkerProductivities() {
        return workerProductivities;
    }

    public void setWorkerProductivities(ArrayList<PartProductivityModel> workerProductivities) {
        this.workerProductivities = workerProductivities;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }
}

