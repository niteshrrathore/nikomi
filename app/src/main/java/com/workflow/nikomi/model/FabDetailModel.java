package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class FabDetailModel implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("item_number")
    @Expose
    private String itemNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("wps")
    @Expose
    private String wps;
    @SerializedName("wire")
    @Expose
    private String wire;
    @SerializedName("gas")
    @Expose
    private String gas;
    @SerializedName("primer")
    @Expose
    private String primer;
    @SerializedName("galv")
    @Expose
    private Integer galv;

    @SerializedName("paint")
    @Expose
    public PaintModel PaintModel;


    @SerializedName("fab_drawing")
    @Expose
    private String fabDrawing;
    @SerializedName("front_sheet")
    @Expose
    private String frontSheet;
    @SerializedName("front_sheet_timestamp")
    @Expose
    private String frontSheetTimestamp;
    @SerializedName("parts_completed")
    @Expose
    private Integer partsCompleted;
    @SerializedName("fab_by_user_id")
    @Expose
    private Integer fabByUserId;
    @SerializedName("fab_by_signature")
    @Expose
    private String fabBySignature;
    @SerializedName("fab_by_timestamp")
    @Expose
    private String fabByTimestamp;
    @SerializedName("fab_checked_by_user_id")
    @Expose
    private Integer fabCheckedByUserId;
    @SerializedName("fab_checked_by_signature")
    @Expose
    private String fabCheckedBySignature;
    @SerializedName("fab_checked_by_timestamp")
    @Expose
    private String fabCheckedByTimestamp;
    @SerializedName("weld_by_user_id")
    @Expose
    private Integer weldByUserId;
    @SerializedName("weld_by_signature")
    @Expose
    private String weldBySignature;
    @SerializedName("weld_by_timestamp")
    @Expose
    private String weldByTimestamp;
    @SerializedName("weld_checked_by_user_id")
    @Expose
    private Integer weldCheckedByUserId;
    @SerializedName("weld_checked_by_signature")
    @Expose
    private String weldCheckedBySignature;
    @SerializedName("weld_checked_by_timestamp")
    @Expose
    private String weldCheckedByTimestamp;
    @SerializedName("inspect_by_user_id")
    @Expose
    private Integer inspectByUserId;
    @SerializedName("inspect_by_signature")
    @Expose
    private String inspectBySignature;
    @SerializedName("inspect_by_timestamp")
    @Expose
    private String inspectByTimestamp;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("is_publish")
    @Expose
    private Integer isPublish;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("fab_by_user")
    @Expose
    private UserDetail fabByUser;
    @SerializedName("fab_checked_by_user")
    @Expose
    private UserDetail fabCheckedByUser;
    @SerializedName("weld_by_user")
    @Expose
    private UserDetail weldByUser;
    @SerializedName("weld_checked_by_user")
    @Expose
    private UserDetail weldCheckedByUser;
    @SerializedName("inspect_by_user")
    @Expose
    private UserDetail inspectByUser;
    @SerializedName("fab_files")
    @Expose
    private ArrayList<FabInspectPhotoModel> fabFiles;
    @SerializedName("worker_productivities")
    @Expose
    private ArrayList<FabProductivityModel> workerProductivities;
    @SerializedName("parts")
    @Expose
    private ArrayList<PartDetailModel> parts;
    @SerializedName("parts_count")
    @Expose
    private Integer partsCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWps() {
        return wps;
    }

    public void setWps(String wps) {
        this.wps = wps;
    }

    public String getWire() {
        return wire;
    }

    public void setWire(String wire) {
        this.wire = wire;
    }

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }

    public String getPrimer() {
        return primer;
    }

    public void setPrimer(String primer) {
        this.primer = primer;
    }

    public Integer getGalv() {
        return galv;
    }

    public void setGalv(Integer galv) {
        this.galv = galv;
    }

    public com.workflow.nikomi.model.PaintModel getPaintModel() {
        return PaintModel;
    }

    public void setPaintModel(com.workflow.nikomi.model.PaintModel paintModel) {
        PaintModel = paintModel;
    }

    public String getFabDrawing() {
        return fabDrawing;
    }

    public void setFabDrawing(String fabDrawing) {
        this.fabDrawing = fabDrawing;
    }

    public String getFrontSheet() {
        return frontSheet;
    }

    public void setFrontSheet(String frontSheet) {
        this.frontSheet = frontSheet;
    }

    public String getFrontSheetTimestamp() {
        return frontSheetTimestamp;
    }

    public void setFrontSheetTimestamp(String frontSheetTimestamp) {
        this.frontSheetTimestamp = frontSheetTimestamp;
    }

    public Integer getPartsCompleted() {
        return partsCompleted;
    }

    public void setPartsCompleted(Integer partsCompleted) {
        this.partsCompleted = partsCompleted;
    }

    public Integer getFabByUserId() {
        return fabByUserId;
    }

    public void setFabByUserId(Integer fabByUserId) {
        this.fabByUserId = fabByUserId;
    }

    public String getFabBySignature() {
        return fabBySignature;
    }

    public void setFabBySignature(String fabBySignature) {
        this.fabBySignature = fabBySignature;
    }

    public String getFabByTimestamp() {
        return fabByTimestamp;
    }

    public void setFabByTimestamp(String fabByTimestamp) {
        this.fabByTimestamp = fabByTimestamp;
    }

    public Integer getFabCheckedByUserId() {
        return fabCheckedByUserId;
    }

    public void setFabCheckedByUserId(Integer fabCheckedByUserId) {
        this.fabCheckedByUserId = fabCheckedByUserId;
    }

    public String getFabCheckedBySignature() {
        return fabCheckedBySignature;
    }

    public void setFabCheckedBySignature(String fabCheckedBySignature) {
        this.fabCheckedBySignature = fabCheckedBySignature;
    }

    public String getFabCheckedByTimestamp() {
        return fabCheckedByTimestamp;
    }

    public void setFabCheckedByTimestamp(String fabCheckedByTimestamp) {
        this.fabCheckedByTimestamp = fabCheckedByTimestamp;
    }

    public Integer getWeldByUserId() {
        return weldByUserId;
    }

    public void setWeldByUserId(Integer weldByUserId) {
        this.weldByUserId = weldByUserId;
    }

    public String getWeldBySignature() {
        return weldBySignature;
    }

    public void setWeldBySignature(String weldBySignature) {
        this.weldBySignature = weldBySignature;
    }

    public String getWeldByTimestamp() {
        return weldByTimestamp;
    }

    public void setWeldByTimestamp(String weldByTimestamp) {
        this.weldByTimestamp = weldByTimestamp;
    }

    public Integer getWeldCheckedByUserId() {
        return weldCheckedByUserId;
    }

    public void setWeldCheckedByUserId(Integer weldCheckedByUserId) {
        this.weldCheckedByUserId = weldCheckedByUserId;
    }

    public String getWeldCheckedBySignature() {
        return weldCheckedBySignature;
    }

    public void setWeldCheckedBySignature(String weldCheckedBySignature) {
        this.weldCheckedBySignature = weldCheckedBySignature;
    }

    public String getWeldCheckedByTimestamp() {
        return weldCheckedByTimestamp;
    }

    public void setWeldCheckedByTimestamp(String weldCheckedByTimestamp) {
        this.weldCheckedByTimestamp = weldCheckedByTimestamp;
    }

    public Integer getInspectByUserId() {
        return inspectByUserId;
    }

    public void setInspectByUserId(Integer inspectByUserId) {
        this.inspectByUserId = inspectByUserId;
    }

    public String getInspectBySignature() {
        return inspectBySignature;
    }

    public void setInspectBySignature(String inspectBySignature) {
        this.inspectBySignature = inspectBySignature;
    }

    public String getInspectByTimestamp() {
        return inspectByTimestamp;
    }

    public void setInspectByTimestamp(String inspectByTimestamp) {
        this.inspectByTimestamp = inspectByTimestamp;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserDetail getFabByUser() {
        return fabByUser;
    }

    public void setFabByUser(UserDetail fabByUser) {
        this.fabByUser = fabByUser;
    }

    public UserDetail getFabCheckedByUser() {
        return fabCheckedByUser;
    }

    public void setFabCheckedByUser(UserDetail fabCheckedByUser) {
        this.fabCheckedByUser = fabCheckedByUser;
    }

    public UserDetail getWeldByUser() {
        return weldByUser;
    }

    public void setWeldByUser(UserDetail weldByUser) {
        this.weldByUser = weldByUser;
    }

    public UserDetail getWeldCheckedByUser() {
        return weldCheckedByUser;
    }

    public void setWeldCheckedByUser(UserDetail weldCheckedByUser) {
        this.weldCheckedByUser = weldCheckedByUser;
    }

    public UserDetail getInspectByUser() {
        return inspectByUser;
    }

    public void setInspectByUser(UserDetail inspectByUser) {
        this.inspectByUser = inspectByUser;
    }

    public ArrayList<FabInspectPhotoModel> getFabFiles() {
        return fabFiles;
    }

    public void setFabFiles(ArrayList<FabInspectPhotoModel> fabFiles) {
        this.fabFiles = fabFiles;
    }

    public ArrayList<FabProductivityModel> getWorkerProductivities() {
        return workerProductivities;
    }

    public void setWorkerProductivities(ArrayList<FabProductivityModel> workerProductivities) {
        this.workerProductivities = workerProductivities;
    }

    public ArrayList<PartDetailModel> getParts() {
        return parts;
    }

    public void setParts(ArrayList<PartDetailModel> parts) {
        this.parts = parts;
    }


    public Integer getPartsCount() {
        return partsCount;
    }

    public void setPartsCount(Integer partsCount) {
        this.partsCount = partsCount;
    }

}

