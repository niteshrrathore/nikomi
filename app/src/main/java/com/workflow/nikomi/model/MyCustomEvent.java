package com.workflow.nikomi.model;

public class MyCustomEvent {

    public boolean isUpdate;
    public String isHardUpdate;

    public MyCustomEvent(boolean isUpdate, String isHardUpdate) {
        this.isUpdate = isUpdate;
        this.isHardUpdate = isHardUpdate;
    }
}
