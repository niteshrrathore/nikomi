package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobProductivityModel {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }
}
