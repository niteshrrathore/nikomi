package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FabInspectPhotoModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("fabrication_id")
    @Expose
    private Integer fabricationId;
    @SerializedName("file_type")
    @Expose
    private String fileType;
    @SerializedName("mime_type")
    @Expose
    private String mimeType;
    @SerializedName("file_location")
    @Expose
    private String fileLocation;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFabricationId() {
        return fabricationId;
    }

    public void setFabricationId(Integer fabricationId) {
        this.fabricationId = fabricationId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
