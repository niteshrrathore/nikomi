package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("job_number")
    @Expose
    private String jobNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("onsite_worker_user_id")
    @Expose
    private Integer onsiteWorkerUserId;
    @SerializedName("onsite_worker_ack_timestamp")
    @Expose
    private String onsiteWorkerAckTimestamp;
    @SerializedName("onsite_status_id")
    @Expose
    private Integer onsiteStatusId;
    @SerializedName("workshop_status_id")
    @Expose
    private Integer workshopStatusId;
    @SerializedName("is_publish")
    @Expose
    private Integer isPublish;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("parts_count")
    @Expose
    private Integer parts_count;
    @SerializedName("fabrications_count")
    @Expose
    private Integer fabricationsCount;
    @SerializedName("working_on")
    @Expose
    private Integer working_on;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getOnsiteWorkerUserId() {
        return onsiteWorkerUserId;
    }

    public void setOnsiteWorkerUserId(Integer onsiteWorkerUserId) {
        this.onsiteWorkerUserId = onsiteWorkerUserId;
    }

    public String getOnsiteWorkerAckTimestamp() {
        return onsiteWorkerAckTimestamp;
    }

    public void setOnsiteWorkerAckTimestamp(String onsiteWorkerAckTimestamp) {
        this.onsiteWorkerAckTimestamp = onsiteWorkerAckTimestamp;
    }

    public Integer getOnsiteStatusId() {
        return onsiteStatusId;
    }

    public void setOnsiteStatusId(Integer onsiteStatusId) {
        this.onsiteStatusId = onsiteStatusId;
    }

    public Integer getWorkshopStatusId() {
        return workshopStatusId;
    }

    public void setWorkshopStatusId(Integer workshopStatusId) {
        this.workshopStatusId = workshopStatusId;
    }

    public Integer getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getParts_count() {
        return parts_count;
    }

    public void setParts_count(Integer parts_count) {
        this.parts_count = parts_count;
    }

    public Integer getFabricationsCount() {
        return fabricationsCount;
    }

    public void setFabricationsCount(Integer fabricationsCount) {
        this.fabricationsCount = fabricationsCount;
    }

    public Integer getWorking_on() {
        return working_on;
    }

    public void setWorking_on(Integer working_on) {
        this.working_on = working_on;
    }
}