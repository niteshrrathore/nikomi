package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class JobDetailModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("item_number")
    @Expose
    private String itemNumber;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("inspect_ready_by_user_id")
    @Expose
    private Integer inspectReadyByUserId;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("part_number")
    @Expose
    private String partNumber;
    @SerializedName("parts_count")
    @Expose
    private Integer partsCount;
    @SerializedName("worked_on")
    @Expose
    private Integer workedOn;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("worker_productivities")
    @Expose
    private ArrayList<PartProductivityModel> workerProductivities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public ArrayList<PartProductivityModel> getWorkerProductivities() {
        return workerProductivities;
    }

    public void setWorkerProductivities(ArrayList<PartProductivityModel> workerProductivities) {
        this.workerProductivities = workerProductivities;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public Integer getPartsCount() {
        return partsCount;
    }

    public void setPartsCount(Integer partsCount) {
        this.partsCount = partsCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getWorkedOn() {
        return workedOn;
    }

    public void setWorkedOn(Integer workedOn) {
        this.workedOn = workedOn;
    }

    public Integer getInspectReadyByUserId() {
        return inspectReadyByUserId;
    }

    public void setInspectReadyByUserId(Integer inspectReadyByUserId) {
        this.inspectReadyByUserId = inspectReadyByUserId;
    }
}

