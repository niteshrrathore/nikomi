package com.workflow.nikomi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobTimesheetOnsiteModel {
    @SerializedName("job_name")
    @Expose
    private String jobName;
    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("parts_count")
    @Expose
    private Integer partsCount;
    @SerializedName("fabrications_count")
    @Expose
    private Integer fabricationsCount;
    @SerializedName("total_time_spent")
    @Expose
    private String totalTimeSpent;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Integer getPartsCount() {
        return partsCount;
    }

    public void setPartsCount(Integer partsCount) {
        this.partsCount = partsCount;
    }

    public Integer getFabricationsCount() {
        return fabricationsCount;
    }

    public void setFabricationsCount(Integer fabricationsCount) {
        this.fabricationsCount = fabricationsCount;
    }

    public String getTotalTimeSpent() {
        return totalTimeSpent;
    }

    public void setTotalTimeSpent(String totalTimeSpent) {
        this.totalTimeSpent = totalTimeSpent;
    }

}