package com.workflow.nikomi.utils;
import android.app.Activity;
import android.app.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;

import com.workflow.nikomi.R;


public class CustomProgressDialog {

    private Activity activity = null;
    private String infoText = "";
    private AlertDialog dialog;
    private AlertDialog.Builder alertDialogBuilder;


    public CustomProgressDialog(Activity activity) {

        this.activity = activity;
        // dialog = new Dialog(this.activity);
        alertDialogBuilder = new AlertDialog.Builder(activity);
        dialog = alertDialogBuilder.create();

    }

    public void setTitle(String infoText) {
        this.infoText = infoText;
    }

    public void show() {
        if (!dialog.isShowing()) {
            //dialog.show();
            openCustomProgressDialog();
        }
    }

    public void dismiss() {
        try {
            if (dialog != null) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isShowing() {
        try {
            if (dialog.isShowing()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void openCustomProgressDialog() {
        try {

            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setView(activity.getLayoutInflater().inflate(R.layout.dialog_custom_progess, null));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


}
