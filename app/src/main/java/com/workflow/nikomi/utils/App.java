package com.workflow.nikomi.utils;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.LocaleList;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.workflow.nikomi.R;
import com.workflow.nikomi.activity.LoginActivity;
import com.workflow.nikomi.api.APIRequest;
import com.workflow.nikomi.enums.ServicesType;
import com.workflow.nikomi.interfaces.CallbackOKCancelInterface;
import com.workflow.nikomi.interfaces.CallbackOkInterface;
import com.workflow.nikomi.interfaces.ServicesListener;
import com.workflow.nikomi.model.JobPartStatusModel;
import com.workflow.nikomi.model.MyCustomEvent;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.OkHttpClient;

import static com.workflow.nikomi.utils.Constant.ANDROID;
import static com.workflow.nikomi.utils.Constant.DEVICE_TYPE;
import static com.workflow.nikomi.utils.Constant.EMAIL;
import static com.workflow.nikomi.utils.Constant.KEY_ERROR;
import static com.workflow.nikomi.utils.Constant.KEY_SUCCESS;
import static com.workflow.nikomi.utils.Constant.MESSAGE;
import static com.workflow.nikomi.utils.Constant.PASSWORD;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE;
import static com.workflow.nikomi.utils.Constant.RESPONSE_CODE_SUCCESS;
import static com.workflow.nikomi.utils.Constant.RESPONSE_DATA;
import static com.workflow.nikomi.utils.Constant.SHOW_MESSAGE;
import static com.workflow.nikomi.utils.Constant.STATUS;

public class App extends Application implements Application.ActivityLifecycleCallbacks , ServicesListener {

    private static App mInstance = null;
    private static Context context;
    private static OkHttpClient mOkHttpClient;
    public static final String CHANNEL_ID = "AutoSignoutServiceChannel";
    public static String Live_URL;
   public static final String DEV_BASE_URL = "https://dashboard.nikomi.co.uk/";
    //  public static final String DEV_BASE_URL = "http://ec2-3-11-80-80.eu-west-2.compute.amazonaws.com/";
    public static final String QA_BASE_URL = "http://ec2-3-10-142-63.eu-west-2.compute.amazonaws.com/";
    public static final String PRODUCTION_BASE_URL = "https://dashboard.nikomi.co.uk/";
    public static  int showOverRideValue=0;
    private static final String TAG = "App";
    Activity activity;
    private int activityReferences = 0;
    private boolean isActivityChangingConfigurations = false;
    AppSession appSession;

    public void onCreate() {
        super.onCreate();
        mInstance = this;
        App.context = getApplicationContext();
        mOkHttpClient = OkClientFactory.provideOkHttpClient(this);
        createNotificationChannel();
        appSession = new AppSession(this);
        registerActivityLifecycleCallbacks(this);
        //Live_URL = App.QA_BASE_URL;
    }

    @Override
    protected void attachBaseContext(Context base) {
        //super.attachBaseContext(LocaleHelper.onAttach(base, "da"));//en
        /*if (LocaleList.getDefault().get(0).getLanguage().equals("en")){
            super.attachBaseContext(LocaleHelper.onAttach(base, "en"));//da
        }else{
            super.attachBaseContext(LocaleHelper.onAttach(base, "da"));//en
        }*/

        super.attachBaseContext(base);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        /*if (!LocaleHelper.getLanguage(this).equals(newConfig.locale.getLanguage())){
            if (newConfig.locale.getLanguage().equals("en")){
                LocaleHelper.setLocale(this, "en");
            }else{
                LocaleHelper.setLocale(this, "da");
            }

        }*/
    }

    private AppCompatActivity mCurrentActivity = null;

    public AppCompatActivity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(AppCompatActivity mCurrentActivity) {
        this.mCurrentActivity = mCurrentActivity;
    }

    public static App getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return App.context;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Example Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    //---------------------------------------------------------------------activity life cycle------------------------------------
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        this.activity = activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (++activityReferences == 1 && !isActivityChangingConfigurations) {
            if (appSession.isUserLogin()) {
                Log.e(TAG, "onActivityStarted:>>>>>>>>>>>>>> " );
                geVersionUpdateDetails();



            }
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

        isActivityChangingConfigurations = activity.isChangingConfigurations();
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {
            // App enters background
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {


    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    //--------------------------------------------------------------api call---------------------------------------------

     public void  geVersionUpdateDetails(){
        HashMap<String,String> params = new HashMap<>();
        params.put(DEVICE_TYPE, ANDROID);
         APIRequest.generalApiRequest(getApplicationContext(), activity,
                 this, params, ServicesType.getVersionCodeDetails, true);
     }


    @Override
    public void success(String response, ServicesType type) {

        switch (type) {
            case getVersionCodeDetails:
            try {
                JSONObject jsonObject = new JSONObject(response);

                boolean statusCode = jsonObject.getBoolean(STATUS);
                boolean showMessage = jsonObject.getBoolean(SHOW_MESSAGE);
                String message = jsonObject.optString(MESSAGE);
                String responseCode = jsonObject.optString(RESPONSE_CODE);

                if (statusCode && responseCode.equals(RESPONSE_CODE_SUCCESS)) {
                    JSONObject responseObject = jsonObject.getJSONObject(RESPONSE_DATA);
                    long versionCode = Utility.getVersionCode(this);
                    long hardVersionCode = responseObject.getLong("hard_version");
                    long softVersionCode = responseObject.getLong("soft_version");

                    if (hardVersionCode > versionCode) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(new MyCustomEvent(true, "1"));
                            }
                        }, 400);

                    } else if (softVersionCode > versionCode) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(new MyCustomEvent(true, "0"));
                            }
                        }, 400);
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            break;
        }

    }

    @Override
    public void failed(String message, ServicesType type) {

    }
}
