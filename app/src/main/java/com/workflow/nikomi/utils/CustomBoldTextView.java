package com.workflow.nikomi.utils;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public class CustomBoldTextView extends AppCompatTextView {

    public CustomBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(Utility.setFontBold(context));
    }

    public CustomBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public CustomBoldTextView(Context context) {
        super(context);
    }


}
