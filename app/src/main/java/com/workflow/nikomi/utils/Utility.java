package com.workflow.nikomi.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kyanogen.signatureview.SignatureView;
import com.workflow.nikomi.BuildConfig;
import com.workflow.nikomi.R;
import com.squareup.picasso.Picasso;
import com.workflow.nikomi.adapters.DocumentDetailsAdapter;
import com.workflow.nikomi.interfaces.CallBackOk;
import com.workflow.nikomi.interfaces.CallBackSignatureSave;
import com.workflow.nikomi.interfaces.CallbackOKCancelInterface;
import com.workflow.nikomi.interfaces.CallbackOkInterface;
import com.workflow.nikomi.interfaces.OverrideJobCallBack;
import com.workflow.nikomi.model.DocumentDetailsModel;
import com.workflow.nikomi.model.JobPartStatusModel;
import com.workflow.nikomi.model.UserDetail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.ACTIVITY_SERVICE;

public class Utility {

    private static final String TAG = "Utility";
    private static Pattern pattern;
    private static Matcher matcher;
    private static int itemSelectedDistPosition;
    private static final String DATE_PATTERN = "(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((19|20)\\d\\d)";
    public final static String Output_Date = "MMM dd, yyyy 'at' HH:mm";
    private static final String USERNAME_PATTERN = "[a-zA-Z][a-zA-Z ]+[a-zA-Z]$";

    /**
     * Checking for all possible internet providers
     **/
    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidUserName(final String username) {
        matcher = Pattern.compile(USERNAME_PATTERN).matcher(username);
        return matcher.matches();

    }

    public static boolean isValidMobile(String phone) {
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 7 && phone.length() <= 15;
        }
        return false;
    }

    /* this method is used for hide the soft keyboard of device*/
    public static void showSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    /* this method is used for hide the soft keyboard of device*/
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void toaster(final Activity activity, final boolean isError, final String message) {
        try {
            activity.runOnUiThread(new Runnable() {

                public void run() {

                    //Creating the LayoutInflater instance
                    LayoutInflater inflaterToast = activity.getLayoutInflater();

                    //Getting the View object as defined in the customtoast.xml file
                    View layout = inflaterToast.inflate(R.layout.custom_toast, (ViewGroup) activity.findViewById(R.id.custom_toast_layout));

                    LinearLayout toastLayout = layout.findViewById(R.id.custom_toast_layout);

                    if (isError) {
                        toastLayout.setBackground(ContextCompat.getDrawable(activity, R.drawable.rounded_courner_red));
                    } else {
                        toastLayout.setBackground(ContextCompat.getDrawable(activity, R.drawable.rounded_courner_green));
                    }

                    TextView toastMsg = layout.findViewById(R.id.custom_toast_message);
                    toastMsg.setText(message);
                    //Creating the Toast object
                    Toast toast = new Toast(activity);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.setView(layout);//setting the view of custom toast layout
                    toast.show();

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Validate date format with regular expression
     *
     * @param date date address for validation
     * @return true valid date fromat, false invalid date format
     */
    public static boolean validateDate(final String date) {

        pattern = Pattern.compile(DATE_PATTERN);
        matcher = pattern.matcher(date);

        if (matcher.matches()) {

            matcher.reset();

            if (matcher.find()) {

                String day = matcher.group(1);
                String month = matcher.group(2);
                int year = Integer.parseInt(matcher.group(3));

                if (day.equals("31") &&
                        (month.equals("4") || month.equals("6") || month.equals("9") ||
                                month.equals("11") || month.equals("04") || month.equals("06") ||
                                month.equals("09"))) {
                    return false; // only 1,3,5,7,8,10,12 has 31 days
                } else if (month.equals("2") || month.equals("02")) {
                    //leap year
                    if (year % 4 == 0) {
                        if (day.equals("30") || day.equals("31")) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        if (day.equals("29") || day.equals("30") || day.equals("31")) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public static boolean isValidPassword(String password) {

        String expression = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!]).{8,30}$";
        return password.matches(expression);

    }

    public static String getUserName(String name) {
        String userName;
        if (name.length() >= 13) {

            userName = name.substring(0, 13) + "...";

        } else {

            userName = name;

        }

        return userName;

    }

    public static String getPartAndFabNum(Context context, Integer partNum, Integer FabNum) {
        return context.getString(R.string.parts_) + partNum + " | " + context.getString(R.string.fabrication_) + FabNum;

    }

    public static String getQuantity(Context context, Integer partNum, String type, Integer partCount) {
        if (type.equals(Constant.TYPE_ONLY_PART))
            return context.getString(R.string.quantity) + ": " + partNum;
        else
            return context.getString(R.string.parts_included) + ": " + partCount;
    }

    public static String getType(Context context, String type) {
        if (type.equals(Constant.TYPE_ONLY_FAB))
            return context.getString(R.string.fabrication);
        else
            return context.getString(R.string.part);
    }

    public static String getJobTitle(Context context, String jobName, String jobNumber) {
        return jobNumber + " / " + jobName;
    }

    public static String getPartTitle(Context context, String partName, String partNumber) {
        return partNumber + "/" + partName;
    }

    public static void getStatus(Context context, Integer id, String mStatusType, TextView tvJobStatus) {

        AppSession appSession = new AppSession(context);
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<JobPartStatusModel>>() {
        }.getType();
        ArrayList<JobPartStatusModel> arrPackageData = gson.fromJson(appSession.getJobPartStatus(), type);

        for (int i = 0; i < arrPackageData.size(); i++) {

            if (arrPackageData.get(i).getId().equals(id) /*&& jobPartStatusModels.get(i).getType().equals(mStatusType)*/) {
                switch (id) {
                    case 1:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.blue));
                        break;
                    case 2:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.orange));
                        break;
                    case 3:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.colorRed));
                        break;
                    case 4://Ready for Inspection
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.green));
                        break;
                    case 5:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.green));
                        break;
                    case 6:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.blue));
                        break;
                    case 7:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.orange));
                        break;
                    case 8:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.green));
                        break;
                    case 9:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.green));
                        break;
                    case 10:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.green));
                        break;
                    case 11:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        tvJobStatus.setTextColor(context.getColor(R.color.colorRed));
                        break;

                    default:
                        tvJobStatus.setText(arrPackageData.get(i).getName());
                        break;
                }
            }
        }

    }

    public static String getGMTTimeFromString(String dtStart) {
        //Locale localeSpanish = new Locale("de", "DE");
        SimpleDateFormat formatInput = new SimpleDateFormat("dd MMM yyyy, HH:mm", Locale.getDefault());
        SimpleDateFormat formatOutput = new SimpleDateFormat("dd MMM yyyy, HH:mm", Locale.getDefault());
        formatOutput.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return formatOutput.format(formatInput.parse(dtStart));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }

    public static String getDate(Context context, String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        formatter.setTimeZone(TimeZone.getDefault());
        Date value = null;
        try {
            value = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new TimeAgoUtils(context).getTimeAgo(value);
    }

    public static String getDateForChat(Context context, Date value) {

        return new TimeAgoUtils(context).getTimeAgo(value);
    }

    public static String getDateFromLong(Context context, String time) {

        long millisecond = Long.parseLong(time) * 1000L;

        return new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(new Date(millisecond));
    }

    public static Date getDate(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        //return formatter.format(calendar.getTime());
        return calendar.getTime();
    }

    public static void loadImageFromPicasso(Context context, String imageUrl, Drawable placeHolder, ImageView imageView) {

        Picasso.with(context).load(imageUrl).placeholder(placeHolder).into(imageView);
    }

    public static void loadImageFromPicasso(Context context, String imageUrl, ImageView imageView) {

        Picasso.with(context).load(imageUrl).into(imageView);
    }

    public static File createImageFile(Context context) throws IOException {
        String mCurrentPhotoPath;
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.i("mCurrentPhotoPath>>>", mCurrentPhotoPath.toString());
        return image;
    }


    public static long convertGMTToLocaleTime(long unixTime) {
        try {
            Date date = new Date(unixTime * 1000);

            DateFormat dateFormat = new SimpleDateFormat(Output_Date);
            dateFormat.setTimeZone(TimeZone.getDefault());

            String localDate = dateFormat.format(date);
            long localUnixTime = dateFormat.parse(localDate).getTime() / 1000;
            return localUnixTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Date localToGMT() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date gmt = new Date(sdf.format(date));
        return gmt;
    }


    public static Date gmttoLocalDate(Date date) {

        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        return local;

    }

    /***
     * This will convert the SED to Unix Timestamp
     * See https://www.unixtimestamp.com/index.php for more info on UnixTimeStamp
     *
     * @param
     * @return
     */
    public static long getUnixTimeStampFromSED() {
        try {

            Date date = new Date();
            long unixTime = (long) date.getTime() / 1000;

            return unixTime;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;

    }

    public static String getAgeFromDate(int year, int month, int day) {

        Calendar dob = Calendar.getInstance();

        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int today_m = today.get(Calendar.MONTH);

        int dob_m = dob.get(Calendar.MONTH);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (dob_m > today_m) {
            age--;
        } else if (dob_m == today_m) {

            int day_today = today.get(Calendar.DAY_OF_MONTH);

            int day_dob = dob.get(Calendar.DAY_OF_MONTH);
            if (day_dob > day_today) {
                age--;
            }

        }

        return age + "";
    }

    public static void numberOfPartCompletedDialog(final Activity mContext,
                                                   final CallBackOk mCallBackOk) {

        final LayoutInflater inflater = LayoutInflater.from(mContext);
        AppSession appSession = new AppSession(mContext);

        final AppCompatDialog mDialog = new AppCompatDialog(mContext, R.style.DialogSimple);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View dialogLayout = inflater.inflate(R.layout.dialog_number_of_part_completed, null);
        mDialog.setContentView(dialogLayout);

        CustomSemiBoldTextView tvJobName = mDialog.findViewById(R.id.tvJobName);

        if (appSession.getUserPartDetails().getName() != null && !appSession.getUserPartDetails().getName().isEmpty())
            tvJobName.setText(appSession.getUserPartDetails().getName());

        final EditText edtNumberOfPartsCompleted = mDialog.findViewById(R.id.edtNumberOfPartsCompleted);
        TextView btnSave = mDialog.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edtNumberOfPartsCompleted.getText().length() > 0) {
                    mCallBackOk.onOkClicked(edtNumberOfPartsCompleted.getText().toString());
                    mDialog.dismiss();
                } else {
                    Toast.makeText(mContext, "Enter no. of parts completed", Toast.LENGTH_SHORT).show();
                }


            }
        });

        ImageView imgClose = mDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    public static void showUserInActiveDialog(final Activity mContext, String msg) {

        final LayoutInflater inflater = LayoutInflater.from(mContext);

        final AppCompatDialog mDialog = new AppCompatDialog(mContext, R.style.DialogSimple);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View dialogLayout = inflater.inflate(R.layout.dialog_user_inactive, null);
        mDialog.setContentView(dialogLayout);

        CustomRegularTextView tvJobName = mDialog.findViewById(R.id.tvMsg);
        tvJobName.setText(msg);

        TextView btnSave = mDialog.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    public static void overrideJobDialog(final Activity mContext,
                                         final OverrideJobCallBack mOverrideJobCallBack) {

        final LayoutInflater inflater = LayoutInflater.from(mContext);

        final AppCompatDialog mDialog = new AppCompatDialog(mContext, R.style.DialogSimple);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View dialogLayout = inflater.inflate(R.layout.dialog_override_job, null);
        mDialog.setContentView(dialogLayout);

        TextView btnYes = mDialog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mOverrideJobCallBack.onYes();
                mDialog.dismiss();

            }
        });

        TextView btnNo = mDialog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mOverrideJobCallBack.onNo();
                mDialog.dismiss();

            }
        });

        ImageView imgClose = mDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOverrideJobCallBack.onNo();
                mDialog.dismiss();
            }
        });

        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    public static void getSignatureDialog(final Activity mContext, final CallBackSignatureSave mCallBackOk,
                                          final int id, final UserDetail itemAtPosition, final Spinner mSpinner) {

        final LayoutInflater inflater = LayoutInflater.from(mContext);

        final AppCompatDialog mDialog = new AppCompatDialog(mContext, R.style.DialogSimple);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View dialogLayout = inflater.inflate(R.layout.dialog_signature, null);
        mDialog.setContentView(dialogLayout);

        final SignatureView signatureView = (SignatureView) mDialog.findViewById(R.id.signature_view);
        TextView btnSaveSignature = mDialog.findViewById(R.id.btnSaveSignature);
        btnSaveSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!signatureView.isBitmapEmpty()) {
                    mCallBackOk.onOkClicked(signatureView.getSignatureBitmap(), id, itemAtPosition);
                    mDialog.dismiss();
                } else {
                    Toast.makeText(mContext, "Nee to get the signature", Toast.LENGTH_SHORT).show();
                }

            }
        });

        TextView tvClearLink = mDialog.findViewById(R.id.tvClearLink);
        tvClearLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signatureView.clearCanvas();

            }
        });

        ImageView imgClose = mDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                mCallBackOk.onDismiss(mSpinner);
            }
        });

        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }


   /* public static void openGaDocumentDialog(final Activity mContext, ArrayList<DocumentDetailsModel> documentDetailsModelArrayList) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);

        final AppCompatDialog mDialog = new AppCompatDialog(mContext, R.style.DialogSimple);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View dialogLayout = inflater.inflate(R.layout.dialog_view_details, null);
        mDialog.setContentView(dialogLayout);

        RecyclerView recyclerView = (RecyclerView) mDialog.findViewById(R.id.rvGaDocument);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.divider_line));
        recyclerView.addItemDecoration(itemDecorator);

        ImageView imgClose = (ImageView) mDialog.findViewById(R.id.imgClose);
        DocumentDetailsAdapter documentDetailsAdapter = new DocumentDetailsAdapter(mContext, documentDetailsModelArrayList);
        recyclerView.setAdapter(documentDetailsAdapter);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        if (!mDialog.isShowing()) {
            mDialog.show();
        }
    }
*/

    public static void getSignatureDialog(final Activity mContext,
                                          final CallBackSignatureSave mCallBackOk) {

        final LayoutInflater inflater = LayoutInflater.from(mContext);

        final AppCompatDialog mDialog = new AppCompatDialog(mContext, R.style.DialogSimple);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View dialogLayout = inflater.inflate(R.layout.dialog_signature, null);
        mDialog.setContentView(dialogLayout);

        final SignatureView signatureView = (SignatureView) mDialog.findViewById(R.id.signature_view);
        TextView btnSaveSignature = mDialog.findViewById(R.id.btnSaveSignature);
        btnSaveSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!signatureView.isBitmapEmpty()) {
                    mCallBackOk.onOkClicked(signatureView.getSignatureBitmap());
                    mDialog.dismiss();
                } else {
                    Toast.makeText(mContext, "Nee to get the signature", Toast.LENGTH_SHORT).show();
                }

            }
        });

        TextView tvClearLink = mDialog.findViewById(R.id.tvClearLink);
        tvClearLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signatureView.clearCanvas();

            }
        });

        ImageView imgClose = mDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        isInBackground = false;
                    }
                }
            }
        }

        return isInBackground;
    }


    public static Typeface setFontRegular(Context context) {
        Typeface typeLobster = Typeface.createFromAsset(context.getAssets(), "fonts/nunito_regular.ttf");
        return typeLobster;
    }

    public static Typeface setFontBold(Context context) {
        Typeface typeLobster = Typeface.createFromAsset(context.getAssets(), "fonts/nunito_bold.ttf");
        return typeLobster;
    }

    public static Typeface setFontSemiBold(Context context) {
        Typeface typeLobster = Typeface.createFromAsset(context.getAssets(), "fonts/nunito_semibold.ttf");
        return typeLobster;
    }

    public static Date getCurrentDateTime() {

        return Calendar.getInstance().getTime();
    }

    public static ArrayList<File> bitmapToFile(Context context, Bitmap bitmap) { // File name like "image.png"
        //create a file to write bitmap data

        ArrayList<File> imagesArray = new ArrayList<>();
        try {

            File file = new File(Environment.getExternalStorageDirectory() + File.separator + Calendar.getInstance().getTimeInMillis() + ".png");
            file.createNewFile();

//Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos); // YOU can also save it in JPEG
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            imagesArray.add(file);
            return imagesArray;
        } catch (Exception e) {
            e.printStackTrace();
            return null; // it will return null
        }
    }

    public static boolean isServiceRunning(Context mContext, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }

            }
        }
        return false;
    }

    public static long getVersionCode(Context mContext) {
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            long versionCode = BuildConfig.VERSION_CODE;
            return  versionCode;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 1;
    }
    public static AlertDialog showAlertYesCallBackForUpdate(final Context context, String msg, String yes, final CallbackOkInterface callbackOkInterface) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setTitle(context.getString(R.string.alert))
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        callbackOkInterface.onSuccess(true);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
        return alertDialog;
    }


    public static void openPlayStoreSoftUpdate(Context mContext, Activity activity) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
        } catch (android.content.ActivityNotFoundException anfe) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
        }
    }

    public static AlertDialog showAlertYesNoCallBackForUpdate(final Context context, String msg, String yes, String no, final CallbackOKCancelInterface callbackOkInterface) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setTitle(context.getString(R.string.alert))
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton(no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        callbackOkInterface.onFailure(true);
                    }
                })
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        callbackOkInterface.onSuccess(true);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
        return alertDialog;
    }


}
