package com.workflow.nikomi.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.workflow.nikomi.model.FabDetailModel;
import com.workflow.nikomi.model.JobDetailOnsideModel;
import com.workflow.nikomi.model.LangModel;
import com.workflow.nikomi.model.PartDetailModel;
import com.workflow.nikomi.model.UserDetail;

import static android.content.Context.MODE_PRIVATE;
import static com.workflow.nikomi.utils.Constant.TOKEN;

public class AppSession {

    private SharedPreferences mPrefs;
    private SharedPreferences.Editor prefsEditor;
    private static final String SHARED = "SugarDating";
    private static final String USER_DETAILS = "user";
    private static final String USER_PART_DETAILS = "part";
    private static final String USER_FAB_DETAILS = "fab";
    private static final String USER_JOB_DETAILS = "job";
    private static final String USER_PREF_LANG = "user_language";
    private static final String IS_FIRST_LOGIN = "is_first_login";
    private static final String IS_USER_LOGIN = "is_user_login";
    private static final String IS_ABORT_CLICKED = "is_abort_clicked";
    private static final String IS_NOTIFICATION = "is_notification";
    private static final String IS_OVERRIDE_JOB = "is_override_job";
    private static final String IS_SHIFT_HR = "shift_hour";
    private static final String DEVICE_TOKEN = "device_token";
    private static final String JOB_PART_STATUS = "status";
    private static final String BASE_URL = "base_url";

    public AppSession(Context context) {
        this.mPrefs = context.getSharedPreferences(SHARED, MODE_PRIVATE);
    }

    public void clearPrefs(){
        prefsEditor = mPrefs.edit();
        prefsEditor.clear();
        prefsEditor.apply();
    }

    public void storeJobPartStatus(String mStrStatus){

        prefsEditor = mPrefs.edit();
        prefsEditor.putString(JOB_PART_STATUS, mStrStatus);
        prefsEditor.commit();

    }

    public String getJobPartStatus(){

        String json = mPrefs.getString(JOB_PART_STATUS, "");

        return json;
    }

    public void storeBaseUrl(String mStrStatus){

        prefsEditor = mPrefs.edit();
        prefsEditor.putString(BASE_URL, mStrStatus);
        prefsEditor.commit();

    }

    public String getBaseUrl(){

        String json = mPrefs.getString(BASE_URL, "");

        return json;
    }

    public void storeShiftHour(String strShiftHr){

        prefsEditor = mPrefs.edit();
        prefsEditor.putString(IS_SHIFT_HR, strShiftHr);
        prefsEditor.commit();

    }

    public String getShiftHour(){

        String json = mPrefs.getString(IS_SHIFT_HR, null);

        return json;
    }

    public void storeUser(UserDetail mUserDetail){

        prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mUserDetail);
        prefsEditor.putString(USER_DETAILS, json);
        prefsEditor.commit();

    }

    public UserDetail getUserDetails(){
        Gson gson = new Gson();
        String json = mPrefs.getString(USER_DETAILS, null);
        UserDetail obj = gson.fromJson(json, UserDetail.class);

        return obj;
    }

    public void storeUserPartDetail(PartDetailModel mPartDetailModel, int actionPartFabInProgress){
        mPartDetailModel.setStatusId(actionPartFabInProgress);
        prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mPartDetailModel);
        prefsEditor.putString(USER_PART_DETAILS, json);
        prefsEditor.commit();

    }

    public PartDetailModel getUserPartDetails(){
        Gson gson = new Gson();
        String json = mPrefs.getString(USER_PART_DETAILS, null);
        PartDetailModel obj = gson.fromJson(json, PartDetailModel.class);

        return obj;
    }

    public void storeUserFabDetail(FabDetailModel mFabDetailModel, int actionPartFabInProgress){
        mFabDetailModel.setStatusId(actionPartFabInProgress);
        prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mFabDetailModel);
        prefsEditor.putString(USER_FAB_DETAILS, json);
        prefsEditor.commit();

    }

    public FabDetailModel getUserFabDetails(){
        Gson gson = new Gson();
        String json = mPrefs.getString(USER_FAB_DETAILS, null);
        FabDetailModel obj = gson.fromJson(json, FabDetailModel.class);

        return obj;
    }

    public void storeOnsiteJobDetail(JobDetailOnsideModel mJobDetailOnsideModel, int actionPartFabInProgress){
        mJobDetailOnsideModel.setOnsiteStatusId(actionPartFabInProgress);
        prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mJobDetailOnsideModel);
        prefsEditor.putString(USER_JOB_DETAILS, json);
        prefsEditor.commit();

    }

    public JobDetailOnsideModel getOnsiteJobDetails(){
        Gson gson = new Gson();
        String json = mPrefs.getString(USER_JOB_DETAILS, null);
        JobDetailOnsideModel obj = gson.fromJson(json, JobDetailOnsideModel.class);

        return obj;
    }

    public boolean isPartAbortClicked(){
        return mPrefs.getBoolean(IS_ABORT_CLICKED, false);
    }

    public void setPartAbortClicked(boolean isLogin){

        prefsEditor = mPrefs.edit();
        prefsEditor.putBoolean(IS_ABORT_CLICKED, isLogin);
        prefsEditor.apply();

    }

    public boolean isNotification(){
        return mPrefs.getBoolean(IS_NOTIFICATION, false);
    }

    public void setNotificationIndicator(boolean isLogin){

        prefsEditor = mPrefs.edit();
        prefsEditor.putBoolean(IS_NOTIFICATION, isLogin);
        prefsEditor.apply();

    }

    public void storeLang(LangModel resultModel){

        prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(resultModel);
        prefsEditor.putString(USER_PREF_LANG, json);
        prefsEditor.commit();

    }

    public LangModel getLang(){
        Gson gson = new Gson();
        String json = mPrefs.getString(USER_PREF_LANG, "");
        LangModel obj = gson.fromJson(json, LangModel.class);

        return obj;
    }

    public void storeSessionToken(String deviceToken){

        prefsEditor = mPrefs.edit();
        prefsEditor.putString(TOKEN, deviceToken);
        prefsEditor.apply();

    }

    public String getSessionToken(){

        String json = mPrefs.getString(TOKEN, "");

        return json;
    }

    public void setFirstLogin(boolean deviceToken){

        prefsEditor = mPrefs.edit();
        prefsEditor.putBoolean(IS_FIRST_LOGIN, deviceToken);
        prefsEditor.apply();

    }

    public boolean isFirstLogin(){
        return mPrefs.getBoolean(IS_FIRST_LOGIN, true);
    }

    public void setUserLogin(boolean isLogin){

        prefsEditor = mPrefs.edit();
        prefsEditor.putBoolean(IS_USER_LOGIN, isLogin);
        prefsEditor.apply();

    }

    public boolean isUserLogin(){
        return mPrefs.getBoolean(IS_USER_LOGIN, false);
    }

    public void setOverrideJob(boolean isOverrideJob){

        prefsEditor = mPrefs.edit();
        prefsEditor.putBoolean(IS_OVERRIDE_JOB, isOverrideJob);
        prefsEditor.apply();

    }

    public boolean isUserOverrideJob(){
        return mPrefs.getBoolean(IS_OVERRIDE_JOB, false);
    }

    public void storeUserDeviceToken(String deviceToken){

        prefsEditor = mPrefs.edit();
        prefsEditor.putString(DEVICE_TOKEN, deviceToken);
        prefsEditor.apply();

    }

    public String getUserDeviceToken(){

        String json = mPrefs.getString(DEVICE_TOKEN, "");

        return json;
    }
}
