package com.workflow.nikomi.utils;

import android.app.Application;

import androidx.annotation.NonNull;

import com.workflow.nikomi.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by IZISS on 12/06/2020.
 */
public class OkClientFactory {
    // Cache size for the OkHttpClient

    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    private static final String BASIC_AUTHENTICATION_USER_NAME = "smartercareplan";
    private static final String BASIC_AUTHENTICATION_PASSWORD = "smartercareplan123";


    private OkClientFactory() {
    }

    @NonNull
    public static OkHttpClient provideOkHttpClient(Application app) {
        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(100, TimeUnit.SECONDS)
                .connectTimeout(100, TimeUnit.SECONDS)
                .cache(cache);

        Gson gson = new GsonBuilder().setLenient()
                .create();

//
//        OkHttpClient.Builder builder = new OkHttpClient.Builder().addInterceptor(new AuthenticationInterceptor(BASIC_AUTHENTICATION_USER_NAME, BASIC_AUTHENTICATION_PASSWORD)).readTimeout(600, TimeUnit.SECONDS).connectTimeout(600, TimeUnit.SECONDS)
//                .cache(cache);


        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.interceptors().add(loggingInterceptor);
        }
        return builder.build();
    }
}
