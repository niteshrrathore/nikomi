package com.workflow.nikomi.utils;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public class CustomSemiBoldTextView extends AppCompatTextView {

    public CustomSemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(Utility.setFontSemiBold(context));
    }

    public CustomSemiBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public CustomSemiBoldTextView(Context context) {
        super(context);
    }


}
