package com.workflow.nikomi.utils;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public class CustomRegularTextView extends AppCompatTextView {

    public CustomRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(Utility.setFontRegular(context));
    }

    public CustomRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public CustomRegularTextView(Context context) {
        super(context);
    }


}
