package com.workflow.nikomi.utils;

public class Constant {

    public static final String INPUTE_DATE_FORMAT = "dd MMM yyyy, ";
    public static final String INPUTE_DATE_FORMAT_AGE = "dd-MM-yyyy";
    public static final String OUTPUT_DATE_FORMAT = "dd MMM yyyy hh:mm a";
    public static final String OUTPUT_DATE_FORMAT_INVITATION = "dd MMM yyyy, hh:mm a";

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "token";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String MOBILE = "mobile";
    public static final String TYPE = "type";
    public static final String AVTAR = "avatar";
    public static final String CREATED_BY = "created_by";
    public static final String DELETE_STATUS = "delete_status";
    public static final String IS_ACTIVE = "is_active";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";
    public static final String PROFILE_URL = "profile_url";
    public static final String PASSWORD_CONFIRMATION = "password_confirmation";
    public static final String OLD_PASSWORD = "old_password";
    public static final String SEARCH = "search";
    public static final String LIMIT = "limit";
    public static final String OFFSET = "offset";
    public static final String SORT_BY = "sort_by";
    public static final String SORT = "sort";
    public static final String WIRE = "wire";
    public static final String GAS = "gas";
    public static final String PRIMER = "primer";
    public static final String SELECT = "Select";
    public static final String PART_TYPE_BOTH = "BOTH";
    public static final String PART_TYPE_DRILL = "DRILL";
    public static final String PART_TYPE_CUT = "CUT";
    public static final String PART_TYPE_FAB = "FAB";
    public static final String PART_TYPE_WELD = "WELD";
    public static final String FILTER = "filter";
    public static final String PART_ID = "part_id";
    public static final String FABRICATION_ID = "fabrication_id";
    public static final String JOB_ID = "job_id";
    public static final String JOB_NAME = "job_name";
    public static final String JOB_NUMBER = "job_number";

    public static final String CUT_BY_USER_ID = "cut_by_user_id";
    public static final String CUT_CHECKED_BY_USER_ID = "cut_checked_by_user_id";
    public static final String DRILL_BY_USER_ID = "drill_by_user_id";
    public static final String DRILL_CHECKED_BY_USER_ID = "drill_checked_by_user_id";

    public static final String FAB_BY_USER_ID = "fab_by_user_id";
    public static final String FAB_CHECKED_BY_USER_ID = "fab_checked_by_user_id";
    public static final String WELD_BY_USER_ID = "weld_by_user_id";
    public static final String WELD_CHECKED_BY_USER_ID = "weld_checked_by_user_id";

    public static final String INSPECT_BY_USER_ID = "inspect_by_user_id";
    public static final String PARTS_COMPLETED = "parts_completed";
    public static final String STATUS_ID = "status_id";
    public static final String FILE_TYPE = "file_type";
    public static final String TYPE_JOB = "JOB";
    public static final String TYPE_ALL = "ALL";
    public static final String TYPE_ONLY_PART = "PART";
    public static final String TYPE_ONLY_FAB = "FAB";
    public static final String TYPE_ONSITE_DOC = "onsite_document";
    public static final String KEY_FROM = "key_from";
    public static final String TYPE_PART_FAB = "PART_FAB";
    public static final String INSPECTION_HISTORY = "inspection_history";
    public static final String JOB_HISTORY = "job_history";

    public static final int ACTION_PART_FAB_COMPLETED = 9;
    public static final int ACTION_PART_FAB_READY_FOR_INSPECT = 10;
    public static final int ACTION_PART_FAB_READY_FOR_ONSITE = 8;
    public static final int ACTION_PART_FAB_IN_PROGRESS = 7;
    public static final int ACTION_PART_FAB_OPEN = 6;
    public static final int ACTION_PART_FAB_NOT_FULFILLED = 11;

    public static final int ACTION_JOB_COMPLETED = 5;
    public static final int ACTION_JOB_IN_PROGRESS = 2;
    public static final int ACTION_JOB_READY_FOR_INSPECT = 4;
    public static final int ACTION_JOB_NOT_FULFILLED = 3;
    public static final int ACTION_JOB_OPEN = 1;

    /*API*/
    public static final String STATUS = "status";
    public static final String SHOW_MESSAGE = "showMessage";
    public static final String MESSAGE = "message";
    public static final String RESPONSE_CODE = "responseCode";
    public static final String LIST = "list";
    public static final String JOB_PART_STATUS = "job_part_status";
    public static final String TOTAL = "total";
    public static final String STATUS_TYPE_JOB = "JOB";
    public static final String WORKSHOP_WORKER = "Workshop Worker";
    public static final String STATUS_TYPE_PART_FEB = "PART_FAB";

    public static final boolean RESPONSE_STATUS_CODE_SUCCESS = true;
    public static final boolean RESPONSE_STATUS_CODE_FAILURE = false;
    public static final String RESPONSE_SHOW_MESSAGE_SUCCESS = "1";
    public static final String RESPONSE_CODE_SUCCESS = "200";
    public static final boolean KEY_SUCCESS = false;
    public static final boolean KEY_ERROR = true;
    public static final String RESPONSE_DATA = "responseData";
    public static final String PART_DRAWING = "part_drawing";
    public static final String FAB_DRAWING = "fab_drawing";
    public static final String GENERAL_ASSEMBLY = "general_assembly";

    public static final String RESPONSE_ID = "id";
    public static final int LIMIT_COUNT = 15;
    public static long SHIFT_HOURS;
    public static final int SHIFT_MIN_FINAL = 10;

    public static final String  GENERAL_ASSEMBLY_TYPE="GENERAL_ASSEMBLY";
    public static final int PLAY_STORE_REQUEST_CODE = 333;
    public static final int PLAY_STORE_REQUEST_CODE_LOGIN = 444;
    public static final String DEVICE_TYPE="device_type";
    public static final String ANDROID="ANDROID";
}
