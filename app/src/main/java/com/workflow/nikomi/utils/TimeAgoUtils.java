package com.workflow.nikomi.utils;

import android.content.Context;
import android.os.Build;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class TimeAgoUtils {

    private final int SECOND_MILLIS = 1000;
    private final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private Context mContext;

    public TimeAgoUtils(Context mContext) {
        this.mContext = mContext;
    }

    public String getTimeAgo(Date chatDate) {

        String dateToPass = "09/06/2020 00:00:19";
        String dateToEnd = "11/06/2020 00:00:35";

        String timeToReturn = "";
        String dateToShow = "";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date currentDate = Utility.getCurrentDateTime();

        // Time format to show show on chat
        SimpleDateFormat dateFormatToShow = new SimpleDateFormat("dd-MM-yy HH:mm");

        dateToShow = dateFormatToShow.format(chatDate);

        /*try {

            chatDate = simpleDateFormat.parse(dateToPass);
            currentDate = simpleDateFormat.parse(dateToEnd);

            dateToShow = dateFormatToShow.format(chatDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        //  Time difference in milli seconds
        long diff = currentDate.getTime() - chatDate.getTime();

        // Difference in Milliseconds to get hours and minutes
        long diffDaysInMillis = (long) (diff / DAY_MILLIS);


        long diffDays = 0;

        //------------------------------FOR DAYS-------------------------------//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LocalDate strDate = chatDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate enDate = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            diffDays = ChronoUnit.DAYS.between(strDate, enDate);
        } else {
            diffDays = getDaysDifference(chatDate, currentDate);
        }

        //------------------------------FOR HOURS-------------------------------//
        // Update difference to calculate hours
        diff -= diffDaysInMillis * DAY_MILLIS;

        // Difference in hours
        long diffHours = (long) (diff / HOUR_MILLIS);


        //------------------------------FOR MINUTES-------------------------------//
        // Update difference to calculate minutes
        diff -= diffHours * HOUR_MILLIS;

        // Difference in minutes
        long diffMin = (long) (diff / MINUTE_MILLIS);

        //------------------------------FOR SECONDS-------------------------------//
        // Update difference to calculate seconds
        diff -= diffMin * MINUTE_MILLIS;

        // Difference in seconds
        long diffSec = (long) (diff / SECOND_MILLIS);

        if (diffDays > 0) {
            if (diffDays > 1) {
                if (diffDays > 6) {
                    // Need to show date here in a specific format
                    timeToReturn = dateToShow;
                } else {
                    timeToReturn = diffDays + " days ago";
                }
            } else {
                timeToReturn = "yesterday";
            }
        } else if (diffHours > 0) {
            if (diffHours > 1) {
                timeToReturn = diffHours + " hrs ago";
            } else {
                timeToReturn = diffHours + " hr ago";
            }
        } else if (diffMin > 0) {
            if (diffMin > 1) {
                timeToReturn = diffMin + " mins ago";
            } else {
                timeToReturn = "a min ago";
            }
        } else if (diffSec > 0) {
            timeToReturn = "just now";
        } else {
            timeToReturn = dateToShow;
        }

        return (timeToReturn);
    }

    /***
     * This method will be used to get chat date difference
     * @param chatDate
     * @param currentDate
     * @return
     */
    private long getDaysDifference(Date chatDate, Date currentDate) {

        Calendar chatDateCalendar;
        Calendar currentDayCalendar;

        // Chat day
        //endDateCalendar = Calendar.getInstance(TimeZone.getTimeZone("EST"));
        chatDateCalendar = Calendar.getInstance();
        chatDateCalendar.setTimeInMillis(chatDate.getTime());
        chatDateCalendar.set(Calendar.MILLISECOND, 0);
        chatDateCalendar.set(Calendar.MINUTE, 0);
        chatDateCalendar.set(Calendar.HOUR, 0);

        // Current day
        //currentDayCalendar = Calendar.getInstance(TimeZone.getTimeZone("EST"));
        currentDayCalendar = Calendar.getInstance();
        currentDayCalendar.setTimeInMillis(currentDate.getTime());
        currentDayCalendar.set(Calendar.MILLISECOND, 0);
        currentDayCalendar.set(Calendar.MINUTE, 0);
        currentDayCalendar.set(Calendar.HOUR, 0);

        long remainingDays = Math.round((float) (currentDayCalendar.getTimeInMillis() - chatDateCalendar.getTimeInMillis()) / DAY_MILLIS);

        return remainingDays;
    }

}
// References:
// https://stackoverflow.com/questions/27559422/calculating-elasped-time-between-midnight-and-current-time
// https://stackoverflow.com/questions/13198609/java-calendar-getting-difference-between-two-dates-times-off-by-one
// https://stackoverflow.com/questions/2517709/comparing-two-java-util-dates-to-see-if-they-are-in-the-same-day


