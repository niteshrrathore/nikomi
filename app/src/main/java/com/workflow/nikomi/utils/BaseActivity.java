package com.workflow.nikomi.utils;

import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.workflow.nikomi.BuildConfig;
import com.workflow.nikomi.R;
import com.workflow.nikomi.interfaces.CallbackOKCancelInterface;
import com.workflow.nikomi.interfaces.CallbackOkInterface;
import com.workflow.nikomi.interfaces.OverrideJobCallBack;
import com.workflow.nikomi.model.MyCustomEvent;
import com.workflow.nikomi.service.AutoSignoutPopupBroadcastService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.workflow.nikomi.utils.Constant.PLAY_STORE_REQUEST_CODE;
import static com.workflow.nikomi.utils.Utility.showAlertYesCallBackForUpdate;
import static com.workflow.nikomi.utils.Utility.showAlertYesNoCallBackForUpdate;

public class BaseActivity extends AppCompatActivity implements OverrideJobCallBack {

    AppSession appSession;
    public static Typeface typeface;
    public JobScheduler jobScheduler;
    private static final String TAG = "BaseActivity";
    static Context mContext;
    private AlertDialog hardUpdateAlertDialog, softUpdateAlertDialog;
    Intent intent;
    /*public static final String DEV_BASE_URL = "http://ec2-3-11-80-80.eu-west-2.compute.amazonaws.com/";
    public static final String QA_BASE_URL = "http://ec2-3-10-142-63.eu-west-2.compute.amazonaws.com/";*/

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }



    @Nullable
    @Override
    public Intent getSupportParentActivityIntent() {
        return super.getSupportParentActivityIntent();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        StrictMode.VmPolicy.Builder smBuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(smBuilder.build());
        typeface = Typeface.createFromAsset(getAssets(), "fonts/nunito_regular.ttf");
        jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //updateGUI(intent); // or whatever method used to update your GUI fields
            if (appSession.isUserLogin())
            Utility.overrideJobDialog(BaseActivity.this, BaseActivity.this);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        appSession = new AppSession(BaseActivity.this);
        registerReceiver(br, new IntentFilter("JobSchedulerService"));
        Log.i(TAG, "Registered broacast receiver");

        if (appSession.getUserDetails()!=null){
       //  if(getIntent()!=null && getIntent().getIntExtra("override",0) == 1 && appSession.isUserLogin()){
          if(App.showOverRideValue == 1 && appSession.isUserLogin()){
                //open the pop if user comes from the background
                Utility.overrideJobDialog(BaseActivity.this, BaseActivity.this);
            }
        }

        if (appSession.isUserLogin() && !Utility.isServiceRunning(this, AutoSignoutPopupBroadcastService.class)){
            //Long.parseLong(appSession.getShiftHour())-System.currentTimeMillis()
            startAutoSignoutService(/*String.valueOf(SHIFT_HOURS)*/);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(br);
        Log.i(TAG, "Unregistered broacast receiver");
    }

    @Override
    protected void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();

    }

    @Override
    public void onStop() {
        try {
            EventBus.getDefault().unregister(this);
            unregisterReceiver(br);
        } catch (Exception e) {
            // Receiver was probably already stopped in onPause()
        }
        super.onStop();
    }

    @Override
    public void onYes() {

        App.showOverRideValue=0;
        stopAutoSignoutService();
        jobScheduler.cancelAll();
        appSession.setOverrideJob(true);
    }

    @Override
    public void onNo() {
        App.showOverRideValue =0;
        appSession.setOverrideJob(false);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        App.showOverRideValue= intent.getIntExtra("override",0);

    }

    public void startAutoSignoutService(/*String shiftHours*/) {
        Intent serviceIntent = new Intent(this, AutoSignoutPopupBroadcastService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public static void stopAutoSignoutService() {
        Intent serviceIntent = new Intent(mContext, AutoSignoutPopupBroadcastService.class);
        mContext.stopService(serviceIntent);
    }



    @Subscribe
    public void customEventReceived(MyCustomEvent event) {
        showUpdateAlert(event.isUpdate, event.isHardUpdate);
    }


    /**
     * This will show alert to update the application
     *
     * @param isUpdate
     * @param isHardUpdate
     */
    private void showUpdateAlert(boolean isUpdate, String isHardUpdate) {
        if (isUpdate) {

            if (isHardUpdate.equals("1")) {
                //Hide if soft update popup is present
                hideAlreadyShownDialog(softUpdateAlertDialog);
                if (!isDialogVisible(hardUpdateAlertDialog)) {
                    hardUpdateAlertDialog = Utility.showAlertYesCallBackForUpdate(this, mContext.getString(R.string.app_update_hard), mContext.getString(R.string.update), new CallbackOkInterface() {
                        @Override
                        public void onSuccess(boolean isSuccess) {
                            openPlayStore();
                        }
                    });
                    return;
                }
            } else {
                //Hide if soft update popup is present
                hideAlreadyShownDialog(hardUpdateAlertDialog);

                if (!isDialogVisible(softUpdateAlertDialog)) {
                    softUpdateAlertDialog = Utility.showAlertYesNoCallBackForUpdate(this, mContext.getString(R.string.app_update_soft), mContext.getString(R.string.update), mContext.getString(R.string.cancel), new CallbackOKCancelInterface() {
                        @Override
                        public void onSuccess(boolean isSuccess) {
                            Utility.openPlayStoreSoftUpdate(mContext, BaseActivity.this);
                        }

                        @Override
                        public void onFailure(boolean isSuccess) {
                        }
                    });
                }
            }
        } else {

            hideAlreadyShownDialog(hardUpdateAlertDialog);
            hideAlreadyShownDialog(softUpdateAlertDialog);
        }
    }

    /**
     * This will hide the dialog if already shown
     *
     * @param updateAlertDialog
     */
    private void hideAlreadyShownDialog(AlertDialog updateAlertDialog) {
        // If update is not available, it means application is updated then hide the dialog if already present
        if (updateAlertDialog != null) {
            if (updateAlertDialog.isShowing()) {
                updateAlertDialog.dismiss();
            }
        }
    }

    /**
     * This will provide the status of a dialog
     *
     * @param updateAlertDialog
     */
    private boolean isDialogVisible(AlertDialog updateAlertDialog) {
        // If dialog is showing then it will return true
        if (updateAlertDialog != null) {
            if (updateAlertDialog.isShowing()) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method will redirect user to Google play store to update the application
     */
    public void openPlayStore() {
        try {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)), PLAY_STORE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)), PLAY_STORE_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLAY_STORE_REQUEST_CODE) {
            try {
                getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_ACTIVITIES);

                if (App.getInstance() != null) {
                    App.getInstance().geVersionUpdateDetails();
                }
                //App Installed
            } catch (PackageManager.NameNotFoundException e) {
                //App doesn't installed
                if (App.getInstance() != null) {
                    App.getInstance().geVersionUpdateDetails();
                }
            }
        }
    }

}