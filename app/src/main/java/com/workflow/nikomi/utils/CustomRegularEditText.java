package com.workflow.nikomi.utils;

import android.content.Context;
import android.util.AttributeSet;


public class CustomRegularEditText extends androidx.appcompat.widget.AppCompatEditText {

    public CustomRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(Utility.setFontRegular(context));
    }

    public CustomRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public CustomRegularEditText(Context context) {
        super(context);
    }


}
